import request from '@/utils/request'

// 查询资产借用列表
export function listBorrow(query) {
  return request({
    url: '/system/borrow/list',
    method: 'get',
    params: query
  })
}

// 查询资产借用详细
export function getBorrow(id) {
  return request({
    url: '/system/borrow/' + id,
    method: 'get'
  })
}

// 新增资产借用
export function addBorrow(data) {
  return request({
    url: '/system/borrow',
    method: 'post',
    data: data
  })
}

// 修改资产借用
export function updateBorrow(data) {
  return request({
    url: '/system/borrow',
    method: 'put',
    data: data
  })
}

// 删除资产借用
export function delBorrow(id) {
  return request({
    url: '/system/borrow/' + id,
    method: 'delete'
  })
}
