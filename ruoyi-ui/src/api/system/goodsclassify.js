import request from '@/utils/request'

// 查询物品分类列表
export function listGoodsclassify(query) {
  return request({
    url: '/system/goodsclassify/list',
    method: 'get',
    params: query
  })
}

// 查询物品分类详细
export function getGoodsclassify(id) {
  return request({
    url: '/system/goodsclassify/' + id,
    method: 'get'
  })
}

// 新增物品分类
export function addGoodsclassify(data) {
  return request({
    url: '/system/goodsclassify',
    method: 'post',
    data: data
  })
}

// 修改物品分类
export function updateGoodsclassify(data) {
  return request({
    url: '/system/goodsclassify',
    method: 'put',
    data: data
  })
}

// 删除物品分类
export function delGoodsclassify(id) {
  return request({
    url: '/system/goodsclassify/' + id,
    method: 'delete'
  })
}
