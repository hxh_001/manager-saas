import request from '@/utils/request'

// 查询物品入库单列表
export function listGoodsin(query) {
  return request({
    url: '/system/goodsin/list',
    method: 'get',
    params: query
  })
}

// 查询物品入库单详细
export function getGoodsin(id) {
  return request({
    url: '/system/goodsin/' + id,
    method: 'get'
  })
}

// 新增物品入库单
export function addGoodsin(data) {
  return request({
    url: '/system/goodsin',
    method: 'post',
    data: data
  })
}

// 修改物品入库单
export function updateGoodsin(data) {
  return request({
    url: '/system/goodsin',
    method: 'put',
    data: data
  })
}

// 删除物品入库单
export function delGoodsin(id) {
  return request({
    url: '/system/goodsin/' + id,
    method: 'delete'
  })
}
