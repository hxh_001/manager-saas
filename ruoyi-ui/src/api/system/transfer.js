import request from '@/utils/request'

// 查询资产调拨列表
export function listTransfer(query) {
  return request({
    url: '/system/transfer/list',
    method: 'get',
    params: query
  })
}

// 查询资产调拨详细
export function getTransfer(id) {
  return request({
    url: '/system/transfer/' + id,
    method: 'get'
  })
}

// 新增资产调拨
export function addTransfer(data) {
  return request({
    url: '/system/transfer',
    method: 'post',
    data: data
  })
}

// 修改资产调拨
export function updateTransfer(data) {
  return request({
    url: '/system/transfer',
    method: 'put',
    data: data
  })
}

// 删除资产调拨
export function delTransfer(id) {
  return request({
    url: '/system/transfer/' + id,
    method: 'delete'
  })
}
