import request from '@/utils/request'

// 查询资产退库列表
export function listRetire(query) {
  return request({
    url: '/system/retire/list',
    method: 'get',
    params: query
  })
}

// 查询资产退库详细
export function getRetire(id) {
  return request({
    url: '/system/retire/' + id,
    method: 'get'
  })
}

// 新增资产退库
export function addRetire(data) {
  return request({
    url: '/system/retire',
    method: 'post',
    data: data
  })
}

// 修改资产退库
export function updateRetire(data) {
  return request({
    url: '/system/retire',
    method: 'put',
    data: data
  })
}

// 删除资产退库
export function delRetire(id) {
  return request({
    url: '/system/retire/' + id,
    method: 'delete'
  })
}
