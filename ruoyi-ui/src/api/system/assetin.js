import request from '@/utils/request'

// 查询资产入库单列表
export function listAssetin(query) {
  return request({
    url: '/system/assetin/list',
    method: 'get',
    params: query
  })
}

// 查询资产入库单详细
export function getAssetin(id) {
  return request({
    url: '/system/assetin/' + id,
    method: 'get'
  })
}

// 新增资产入库单
export function addAssetin(data) {
  return request({
    url: '/system/assetin',
    method: 'post',
    data: data
  })
}

// 修改资产入库单
export function updateAssetin(data) {
  return request({
    url: '/system/assetin',
    method: 'put',
    data: data
  })
}

// 删除资产入库单
export function delAssetin(id) {
  return request({
    url: '/system/assetin/' + id,
    method: 'delete'
  })
}
