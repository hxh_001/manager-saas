import request from '@/utils/request'

// 查询资产归还列表
export function listReturn(query) {
  return request({
    url: '/system/return/list',
    method: 'get',
    params: query
  })
}

// 查询资产归还详细
export function getReturn(id) {
  return request({
    url: '/system/return/' + id,
    method: 'get'
  })
}

// 新增资产归还
export function addReturn(data) {
  return request({
    url: '/system/return',
    method: 'post',
    data: data
  })
}

// 修改资产归还
export function updateReturn(data) {
  return request({
    url: '/system/return',
    method: 'put',
    data: data
  })
}

// 删除资产归还
export function delReturn(id) {
  return request({
    url: '/system/return/' + id,
    method: 'delete'
  })
}
