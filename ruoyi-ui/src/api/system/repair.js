import request from '@/utils/request'

// 查询资产维修列表
export function listRepair(query) {
  return request({
    url: '/system/repair/list',
    method: 'get',
    params: query
  })
}

// 查询资产维修详细
export function getRepair(id) {
  return request({
    url: '/system/repair/' + id,
    method: 'get'
  })
}

// 新增资产维修
export function addRepair(data) {
  return request({
    url: '/system/repair',
    method: 'post',
    data: data
  })
}

// 修改资产维修
export function updateRepair(data) {
  return request({
    url: '/system/repair',
    method: 'put',
    data: data
  })
}

// 删除资产维修
export function delRepair(id) {
  return request({
    url: '/system/repair/' + id,
    method: 'delete'
  })
}
