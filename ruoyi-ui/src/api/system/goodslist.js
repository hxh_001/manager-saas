import request from '@/utils/request'

// 查询物品名目列表
export function listGoodslist(query) {
  return request({
    url: '/system/goodslist/list',
    method: 'get',
    params: query
  })
}

// 查询物品名目详细
export function getGoodslist(id) {
  return request({
    url: '/system/goodslist/' + id,
    method: 'get'
  })
}

// 新增物品名目
export function addGoodslist(data) {
  return request({
    url: '/system/goodslist',
    method: 'post',
    data: data
  })
}

// 修改物品名目
export function updateGoodslist(data) {
  return request({
    url: '/system/goodslist',
    method: 'put',
    data: data
  })
}

// 删除物品名目
export function delGoodslist(id) {
  return request({
    url: '/system/goodslist/' + id,
    method: 'delete'
  })
}
