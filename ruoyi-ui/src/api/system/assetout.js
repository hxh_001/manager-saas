import request from '@/utils/request'

// 查询外出管理列表
export function listAssetout(query) {
  return request({
    url: '/system/assetout/list',
    method: 'get',
    params: query
  })
}

// 查询外出管理详细
export function getAssetout(id) {
  return request({
    url: '/system/assetout/' + id,
    method: 'get'
  })
}

// 新增外出管理
export function addAssetout(data) {
  return request({
    url: '/system/assetout',
    method: 'post',
    data: data
  })
}

// 修改外出管理
export function updateAssetout(data) {
  return request({
    url: '/system/assetout',
    method: 'put',
    data: data
  })
}

// 删除外出管理
export function delAssetout(id) {
  return request({
    url: '/system/assetout/' + id,
    method: 'delete'
  })
}
