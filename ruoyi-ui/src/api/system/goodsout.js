import request from '@/utils/request'

// 查询物品出库单（关联入库单）列表
export function listGoodsout(query) {
  return request({
    url: '/system/goodsout/list',
    method: 'get',
    params: query
  })
}

// 查询物品出库单（关联入库单）详细
export function getGoodsout(id) {
  return request({
    url: '/system/goodsout/' + id,
    method: 'get'
  })
}

// 新增物品出库单（关联入库单）
export function addGoodsout(data) {
  return request({
    url: '/system/goodsout',
    method: 'post',
    data: data
  })
}

// 修改物品出库单（关联入库单）
export function updateGoodsout(data) {
  return request({
    url: '/system/goodsout',
    method: 'put',
    data: data
  })
}

// 删除物品出库单（关联入库单）
export function delGoodsout(id) {
  return request({
    url: '/system/goodsout/' + id,
    method: 'delete'
  })
}
