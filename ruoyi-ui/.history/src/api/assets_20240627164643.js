import request from '@/utils/request'


// 使用单位：/system/dept/list/unit
// 使用部门：/system/user/deptTree?parentId=
// 使用人：/system/user/deptUsers/{deptId}
// 获取路由
export const unitList = () => {
  return request({
    url: '/system/dept/list/unit',
    method: 'get'
  })
}
export const unitList = () => {
  return request({
    url: '/system/user/deptTree?parentId=',
    method: 'get'
  })
}
export const deptUsers = (deptId) => {
  return request({
    url: `/system/user/deptUsers/${deptId}`,
    method: 'get'
  })
}