package com.ruoyi.framework.config;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;

/**
 * Mybatis Plus 配置
 *
 * @author ruoyi
 */
@EnableTransactionManagement(proxyTargetClass = true)
@Configuration
public class MybatisPlusConfig
{
	@Bean
	public MybatisPlusInterceptor mybatisPlusInterceptor() {
		MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
		// 分页插件
		interceptor.addInnerInterceptor(paginationInnerInterceptor());
		// 多租户
		interceptor.addInnerInterceptor(tenantLineInnerInterceptor());
		// 乐观锁插件
		interceptor.addInnerInterceptor(optimisticLockerInnerInterceptor());
		// 阻断插件
		interceptor.addInnerInterceptor(blockAttackInnerInterceptor());
		return interceptor;
	}

    /**
     * 分页插件，自动识别数据库类型 https://baomidou.com/guide/interceptor-pagination.html
     */
    public PaginationInnerInterceptor paginationInnerInterceptor()
    {
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        // 设置数据库类型为mysql
        paginationInnerInterceptor.setDbType(DbType.MYSQL);
        // 设置最大单页限制数量，默认 500 条，-1 不受限制
        paginationInnerInterceptor.setMaxLimit(-1L);
        return paginationInnerInterceptor;
    }
    
    private InnerInterceptor tenantLineInnerInterceptor() {
        return new TenantLineInnerInterceptor(new TenantLineHandler() {
            @Override
            public Expression getTenantId() {
            	if(Objects.nonNull(SecurityContextHolder.getContext().getAuthentication())) {
            		LoginUser loginUser = SecurityUtils.getLoginUser();
                	
                	boolean isAdmin = SysUser.isAdmin(loginUser.getUserId());
                	if(!isAdmin) {
                		
                		return new LongValue(loginUser.getUser().getMerchantId());
                	}
            	}
            	System.out.println("超管用户无需过滤租户");
        		return new LongValue(0);// 测试租号 merchant_id
            	
            	 
            }

            @Override
            public String getTenantIdColumn() {
            	if(Objects.nonNull(SecurityContextHolder.getContext().getAuthentication())) {
            		LoginUser loginUser = SecurityUtils.getLoginUser();
                	boolean isAdmin = SysUser.isAdmin(loginUser.getUserId());
                	if(!isAdmin) {
                		return "merchant_id";
                	}
            	}
            	System.out.println("超管用户无需过滤租户");
        		return "superadmin";
            }

            // 这是 default 方法,默认返回 false 表示所有表都需要拼多租户条件
            @Override
            public boolean ignoreTable(String tableName) {
                return !StringUtils.equalsAny(tableName,
                		"mms_asset","mms_asset_borrow",
                        "mms_asset_classify","mms_asset_depreciation",
                        "mms_asset_info_change","mms_asset_inventory",
                        "mms_asset_inventoryrelation","mms_asset_no_rule",
                        "mms_asset_receive","mms_asset_relation",
                        "mms_asset_repair","mms_asset_retire",
                        "mms_asset_return","mms_asset_scrap",
                        "mms_asset_transfer","mms_goodsclassify",
                        "mms_goodsin","mms_goodsinrelation",
                        "mms_goodslist","mms_goodsout",
                        "mms_inventory_order","mms_supplier",
                        "sys_user","sys_dept","sys_role"
                    );
            	//return true;
            }
        });
    }
    
    

    /**
     * 乐观锁插件 https://baomidou.com/guide/interceptor-optimistic-locker.html
     */
    public OptimisticLockerInnerInterceptor optimisticLockerInnerInterceptor()
    {
        return new OptimisticLockerInnerInterceptor();
    }

    /**
     * 如果是对全表的删除或更新操作，就会终止该操作 https://baomidou.com/guide/interceptor-block-attack.html
     */
    public BlockAttackInnerInterceptor blockAttackInnerInterceptor()
    {
        return new BlockAttackInnerInterceptor();
    }
}
