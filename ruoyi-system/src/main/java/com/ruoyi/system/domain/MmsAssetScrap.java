package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产报废对象 mms_asset_scrap
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetScrap extends BaseEntity
{
    private static final long serialVersionUID = 1L;

  
    private Long id;

    /** 租户id */
    private Long merchantId;
    
    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 报废日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报废日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date disposalDate;

    /** 单据说明 */
    @Excel(name = "单据说明")
    private String disposalDes;

    /** 报废单号 */
    @Excel(name = "报废单号")
    private String disposalNo;

    /** 报废原因 */
    @Excel(name = "报废原因")
    private String disposalReason;

    /** 财务意见 */
    @Excel(name = "财务意见")
    private String financeOpinion;

    /** 主管部门意见 */
    @Excel(name = "主管部门意见")
    private String mainDeptOpinion;

    /** 报废状态 */
    @Excel(name = "报废状态")
    private Long status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setDisposalDate(Date disposalDate) 
    {
        this.disposalDate = disposalDate;
    }

    public Date getDisposalDate() 
    {
        return disposalDate;
    }
    public void setDisposalDes(String disposalDes) 
    {
        this.disposalDes = disposalDes;
    }

    public String getDisposalDes() 
    {
        return disposalDes;
    }
    public void setDisposalNo(String disposalNo) 
    {
        this.disposalNo = disposalNo;
    }

    public String getDisposalNo() 
    {
        return disposalNo;
    }
    public void setDisposalReason(String disposalReason) 
    {
        this.disposalReason = disposalReason;
    }

    public String getDisposalReason() 
    {
        return disposalReason;
    }
    public void setFinanceOpinion(String financeOpinion) 
    {
        this.financeOpinion = financeOpinion;
    }

    public String getFinanceOpinion() 
    {
        return financeOpinion;
    }
    public void setMainDeptOpinion(String mainDeptOpinion) 
    {
        this.mainDeptOpinion = mainDeptOpinion;
    }

    public String getMainDeptOpinion() 
    {
        return mainDeptOpinion;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("disposalDate", getDisposalDate())
            .append("disposalDes", getDisposalDes())
            .append("disposalNo", getDisposalNo())
            .append("disposalReason", getDisposalReason())
            .append("financeOpinion", getFinanceOpinion())
            .append("mainDeptOpinion", getMainDeptOpinion())
            .append("status", getStatus())
            .toString();
    }

	public String getAssetIds() {
		return assetIds;
	}

	public void setAssetIds(String assetIds) {
		this.assetIds = assetIds;
	}

	public List<MmsAsset> getAssetList() {
		return assetList;
	}

	public void setAssetList(List<MmsAsset> assetList) {
		this.assetList = assetList;
	}
}
