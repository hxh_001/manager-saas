package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物品出库单（关联入库单）对象 mms_goodsout
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public class MmsGoodsout extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    private String goodsoutNo;

    /** 出库数量 */
    @Excel(name = "出库数量")
    private Long number;

    /** 入库单 */
    @Excel(name = "入库单")
    private Long goodsinId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodsoutNo(String goodsoutNo) 
    {
        this.goodsoutNo = goodsoutNo;
    }

    public String getGoodsoutNo() 
    {
        return goodsoutNo;
    }
    public void setNumber(Long number) 
    {
        this.number = number;
    }

    public Long getNumber() 
    {
        return number;
    }
    public void setGoodsinId(Long goodsinId) 
    {
        this.goodsinId = goodsinId;
    }

    public Long getGoodsinId() 
    {
        return goodsinId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsoutNo", getGoodsoutNo())
            .append("number", getNumber())
            .append("goodsinId", getGoodsinId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
