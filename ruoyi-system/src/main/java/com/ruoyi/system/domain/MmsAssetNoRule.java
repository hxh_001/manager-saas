package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 编码规则对象 mms_asset_no_rule
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public class MmsAssetNoRule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

  
    private Long id;
    
    /**
     * 租户id
     */
    private Long merchantId;
    

    /** 编号规则 */
    @Excel(name = "编号规则")
    private String backup1;

    /** 状态 */
    @Excel(name = "状态")
    private Long isOn;
    
    private Integer type;

    /** 编号规则 */
    @Excel(name = "编号规则")
    private String noRule;
    
    
    /** 编号规则 */
    @Excel(name = "自增值")
    private Integer autoNum;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup1(String backup1) 
    {
        this.backup1 = backup1;
    }

    public String getBackup1() 
    {
        return backup1;
    }
    public void setIsOn(Long isOn) 
    {
        this.isOn = isOn;
    }

    public Long getIsOn() 
    {
        return isOn;
    }
    public void setNoRule(String noRule) 
    {
        this.noRule = noRule;
    }

    public String getNoRule() 
    {
        return noRule;
    }

  

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getAutoNum() {
		return autoNum;
	}

	public void setAutoNum(Integer autoNum) {
		this.autoNum = autoNum;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	
	
	
}
