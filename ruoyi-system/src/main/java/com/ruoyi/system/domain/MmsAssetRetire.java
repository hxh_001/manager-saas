package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产退库对象 mms_asset_retire
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetRetire extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;

    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 退库日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退库日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date retireDate;

    /** 退库性质 */
    @Excel(name = "退库性质")
    private Long retireDes;

    /** 退库单号 */
    @Excel(name = "退库单号")
    private String retireNo;

    /** 退库状态 */
    @Excel(name = "退库状态")
    private Long status;

    /** 存放地点 */
    @Excel(name = "存放地点")
    private Long storge;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setRetireDate(Date retireDate) 
    {
        this.retireDate = retireDate;
    }

    public Date getRetireDate() 
    {
        return retireDate;
    }
    public void setRetireDes(Long retireDes) 
    {
        this.retireDes = retireDes;
    }

    public Long getRetireDes() 
    {
        return retireDes;
    }
    public void setRetireNo(String retireNo) 
    {
        this.retireNo = retireNo;
    }

    public String getRetireNo() 
    {
        return retireNo;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setStorge(Long storge) 
    {
        this.storge = storge;
    }

    public Long getStorge() 
    {
        return storge;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("retireDate", getRetireDate())
            .append("retireDes", getRetireDes())
            .append("retireNo", getRetireNo())
            .append("status", getStatus())
            .append("storge", getStorge())
            .toString();
    }
}
