package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 资产信息对象 mms_asset
 * 
 * @author ruoyi
 * @date 2024-06-18
 */
@Data
public class MmsAsset extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 资产名称 */
    @Excel(name = "资产名称")
    private String assetName;
    
    @TableField(exist = false)
    private String inStock;

    /** 租户id */
    private Long merchantId;

    /** 资产性质 */
    @Excel(name = "资产性质")
    private String assetNature;

    //资产分类id
    private Long classifyId;

    /** 资产编号 */
    @Excel(name = "资产编号")
    private String assetNo;

    /** 会计凭证号 */
    @Excel(name = "会计凭证号")
    private String accountingNumber;

    /** 金额 */
    @Excel(name = "金额")
    private Long amount;

    /** 区域 */
    @Excel(name = "区域")
    private String areaName;
    
    private Long areaId;

    /** 资产原值 */
    @Excel(name = "资产原值")
    private BigDecimal assetOriginalValue;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 配置信息 */
    @Excel(name = "配置信息")
    private String configInfo;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** EPC */
    @Excel(name = "EPC")
    private String epc;

    /** 扩展信息 */
    @Excel(name = "扩展信息")
    private String expansionInfo;

    /** 财务编号 */
    @Excel(name = "财务编号")
    private String financeNo;

    /** 取得方式 */
    @Excel(name = "取得方式")
    private String gainWay;

    /** 生产厂商 */
    @Excel(name = "生产厂商")
    private String manufacturer;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String measureUnit;

    /** 购置日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "购置日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date purchaseDate;

    /** 序列号 */
    @Excel(name = "序列号")
    private String serialNumber;

    /** 质保期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "质保期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date shelfLife;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String specificationModel;

    /** 资产状态 */
    @Excel(name = "资产状态")
    private Integer status;

    /** 存放地点 */
    @Excel(name = "存放地点")
    private String storageLocationName;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplier;

    /** 税额 */
    @Excel(name = "税额")
    private String tax;
    
    /** 使用部门 */
    @Excel(name = "使用部门")
    private Long useDeptId;

    /** 使用部门 */
    @Excel(name = "使用部门名称")
    private String useDeptName;
    
    /** 使用部门 */
    @Excel(name = "使用单位")
    private Long useUnitId;

    /** 使用单位 */
    @Excel(name = "使用单位名称")
    private String useUnitName;

    /** 使用月限 */
    @Excel(name = "使用月限")
    private Long usefulLife;
    
    /** 使用人 */
    private Long userId;

    /** 使用人 */
    @Excel(name = "使用人")
    private String userName;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("assetName", getAssetName())
            .append("assetNature", getAssetNature())
            .append("assetNo", getAssetNo())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("accountingNumber", getAccountingNumber())
            .append("amount", getAmount())
            .append("areaName", getAreaName())
            .append("assetOriginalValue", getAssetOriginalValue())
            .append("brand", getBrand())
            .append("configInfo", getConfigInfo())
            .append("contractNo", getContractNo())
            .append("epc", getEpc())
            .append("expansionInfo", getExpansionInfo())
            .append("financeNo", getFinanceNo())
            .append("gainWay", getGainWay())
            .append("manufacturer", getManufacturer())
            .append("measureUnit", getMeasureUnit())
            .append("purchaseDate", getPurchaseDate())
            .append("serialNumber", getSerialNumber())
            .append("shelfLife", getShelfLife())
            .append("specificationModel", getSpecificationModel())
            .append("status", getStatus())
            .append("storageLocationName", getStorageLocationName())
            .append("supplier", getSupplier())
            .append("tax", getTax())
            .append("useDeptName", getUseDeptName())
            .append("useUnitName", getUseUnitName())
            .append("usefulLife", getUsefulLife())
            .append("userName", getUserName())
            .toString();
    }
}
