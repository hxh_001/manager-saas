package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import lombok.Data;

/**
 * 测试对象 test
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@Data
@TableName("test")
public class Test extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 主键 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/** 名称 */
	@Excel(name = "名称")
	private String name;

	
}
