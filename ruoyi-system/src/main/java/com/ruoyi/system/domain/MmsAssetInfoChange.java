package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 资产信息变更对象 mms_asset_info_change
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetInfoChange extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;

    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 数量 */
    @Excel(name = "数量")
    private Long amount;

    /** 区域 */
    @Excel(name = "区域")
    private String areaName;

    /** 资产分类 */
    @Excel(name = "资产分类")
    private String assetClassifyId;

    /** 资产名称 */
    @Excel(name = "资产名称")
    private String assetName;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 财务编号 */
    @Excel(name = "财务编号")
    private String financeNo;

    /** 生产厂商 */
    @Excel(name = "生产厂商")
    private String manufacturer;

    /**  计量单位 */
    @Excel(name = " 计量单位")
    private String measureUnit;

    /** 序列号 */
    @Excel(name = "序列号")
    private String serialNumber;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String specificationModel;

    /** 存放地点 */
    @Excel(name = "存放地点")
    private String storageLocationName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setAssetClassifyId(String assetClassifyId) 
    {
        this.assetClassifyId = assetClassifyId;
    }

    public String getAssetClassifyId() 
    {
        return assetClassifyId;
    }
    public void setAssetName(String assetName) 
    {
        this.assetName = assetName;
    }

    public String getAssetName() 
    {
        return assetName;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setContractNo(String contractNo) 
    {
        this.contractNo = contractNo;
    }

    public String getContractNo() 
    {
        return contractNo;
    }
    public void setFinanceNo(String financeNo) 
    {
        this.financeNo = financeNo;
    }

    public String getFinanceNo() 
    {
        return financeNo;
    }
    public void setManufacturer(String manufacturer) 
    {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer() 
    {
        return manufacturer;
    }
    public void setMeasureUnit(String measureUnit) 
    {
        this.measureUnit = measureUnit;
    }

    public String getMeasureUnit() 
    {
        return measureUnit;
    }
    public void setSerialNumber(String serialNumber) 
    {
        this.serialNumber = serialNumber;
    }

    public String getSerialNumber() 
    {
        return serialNumber;
    }
    public void setSpecificationModel(String specificationModel) 
    {
        this.specificationModel = specificationModel;
    }

    public String getSpecificationModel() 
    {
        return specificationModel;
    }
    public void setStorageLocationName(String storageLocationName) 
    {
        this.storageLocationName = storageLocationName;
    }

    public String getStorageLocationName() 
    {
        return storageLocationName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("amount", getAmount())
            .append("areaName", getAreaName())
            .append("assetClassifyId", getAssetClassifyId())
            .append("assetName", getAssetName())
            .append("backup", getBackup())
            .append("brand", getBrand())
            .append("contractNo", getContractNo())
            .append("financeNo", getFinanceNo())
            .append("manufacturer", getManufacturer())
            .append("measureUnit", getMeasureUnit())
            .append("serialNumber", getSerialNumber())
            .append("specificationModel", getSpecificationModel())
            .append("storageLocationName", getStorageLocationName())
            .toString();
    }
}
