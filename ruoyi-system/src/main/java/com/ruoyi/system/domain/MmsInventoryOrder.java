package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 盘点管理对象 mms_inventory_order
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public class MmsInventoryOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 盘点单号 */
    @Excel(name = "盘点单号")
    private String inventoryNo;

    /** 是否明盘 */
    @Excel(name = "是否明盘")
    private Long isOn;

    /** 盘点状态 */
    @Excel(name = "盘点状态")
    private Long status;
    /** 指定盘点人 */
    @Excel(name = "指定盘点人")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setInventoryNo(String inventoryNo) 
    {
        this.inventoryNo = inventoryNo;
    }

    public String getInventoryNo() 
    {
        return inventoryNo;
    }
    public void setIsOn(Long isOn) 
    {
        this.isOn = isOn;
    }

    public Long getIsOn() 
    {
        return isOn;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("inventoryNo", getInventoryNo())
            .append("isOn", getIsOn())
            .append("status", getStatus())
            .append("userId", getUserId())
            .toString();
    }
}
