package com.ruoyi.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产和功能项关联对象 mms_asset_relation
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 资产功能项标识（字典值） */
    @Excel(name = "资产功能项标识", readConverterExp = "字=典值")
    private String assetFunction;

    /** 关联的资产id */
    @Excel(name = "关联的资产id")
    private Long assetId;

    /** 被关联资产功能项的id */
    @Excel(name = "被关联资产功能项的id")
    private Long functionId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAssetFunction(String assetFunction) 
    {
        this.assetFunction = assetFunction;
    }

    public String getAssetFunction() 
    {
        return assetFunction;
    }
    public void setAssetId(Long assetId) 
    {
        this.assetId = assetId;
    }

    public Long getAssetId() 
    {
        return assetId;
    }
    public void setFunctionId(Long functionId) 
    {
        this.functionId = functionId;
    }

    public Long getFunctionId() 
    {
        return functionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("assetFunction", getAssetFunction())
            .append("assetId", getAssetId())
            .append("functionId", getFunctionId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
