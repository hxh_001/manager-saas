package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 首页统计，资产分类统计
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Data
public class MmsStatClassify implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 数量
     */
    private Integer count = 0;

    public MmsStatClassify(String name, Integer count) {
        this.name = name;
        this.count = count;
    }
}
