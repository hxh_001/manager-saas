package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 资产折旧对象 mms_asset_depreciation
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@Data
public class MmsAssetDepreciation extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    @Excel(name = "资产id")
    private Long assetId;

    //折旧状态：1：折旧中，2：折旧完成（到期了/金额为0）
    private int depStatus;

    /**
     * 任务名称
     */
    @Excel(name = "任务名称")
    private String taskName;

    /**
     * 月折旧额
     */
    @Excel(name = "月折旧额")
    private BigDecimal monthAmount;

    /**
     * 剩余使用期限
     */
    @Excel(name = "剩余使用期限")
    private Long remainDate;

    /**
     * 累计旧额
     */
    @Excel(name = "累计旧额")
    private BigDecimal totalAmount;

    /**
     * 净值
     */
    @Excel(name = "净值")
    private BigDecimal netWorth;

    /**
     * 到期日
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dueDate;

    /**
     * 已计提期数
     */
    @Excel(name = "已计提期数")
    private Long periodsNo;


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("taskName", getTaskName())
                .append("monthAmount", getMonthAmount())
                .append("remainDate", getRemainDate())
                .append("totalAmount", getTotalAmount())
                .append("netWorth", getNetWorth())
                .append("dueDate", getDueDate())
                .append("periodsNo", getPeriodsNo())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
