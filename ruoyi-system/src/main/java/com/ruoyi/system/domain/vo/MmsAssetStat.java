package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 首页统计
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Data
public class MmsAssetStat implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 总数
     */
    private Integer total;

    /**
     * 在库
     */
    private Integer inStock;

    /**
     * 在用
     */
    private Integer inUse;

    /**
     * 在借
     */
    private Integer inBorrow;

    /**
     * 在修
     */
    private Integer inRepair;

    /**
     * 已报废
     */
    private Integer scrap;

    /**
     * 已外出
     */
    private Integer inOut;

    public MmsAssetStat() {
        this.total = 0;
        this.inStock = 0;
        this.inUse = 0;
        this.inBorrow = 0;
        this.inRepair = 0;
        this.scrap = 0;
        this.inOut = 0;
    }
}
