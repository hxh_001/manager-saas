package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 资产借用对象 mms_asset_borrow
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetBorrow extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;

    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 借用日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "借用日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date borrowDate;

    /** 借用说明 */
    @Excel(name = "借用说明")
    private String borrowDes;

    /** 借还单号 */
    @Excel(name = "借还单号")
    private String borrowNo;

    /** 借用原因 */
    @Excel(name = "借用原因")
    private String borrowReason;

    /** 借用用途 */
    @Excel(name = "借用用途")
    private String borrowUse;

    /** 预计归还日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预计归还日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedReturnDate;

    /** 借用状态 */
    @Excel(name = "借用状态")
    private Long status;

    /** 存放地点 */
    @Excel(name = "存放地点")
    private Long storge;

    /** 使用人 */
    @Excel(name = "使用人")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setBorrowDate(Date borrowDate) 
    {
        this.borrowDate = borrowDate;
    }

    public Date getBorrowDate() 
    {
        return borrowDate;
    }
    public void setBorrowDes(String borrowDes) 
    {
        this.borrowDes = borrowDes;
    }

    public String getBorrowDes() 
    {
        return borrowDes;
    }
    public void setBorrowNo(String borrowNo) 
    {
        this.borrowNo = borrowNo;
    }

    public String getBorrowNo() 
    {
        return borrowNo;
    }
    public void setBorrowReason(String borrowReason) 
    {
        this.borrowReason = borrowReason;
    }

    public String getBorrowReason() 
    {
        return borrowReason;
    }
    public void setBorrowUse(String borrowUse) 
    {
        this.borrowUse = borrowUse;
    }

    public String getBorrowUse() 
    {
        return borrowUse;
    }
    public void setExpectedReturnDate(Date expectedReturnDate) 
    {
        this.expectedReturnDate = expectedReturnDate;
    }

    public Date getExpectedReturnDate() 
    {
        return expectedReturnDate;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setStorge(Long storge) 
    {
        this.storge = storge;
    }

    public Long getStorge() 
    {
        return storge;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("borrowDate", getBorrowDate())
            .append("borrowDes", getBorrowDes())
            .append("borrowNo", getBorrowNo())
            .append("borrowReason", getBorrowReason())
            .append("borrowUse", getBorrowUse())
            .append("expectedReturnDate", getExpectedReturnDate())
            .append("status", getStatus())
            .append("storge", getStorge())
            .append("userId", getUserId())
            .toString();
    }
}
