package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 资产入库单对象 mms_asset_assetin
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Data
public class MmsAssetAssetin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;

    /** 编码 */
    @Excel(name = "编码")
    private String assetinNo;

    /** 入库日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** 使用单位 */
    @Excel(name = "使用单位")
    private Long useUnitId;

    /** 入库部门 */
    @Excel(name = "入库部门")
    private Long inDeptId;

    /** 入库人 */
    @Excel(name = "入库人")
    private Long inUserId;

    /** 采购单位 */
    @Excel(name = "采购单位")
    private Long purchaseUnitId;

    /** 采购部门 */
    @Excel(name = "采购部门")
    private Long purchaseDeptId;

    /** 采购人 */
    @Excel(name = "采购人")
    private Long purchaseUserId;

    /** 采购日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date purchaseTime;

    /** 附件 */
    @Excel(name = "附件")
    private String attachment;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("assetinNo", getAssetinNo())
            .append("inTime", getInTime())
            .append("useUnitId", getUseUnitId())
            .append("inDeptId", getInDeptId())
            .append("inUserId", getInUserId())
            .append("purchaseUnitId", getPurchaseUnitId())
            .append("purchaseDeptId", getPurchaseDeptId())
            .append("purchaseUserId", getPurchaseUserId())
            .append("purchaseTime", getPurchaseTime())
            .append("remark", getRemark())
            .append("attachment", getAttachment())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
