package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 资产盘点对象 mms_asset_inventory
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@Data
public class MmsAssetInventory extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 租户id
     */
    private Long merchantId;

    //@Excel(name = "盘点类型")
    private Integer inventoryType;

    //当盘点类型为部门盘，或是盲盘的时候，需要选择资产，这时候需要赋值
    private String assetIds;

    //当盘点类型为部门盘，或是盲盘的时候，需要选择资产，这时候需要赋值
    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /**
     * 任务名称
     */
    @Excel(name = "任务名称")
    private String taskName;

    /**
     * 任务状态
     */
    @Excel(name = "盘点状态，1未开始 2 盘点中 3 盘点结束")
    private Integer inventoryStatus;

    /**
     * 盘点人
     */
    @Excel(name = "盘点人")
    private Long inventoryUser;

    /**
     * 盘点部门
     */
    @Excel(name = "盘点部门")
    private String inventoryDept;

    /**
     * 盘点单号
     */
    @Excel(name = "盘点单号")
    private String inventoryNo;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String backup;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("taskName", getTaskName())
                .append("inventoryStatus", getInventoryStatus())
                .append("inventoryUser", getInventoryUser())
                .append("inventoryDept", getInventoryDept())
                .append("inventoryNo", getInventoryNo())
                .append("startDate", getStartDate())
                .append("endDate", getEndDate())
                .append("backup", getBackup())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
