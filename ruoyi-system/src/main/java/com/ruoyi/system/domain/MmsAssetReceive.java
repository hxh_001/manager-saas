package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产领用对象 mms_asset_receive
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetReceive extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;
    
    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 领用日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "领用日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date receiveDate;

    /** 领用性质 */
    @Excel(name = "领用性质")
    private Long receiveDes;

    /** 领用用途 */
    @Excel(name = "领用用途")
    private String receiveUse;

    /** 领用状态 */
    @Excel(name = "领用状态")
    private Long status;

    /** 存放地点 */
    @Excel(name = "存放地点")
    private Long storge;

    /** 领用单号 */
    @Excel(name = "领用单号")
    private String transferNo;

    /** 使用人 */
    @Excel(name = "使用人")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setReceiveDate(Date receiveDate) 
    {
        this.receiveDate = receiveDate;
    }

    public Date getReceiveDate() 
    {
        return receiveDate;
    }
    public void setReceiveDes(Long receiveDes) 
    {
        this.receiveDes = receiveDes;
    }

    public Long getReceiveDes() 
    {
        return receiveDes;
    }
    public void setReceiveUse(String receiveUse) 
    {
        this.receiveUse = receiveUse;
    }

    public String getReceiveUse() 
    {
        return receiveUse;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setStorge(Long storge) 
    {
        this.storge = storge;
    }

    public Long getStorge() 
    {
        return storge;
    }
    public void setTransferNo(String transferNo) 
    {
        this.transferNo = transferNo;
    }

    public String getTransferNo() 
    {
        return transferNo;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("receiveDate", getReceiveDate())
            .append("receiveDes", getReceiveDes())
            .append("receiveUse", getReceiveUse())
            .append("status", getStatus())
            .append("storge", getStorge())
            .append("transferNo", getTransferNo())
            .append("userId", getUserId())
            .toString();
    }

	public String getAssetIds() {
		return assetIds;
	}

	public void setAssetIds(String assetIds) {
		this.assetIds = assetIds;
	}

	public List<MmsAsset> getAssetList() {
		return assetList;
	}

	public void setAssetList(List<MmsAsset> assetList) {
		this.assetList = assetList;
	}
}
