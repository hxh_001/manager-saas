package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产维修对象 mms_asset_repair
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetRepair extends BaseEntity
{
    private static final long serialVersionUID = 1L;

   
    private Long id;

    /** 租户id */
    private Long merchantId;
    
    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 报修日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报修日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date repairDate;

    /** 维修说明 */
    @Excel(name = "维修说明")
    private String repairDes;

    /** 维修单号 */
    @Excel(name = "维修单号")
    private String repairNo;

    /** 维修状态 */
    @Excel(name = "维修状态")
    private Long status;

    /** 使用人 */
    @Excel(name = "使用人")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setRepairDate(Date repairDate) 
    {
        this.repairDate = repairDate;
    }

    public Date getRepairDate() 
    {
        return repairDate;
    }
    public void setRepairDes(String repairDes) 
    {
        this.repairDes = repairDes;
    }

    public String getRepairDes() 
    {
        return repairDes;
    }
    public void setRepairNo(String repairNo) 
    {
        this.repairNo = repairNo;
    }

    public String getRepairNo() 
    {
        return repairNo;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("repairDate", getRepairDate())
            .append("repairDes", getRepairDes())
            .append("repairNo", getRepairNo())
            .append("status", getStatus())
            .append("userId", getUserId())
            .toString();
    }

	public String getAssetIds() {
		return assetIds;
	}

	public void setAssetIds(String assetIds) {
		this.assetIds = assetIds;
	}

	public List<MmsAsset> getAssetList() {
		return assetList;
	}

	public void setAssetList(List<MmsAsset> assetList) {
		this.assetList = assetList;
	}
}
