package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物品名目对象 mms_goodslist
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public class MmsGoodslist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String goodsName;

    /** 编码 */
    @Excel(name = "编码")
    private String goodsNo;

    /** 规格 */
    @Excel(name = "规格")
    private String specifications;

    /** 型号 */
    @Excel(name = "型号")
    private String goodsType;

    /** 品牌分类id */
    @Excel(name = "品牌分类id")
    private Long classifyId;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String measurementUnit;

    /** 使用单位 */
    @Excel(name = "使用单位")
    private Long userUnit;

    /** 使用部门 */
    @Excel(name = "使用部门")
    private Long userDept;

    /** 使用人 */
    @Excel(name = "使用人")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsNo(String goodsNo) 
    {
        this.goodsNo = goodsNo;
    }

    public String getGoodsNo() 
    {
        return goodsNo;
    }
    public void setSpecifications(String specifications) 
    {
        this.specifications = specifications;
    }

    public String getSpecifications() 
    {
        return specifications;
    }
    public void setGoodsType(String goodsType) 
    {
        this.goodsType = goodsType;
    }

    public String getGoodsType() 
    {
        return goodsType;
    }
    public void setClassifyId(Long classifyId) 
    {
        this.classifyId = classifyId;
    }

    public Long getClassifyId() 
    {
        return classifyId;
    }
    public void setMeasurementUnit(String measurementUnit) 
    {
        this.measurementUnit = measurementUnit;
    }

    public String getMeasurementUnit() 
    {
        return measurementUnit;
    }
    public void setUserUnit(Long userUnit) 
    {
        this.userUnit = userUnit;
    }

    public Long getUserUnit() 
    {
        return userUnit;
    }
    public void setUserDept(Long userDept) 
    {
        this.userDept = userDept;
    }

    public Long getUserDept() 
    {
        return userDept;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsName", getGoodsName())
            .append("goodsNo", getGoodsNo())
            .append("specifications", getSpecifications())
            .append("goodsType", getGoodsType())
            .append("classifyId", getClassifyId())
            .append("measurementUnit", getMeasurementUnit())
            .append("userUnit", getUserUnit())
            .append("userDept", getUserDept())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
