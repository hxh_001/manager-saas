package com.ruoyi.system.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 供应商管理对象 mms_supplier
 *
 * @author ruoyi
 * @date 2024-08-30
 */
@TableName("mms_supplier")
public class MmsSupplier extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 主键 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/** 编号 */
	@Excel(name = "编号")
	private String no;

	/** 供应商名称 */
	@Excel(name = "供应商名称")
	private String name;

	/** 统一信用代码 */
	@Excel(name = "统一信用代码")
	private String businessLicenseNo;

	/** 税号 */
	@Excel(name = "税号")
	private String taxNo;

	/** 状态 */
	@Excel(name = "状态")
	private Integer status;

	/** 注册地址 */
	@Excel(name = "注册地址")
	private String registerAddress;

	/** 通讯地址 */
	@Excel(name = "通讯地址")
	private String registerCapital;

	/** 邮箱 */
	@Excel(name = "邮箱")
	private String rating;

	/** 联系人姓名 */
	@Excel(name = "联系人姓名")
	private String contact;

	/** 联系电话 */
	@Excel(name = "联系电话")
	private String contactPhone;

	/** 开户行名称 */
	@Excel(name = "开户行名称")
	private String collectingBank;

	/** 账号 */
	@Excel(name = "账号")
	private String account;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getNo() {
		return no;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setBusinessLicenseNo(String businessLicenseNo) {
		this.businessLicenseNo = businessLicenseNo;
	}

	public String getBusinessLicenseNo() {
		return businessLicenseNo;
	}

	public void setTaxNo(String taxNo) {
		this.taxNo = taxNo;
	}

	public String getTaxNo() {
		return taxNo;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}

	public String getRegisterAddress() {
		return registerAddress;
	}

	public void setRegisterCapital(String registerCapital) {
		this.registerCapital = registerCapital;
	}

	public String getRegisterCapital() {
		return registerCapital;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRating() {
		return rating;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContact() {
		return contact;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setCollectingBank(String collectingBank) {
		this.collectingBank = collectingBank;
	}

	public String getCollectingBank() {
		return collectingBank;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccount() {
		return account;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId()).append("no", getNo())
				.append("name", getName()).append("businessLicenseNo", getBusinessLicenseNo())
				.append("taxNo", getTaxNo()).append("status", getStatus())
				.append("registerAddress", getRegisterAddress()).append("registerCapital", getRegisterCapital())
				.append("rating", getRating()).append("contact", getContact()).append("contactPhone", getContactPhone())
				.append("collectingBank", getCollectingBank()).append("account", getAccount())
				.append("createTime", getCreateTime()).append("updateTime", getUpdateTime()).toString();
	}
}
