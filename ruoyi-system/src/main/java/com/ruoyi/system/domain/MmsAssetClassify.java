package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 资产分类对象 mms_asset_classify
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public class MmsAssetClassify extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 父id */
    @Excel(name = "父id")
    private Long pid;

    /** 资产分类名称 */
    @Excel(name = "资产分类名称")
    private String classifyName;

    /** 分类编号 */
    @Excel(name = "分类编号")
    private String classifyNo;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long sort;


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setClassifyName(String classifyName) 
    {
        this.classifyName = classifyName;
    }

    public String getClassifyName() 
    {
        return classifyName;
    }
    public void setClassifyNo(String classifyNo) 
    {
        this.classifyNo = classifyNo;
    }

    public String getClassifyNo() 
    {
        return classifyNo;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pid", getPid())
            .append("classifyName", getClassifyName())
            .append("classifyNo", getClassifyNo())
            .append("sort", getSort())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
