package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 外出管理对象 mms_asset_assetout
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Data
public class MmsAssetAssetout extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;

    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 编码 */
    @Excel(name = "编码")
    private String assetoutNo;

    /** 外出原因 */
    @Excel(name = "外出原因")
    private Integer outReason;

    /** 外出日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "外出日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date outTime;

    /** 申请单位 */
    @Excel(name = "申请单位")
    private Long unitId;

    /** 申请部门 */
    @Excel(name = "申请部门")
    private Long deptId;

    /** 申请人 */
    @Excel(name = "申请人")
    private Long userId;

    /** 预计返回日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预计返回日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planTime;

    /** 实际返回日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "实际返回日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date realTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAssetoutNo(String assetoutNo) 
    {
        this.assetoutNo = assetoutNo;
    }

    public String getAssetoutNo() 
    {
        return assetoutNo;
    }
    public void setOutReason(Integer outReason) 
    {
        this.outReason = outReason;
    }

    public Integer getOutReason() 
    {
        return outReason;
    }
    public void setOutTime(Date outTime) 
    {
        this.outTime = outTime;
    }

    public Date getOutTime() 
    {
        return outTime;
    }
    public void setUnitId(Long unitId) 
    {
        this.unitId = unitId;
    }

    public Long getUnitId() 
    {
        return unitId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPlanTime(Date planTime) 
    {
        this.planTime = planTime;
    }

    public Date getPlanTime() 
    {
        return planTime;
    }
    public void setRealTime(Date realTime) 
    {
        this.realTime = realTime;
    }

    public Date getRealTime() 
    {
        return realTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("assetoutNo", getAssetoutNo())
            .append("outReason", getOutReason())
            .append("outTime", getOutTime())
            .append("unitId", getUnitId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("planTime", getPlanTime())
            .append("realTime", getRealTime())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
