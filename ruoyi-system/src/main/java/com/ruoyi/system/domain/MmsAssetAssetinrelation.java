package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产入库单-关联资产id对象 mms_asset_assetinrelation
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public class MmsAssetAssetinrelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 资产入库单 */
    @Excel(name = "资产入库单")
    private Long assetinId;

    /** 资产id */
    @Excel(name = "资产id")
    private Long assetId;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 入库数量 */
    @Excel(name = "入库数量")
    private Long number;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String measurementUnit;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAssetinId(Long assetinId) 
    {
        this.assetinId = assetinId;
    }

    public Long getAssetinId() 
    {
        return assetinId;
    }
    public void setAssetId(Long assetId) 
    {
        this.assetId = assetId;
    }

    public Long getAssetId() 
    {
        return assetId;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setNumber(Long number) 
    {
        this.number = number;
    }

    public Long getNumber() 
    {
        return number;
    }
    public void setMeasurementUnit(String measurementUnit) 
    {
        this.measurementUnit = measurementUnit;
    }

    public String getMeasurementUnit() 
    {
        return measurementUnit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("assetinId", getAssetinId())
            .append("assetId", getAssetId())
            .append("price", getPrice())
            .append("number", getNumber())
            .append("measurementUnit", getMeasurementUnit())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
