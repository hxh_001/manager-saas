package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资产归还对象 mms_asset_return
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetReturn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户id */
    private Long merchantId;

    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 归还日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "归还日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date borrowDate;

    /** 有无损耗 */
    @Excel(name = "有无损耗")
    private Long retireLoss;

    /** 归还说明 */
    @Excel(name = "归还说明")
    private String returnDes;

    /** 归还单号 */
    @Excel(name = "归还单号")
    private String returnNo;

    /** 归还状态 */
    @Excel(name = "归还状态")
    private Long status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setBorrowDate(Date borrowDate) 
    {
        this.borrowDate = borrowDate;
    }

    public Date getBorrowDate() 
    {
        return borrowDate;
    }
    public void setRetireLoss(Long retireLoss) 
    {
        this.retireLoss = retireLoss;
    }

    public Long getRetireLoss() 
    {
        return retireLoss;
    }
    public void setReturnDes(String returnDes) 
    {
        this.returnDes = returnDes;
    }

    public String getReturnDes() 
    {
        return returnDes;
    }
    public void setReturnNo(String returnNo) 
    {
        this.returnNo = returnNo;
    }

    public String getReturnNo() 
    {
        return returnNo;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("borrowDate", getBorrowDate())
            .append("retireLoss", getRetireLoss())
            .append("returnDes", getReturnDes())
            .append("returnNo", getReturnNo())
            .append("status", getStatus())
            .toString();
    }
}
