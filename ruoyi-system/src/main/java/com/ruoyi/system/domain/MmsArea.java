package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 区域对象 mms_area
 *
 * @author ruoyi
 * @date 2024-08-26
 */
@TableName("mms_area")
public class MmsArea extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/** 父节点 */
	private Long pid;

	/** 区域名称 */
	@Excel(name = "区域名称")
	private String areaName;

	/** 区域编码 */
	@Excel(name = "区域编码")
	private String areaCode;

	/** 状态 1-有效，0-无效 */
	@Excel(name = "状态 1-有效，0-无效")
	private Integer status;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getPid() {
		return pid;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

}
