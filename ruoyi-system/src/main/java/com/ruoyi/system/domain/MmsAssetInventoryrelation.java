package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 资产盘库单-关联资产id对象 mms_asset_inventoryrelation
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Data
@TableName("mms_asset_inventoryrelation")
public class MmsAssetInventoryrelation extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 资产盘库单id
     */
    @Excel(name = "资产盘库单id")
    private Long inventoryId;

    /**
     * 资产id
     */
    @Excel(name = "资产id")
    private Long assetId;

    /**
     * 单价
     */
    @Excel(name = "单价")
    private BigDecimal price;

    /**
     * 数量
     */
    @Excel(name = "数量")
    private Long number;

    /**
     * 计量单位
     */
    @Excel(name = "计量单位")
    private String measurementUnit;

    //盘点情况(未盘、已盘、盘盈、盘亏)
    private int inventorySituation;

    //盘点方式(采集器盘点和手工盘点)
    private int  inventoryWay;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("inventoryId", getInventoryId())
                .append("assetId", getAssetId())
                .append("price", getPrice())
                .append("number", getNumber())
                .append("measurementUnit", getMeasurementUnit())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
