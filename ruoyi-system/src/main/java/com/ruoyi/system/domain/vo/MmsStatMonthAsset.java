package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 首页统计，月资产数量统计
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Data
public class MmsStatMonthAsset implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 月份
     */
    private List<String> month;

    /**
     * 数量
     */
    private List<Integer> addCount;

    /**
     * 每月创建数量
     */
    private List<Integer> createCount;
}
