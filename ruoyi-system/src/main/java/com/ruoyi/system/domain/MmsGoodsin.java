package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物品入库单对象 mms_goodsin
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public class MmsGoodsin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    private String goodsinNo;

    /** 入库人 */
    @Excel(name = "入库人")
    private String userId;

    /** 入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** 库房 */
    @Excel(name = "库房")
    private Integer addressId;

    /** 供应商 */
    @Excel(name = "供应商")
    private Integer supplierId;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal priceTotal;

    /** 物品样数 */
    @Excel(name = "物品样数")
    private Long number;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodsinNo(String goodsinNo) 
    {
        this.goodsinNo = goodsinNo;
    }

    public String getGoodsinNo() 
    {
        return goodsinNo;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setInTime(Date inTime) 
    {
        this.inTime = inTime;
    }

    public Date getInTime() 
    {
        return inTime;
    }
    public void setAddressId(Integer addressId) 
    {
        this.addressId = addressId;
    }

    public Integer getAddressId() 
    {
        return addressId;
    }
    public void setSupplierId(Integer supplierId) 
    {
        this.supplierId = supplierId;
    }

    public Integer getSupplierId() 
    {
        return supplierId;
    }
    public void setPriceTotal(BigDecimal priceTotal) 
    {
        this.priceTotal = priceTotal;
    }

    public BigDecimal getPriceTotal() 
    {
        return priceTotal;
    }
    public void setNumber(Long number) 
    {
        this.number = number;
    }

    public Long getNumber() 
    {
        return number;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsinNo", getGoodsinNo())
            .append("userId", getUserId())
            .append("inTime", getInTime())
            .append("addressId", getAddressId())
            .append("supplierId", getSupplierId())
            .append("remark", getRemark())
            .append("priceTotal", getPriceTotal())
            .append("number", getNumber())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
