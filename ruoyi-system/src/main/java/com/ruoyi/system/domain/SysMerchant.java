package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;



/**
 * 商户对象 sys_merchant
 *
 * @author ruoyi
 * @date 2024-07-15
 */
@TableName("sys_merchant")
public class SysMerchant extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 租户id */
	@TableId(value = "merchant_id", type = IdType.AUTO)
	private Long merchantId;

	/** 租户编号 */
	@Excel(name = "租户编号")
	private String merchantNo;

	/** 名称 */
	@Excel(name = "名称")
	private String name;

	/** 备注 */
	@Excel(name = "备注")
	private String content;

	/** 电话 */
	@Excel(name = "电话")
	private String phone;

	/** 状态 0禁用1启用 */
	@Excel(name = "状态 0禁用1启用")
	private Integer status;

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantNo(String merchantNo) {
		this.merchantNo = merchantNo;
	}

	public String getMerchantNo() {
		return merchantNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

}