package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 资产调拨对象 mms_asset_transfer
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Data
public class MmsAssetTransfer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    
    private Long id;

    /** 租户id */
    private Long merchantId;
    
    private String assetIds;

    @TableField(exist = false)
    private List<MmsAsset> assetList;

    /** 备注 */
    @Excel(name = "备注")
    private String backup;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 存放地点 */
    @Excel(name = "存放地点")
    private Long storge;

    /** 调拨日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "调拨日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date transferDate;

    /** 调拨说明 */
    @Excel(name = "调拨说明")
    private String transferDes;

    /** 调拨单号 */
    @Excel(name = "调拨单号")
    private String transferNo;

    /** 调拨原因 */
    @Excel(name = "调拨原因")
    private String transferReason;

    /** 调拨用途 */
    @Excel(name = "调拨用途")
    private String transferUse;

    /** 使用人 */
    @Excel(name = "使用人")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBackup(String backup) 
    {
        this.backup = backup;
    }

    public String getBackup() 
    {
        return backup;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setStorge(Long storge) 
    {
        this.storge = storge;
    }

    public Long getStorge() 
    {
        return storge;
    }
    public void setTransferDate(Date transferDate) 
    {
        this.transferDate = transferDate;
    }

    public Date getTransferDate() 
    {
        return transferDate;
    }
    public void setTransferDes(String transferDes) 
    {
        this.transferDes = transferDes;
    }

    public String getTransferDes() 
    {
        return transferDes;
    }
    public void setTransferNo(String transferNo) 
    {
        this.transferNo = transferNo;
    }

    public String getTransferNo() 
    {
        return transferNo;
    }
    public void setTransferReason(String transferReason) 
    {
        this.transferReason = transferReason;
    }

    public String getTransferReason() 
    {
        return transferReason;
    }
    public void setTransferUse(String transferUse) 
    {
        this.transferUse = transferUse;
    }

    public String getTransferUse() 
    {
        return transferUse;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("backup", getBackup())
            .append("status", getStatus())
            .append("storge", getStorge())
            .append("transferDate", getTransferDate())
            .append("transferDes", getTransferDes())
            .append("transferNo", getTransferNo())
            .append("transferReason", getTransferReason())
            .append("transferUse", getTransferUse())
            .append("userId", getUserId())
            .toString();
    }

	public String getAssetIds() {
		return assetIds;
	}

	public void setAssetIds(String assetIds) {
		this.assetIds = assetIds;
	}

	public List<MmsAsset> getAssetList() {
		return assetList;
	}

	public void setAssetList(List<MmsAsset> assetList) {
		this.assetList = assetList;
	}
}
