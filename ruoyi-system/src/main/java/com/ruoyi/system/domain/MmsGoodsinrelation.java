package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物品入库单-关联物品id对象 mms_goodsinrelation
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public class MmsGoodsinrelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品入库单 */
    @Excel(name = "物品入库单")
    private Long goodsinId;

    /** 物品id */
    @Excel(name = "物品id")
    private Long googsId;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 入库数量 */
    @Excel(name = "入库数量")
    private Long number;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodsinId(Long goodsinId) 
    {
        this.goodsinId = goodsinId;
    }

    public Long getGoodsinId() 
    {
        return goodsinId;
    }
    public void setGoogsId(Long googsId) 
    {
        this.googsId = googsId;
    }

    public Long getGoogsId() 
    {
        return googsId;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setNumber(Long number) 
    {
        this.number = number;
    }

    public Long getNumber() 
    {
        return number;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsinId", getGoodsinId())
            .append("googsId", getGoogsId())
            .append("price", getPrice())
            .append("number", getNumber())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
