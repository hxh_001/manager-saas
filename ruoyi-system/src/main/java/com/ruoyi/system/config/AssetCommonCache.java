package com.ruoyi.system.config;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.system.domain.MmsArea;
import com.ruoyi.system.mapper.MmsAreaMapper;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysDeptService;

@Service
public class AssetCommonCache {
	
	@Autowired
    private RedisCache redisCache;

	@Autowired
	private SysDeptMapper sysDeptMapper;
	
	@Autowired
	private SysUserMapper sysUserMapper;
	
	
	
	
	@Autowired
	private MmsAreaMapper mmsAreaMapper;
	
	
	public static Map<String,String> map = new ConcurrentHashMap<>(8);
	
	/**
	 *  初始化部门信息缓存
	 */
	public void initDeptCache( ) {
		if(!redisCache.hasKey(Constants.DEPT_REDIS_KEY)) {
			List<SysDept> depts = sysDeptMapper.selectList(null);
		  	for (int i = 0; i < depts.size(); i++) {
		  		SysDept dept = depts.get(i);
		  		redisCache.setCacheMapValue(Constants.DEPT_REDIS_KEY,dept.getDeptName(),dept);
			}
		}
	}
	
	/**
	 * 获取缓存中的部门信息
	 * 
	 * @param deptId
	 * @return
	 */
	public Map<String, SysDept> getUserDept() {
		this.initDeptCache();
		Map<String, SysDept> map = redisCache.getCacheMap(Constants.DEPT_REDIS_KEY);
		return map;
	}

	
//	/**
//	 * 获取缓存中的部门信息
//	 * 
//	 * @param deptId
//	 * @return
//	 */
//	public SysDept getUserDeptByName(String deptName) {
//		this.initDeptCache();
//		SysDept dept = redisCache.getCacheMapValue(Constants.DEPT_REDIS_KEY,deptName);
//		if(Objects.isNull(dept)) {
//			QueryWrapper<SysDept> query = new QueryWrapper<>();
//			query.eq("dept_name", deptName);
//			SysDept record = sysDeptMapper.selectOne(query);
//			if(Objects.nonNull(record)) {
//				redisCache.setCacheMapValue(Constants.DEPT_REDIS_KEY,record.getDeptName(),record);
//				return record;
//			}
//		}
//		return dept;
//	}

	/**
	 * 更新缓存中部门信息--新增/修改
	 * 
	 * @param deptId
	 * @return
	 */
	public void updateUserDeptById(SysDept dept) {
		if(Objects.nonNull(dept)) {
			redisCache.setCacheMapValue(Constants.DEPT_REDIS_KEY,dept.getDeptId()+"",dept);
		}
	}
	
	/**
	 * 删除缓存中部门信息--新增/修改
	 * 
	 * @param deptId
	 * @return
	 */
	public void deleteUserDeptById(Long deptId) {
		if(Objects.nonNull(deptId)) {
			redisCache.deleteCacheMapValue(Constants.DEPT_REDIS_KEY,deptId+"");
		}
	}
	
	
	/**
	 *  初始化区域信息缓存
	 */
	public void initAreaCache( ) {
		if(!redisCache.hasKey(Constants.AREA_REDIS_KEY)) {
			List<MmsArea> list = mmsAreaMapper.selectList(null);
		  	for (int i = 0; i < list.size(); i++) {
		  		MmsArea record = list.get(i);
		  		redisCache.setCacheMapValue(Constants.DEPT_REDIS_KEY,record.getAreaName(),record);
			}
		}
	}
	
	
	/**
	 * 删除区域缓存
	 */
	public void cleanAreaCache( ) {
		if(redisCache.hasKey(Constants.AREA_REDIS_KEY)) {
			redisCache.deleteObject(Constants.AREA_REDIS_KEY);
		}
	}
	
	/**
	 * 获取缓存中的区域信息
	 * 
	 * @param deptId
	 * @return
	 */
	public Map<String, MmsArea> getArea() {
		this.initAreaCache();
		Map<String, MmsArea> map = redisCache.getCacheMap(Constants.AREA_REDIS_KEY);
		return map;
	}
	
	
	/**
	 * 获取资产性质-集合
	 */
	public Map<String,String>  getAssetProp() {
		Map<String,String> map = new ConcurrentHashMap<>(8);
		
		List<SysDictData> list  = DictUtils.getDictCache("asset_prop");
		if(CollectionUtils.isEmpty(list)) {
			return map;
		}
		for (int i = 0; i < list.size(); i++) {
			SysDictData data = list.get(i);
			map.put(data.getDictLabel(), data.getDictValue());
		}
		return map ;
		
	}
	
	/**
	 * 获取资产取得方式-集合
	 */
	public Map<String,String>  getAssetGet() {
		Map<String,String> map = new ConcurrentHashMap<>(8);
		
		List<SysDictData> list  = DictUtils.getDictCache("asset_get");
		if(CollectionUtils.isEmpty(list)) {
			return map;
		}
		for (int i = 0; i < list.size(); i++) {
			SysDictData data = list.get(i);
			map.put(data.getDictLabel(), data.getDictValue());
		}
		return map ;
		
	}
	
	/**
	 * 获取资产存放地点-集合
	 */
	public Map<String,String>  getAssetStorage() {
		Map<String,String> map = new ConcurrentHashMap<>(32);
		
		List<SysDictData> list  = DictUtils.getDictCache("asset_storage");
		if(CollectionUtils.isEmpty(list)) {
			return map;
		}
		for (int i = 0; i < list.size(); i++) {
			SysDictData data = list.get(i);
			map.put(data.getDictLabel(), data.getDictValue());
		}
		return map ;
		
	}
	
	/**
	 *  初始化部门信息缓存
	 */
	public void initUserCache( ) {
		if(!redisCache.hasKey(Constants.USER_REDIS_KEY)) {
			List<SysUser> recordList = sysUserMapper.selectList(null);
		  	for (int i = 0; i < recordList.size(); i++) {
		  		SysUser sysUser = recordList.get(i);
		  		redisCache.setCacheMapValue(Constants.USER_REDIS_KEY,sysUser.getUserName(),sysUser);
			}
		}
	}
	
	/**
	 * 获取缓存中的部门信息
	 * 
	 * @param deptId
	 * @return
	 */
	public Map<String, SysUser> getUser() {
		this.initUserCache();
		Map<String, SysUser> map = redisCache.getCacheMap(Constants.USER_REDIS_KEY);
		return map;
	}
	
	
}
