package com.ruoyi.system.config;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
//开启异步支持
@EnableAsync
public class AsyncTaskConfig implements AsyncConfigurer {
	
	
  @Override
  public Executor getAsyncExecutor() {
      ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
      taskExecutor.setCorePoolSize(10);// 最小线程数
      taskExecutor.setMaxPoolSize(50);// 最大线程数
      taskExecutor.setQueueCapacity(500);// 等待队列

      taskExecutor.initialize();

      return taskExecutor;
  }
  @Override
  public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
      return null;
  }
}