package com.ruoyi.system.generator;

import lombok.Getter;

/**
 * 自动生成编号对应枚举
 */
@Getter
public enum GeneratorEnum {

    LY("领用", "mms_asset_receive", "transfer_no"),
    JY("借用", "mms_asset_borrow", "borrow_no"),
    //CZ("处置", "", ""),
    BF("报废", "mms_asset_scrap", "disposal_no"),
    WX("维修", "mms_asset_repair", "repair_no"),
    TK("退库", "mms_asset_retire", "retire_no"),
    GH("归还", "mms_asset_return", "return_no"),
    DB("调拨", "mms_asset_transfer", "transfer_no"),
    PD("盘点", "mms_asset_inventory", "inventory_no"),
    DK("调库", "", ""),
    WC("外出", "mms_asset_assetout", "assetout_no");
    //INFO("资产信息变更", "mms_asset_info_change", "");

    /**
     * 功能名称
     */
    private final String functionName;

    /**
     * 对应表名
     */
    private final String tableName;
    /**
     * 存入字段
     */
    private final String numberCol;

    GeneratorEnum(String functionName, String tableName, String numberCol) {
        this.functionName = functionName;
        this.tableName = tableName;
        this.numberCol = numberCol;
    }

    /**
     * @param type 类型
     * @return 根据类型查找，没找到抛出异常
     */
    public static GeneratorEnum fromType(String type) {
        for (GeneratorEnum value : GeneratorEnum.values()) {
            if (value.name().equals(type)) {
                return value;
            }
        }
        throw new IllegalArgumentException(type + " does not exist in the GeneratorEnum!");
    }
}
