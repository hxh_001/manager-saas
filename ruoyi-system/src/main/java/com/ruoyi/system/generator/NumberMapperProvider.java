package com.ruoyi.system.generator;

/**
 * @Author:gbm
 * @Date:2024/7/20
 */
public class NumberMapperProvider {
    public String queryNumber(String tableName, String numberCol, String prefix) {
        return new StringBuilder()
                .append("SELECT ")
                .append(numberCol)
                .append(" FROM ")
                .append(tableName)
                .append(" WHERE ")
                .append(numberCol)
                .append(" LIKE CONCAT('")
                .append(prefix)
                .append("', '%') ORDER BY CAST(SUBSTRING_INDEX(")
                .append(numberCol)
                .append(", '")
                .append(prefix)
                .append("', -1) AS UNSIGNED) DESC LIMIT 1")
                .toString();
    }
}
