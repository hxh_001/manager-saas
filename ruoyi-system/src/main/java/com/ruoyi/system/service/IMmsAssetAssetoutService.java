package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetAssetout;

/**
 * 外出管理Service接口
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public interface IMmsAssetAssetoutService 
{
    /**
     * 查询外出管理
     * 
     * @param id 外出管理主键
     * @return 外出管理
     */
    public MmsAssetAssetout selectMmsAssetAssetoutById(Long id);

    /**
     * 查询外出管理列表
     * 
     * @param mmsAssetAssetout 外出管理
     * @return 外出管理集合
     */
    public List<MmsAssetAssetout> selectMmsAssetAssetoutList(MmsAssetAssetout mmsAssetAssetout);

    /**
     * 新增外出管理
     * 
     * @param mmsAssetAssetout 外出管理
     * @return 结果
     */
    public int insertMmsAssetAssetout(MmsAssetAssetout mmsAssetAssetout);

    /**
     * 修改外出管理
     * 
     * @param mmsAssetAssetout 外出管理
     * @return 结果
     */
    public int updateMmsAssetAssetout(MmsAssetAssetout mmsAssetAssetout);

    /**
     * 批量删除外出管理
     * 
     * @param ids 需要删除的外出管理主键集合
     * @return 结果
     */
    public int deleteMmsAssetAssetoutByIds(Long[] ids);

    /**
     * 删除外出管理信息
     * 
     * @param id 外出管理主键
     * @return 结果
     */
    public int deleteMmsAssetAssetoutById(Long id);
}
