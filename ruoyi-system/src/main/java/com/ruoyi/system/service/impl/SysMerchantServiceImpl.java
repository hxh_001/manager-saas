package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysMerchantMapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysMerchant;
import com.ruoyi.system.service.ISysMerchantService;

/**
 * 商户Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-15
 */
@Service
public class SysMerchantServiceImpl extends ServiceImpl<SysMerchantMapper, SysMerchant> implements ISysMerchantService {
    @Autowired
    private SysMerchantMapper sysMerchantMapper;

    /**
     * 查询商户
     *
     * @param merchantId 商户主键
     * @return 商户
     */
    @Override
    public SysMerchant selectSysMerchantByMerchantId(Long merchantId) {
        return sysMerchantMapper.selectSysMerchantByMerchantId(merchantId);
    }

    /**
     * 查询商户列表
     *
     * @param sysMerchant 商户
     * @return 商户
     */
    @Override
    public List<SysMerchant> selectSysMerchantList(SysMerchant sysMerchant) {
        return sysMerchantMapper.selectSysMerchantList(sysMerchant);
    }

    /**
     * 新增商户
     *
     * @param sysMerchant 商户
     * @return 结果
     */
    @Override
    public int insertSysMerchant(SysMerchant sysMerchant) {
        sysMerchant.setCreateTime(DateUtils.getNowDate());
        return sysMerchantMapper.insertSysMerchant(sysMerchant);
    }

    /**
     * 修改商户
     *
     * @param sysMerchant 商户
     * @return 结果
     */
    @Override
    public int updateSysMerchant(SysMerchant sysMerchant) {
        sysMerchant.setUpdateTime(DateUtils.getNowDate());
        return sysMerchantMapper.updateSysMerchant(sysMerchant);
    }

    /**
     * 批量删除商户
     *
     * @param merchantIds 需要删除的商户主键
     * @return 结果
     */
    @Override
    public int deleteSysMerchantByMerchantIds(Long[] merchantIds) {
        return sysMerchantMapper.deleteSysMerchantByMerchantIds(merchantIds);
    }

    /**
     * 删除商户信息
     *
     * @param merchantId 商户主键
     * @return 结果
     */
    @Override
    public int deleteSysMerchantByMerchantId(Long merchantId) {
        return sysMerchantMapper.deleteSysMerchantByMerchantId(merchantId);
    }
}