package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetScrap;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.mapper.MmsAssetScrapMapper;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetScrapService;
import com.ruoyi.system.service.IMmsAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产报废Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetScrapServiceImpl implements IMmsAssetScrapService 
{
    @Autowired
    private MmsAssetScrapMapper mmsAssetScrapMapper;
    
    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;
    
    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产报废
     * 
     * @param id 资产报废主键
     * @return 资产报废
     */
    @Override
    public MmsAssetScrap selectMmsAssetScrapById(Long id)
    {
    	MmsAssetScrap model =  mmsAssetScrapMapper.selectMmsAssetScrapById(id);
        List<MmsAsset>  assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.SCRAP.getType());
    	model.setAssetList(assetList);
    	return model;
    }

    /**
     * 查询资产报废列表
     * 
     * @param mmsAssetScrap 资产报废
     * @return 资产报废
     */
    @Override
    public List<MmsAssetScrap> selectMmsAssetScrapList(MmsAssetScrap mmsAssetScrap)
    {
        return mmsAssetScrapMapper.selectMmsAssetScrapList(mmsAssetScrap);
    }

    /**
     * 新增资产报废
     * 
     * @param mmsAssetScrap 资产报废
     * @return 结果
     */
    @Override
    public int insertMmsAssetScrap(MmsAssetScrap mmsAssetScrap)
    {
        Optional.ofNullable(mmsAssetScrap).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetScrap.getDisposalDate()).orElseThrow(() -> new RuntimeException("报废日期为空"));
        Optional.ofNullable(mmsAssetScrap.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetScrap.setDisposalNo(generator.generateDocumentNumber(MmsAssetRelationEnum.SCRAP.getType()));
        mmsAssetScrap.setCreateTime(DateUtils.getNowDate());
        mmsAssetScrap.setCreateBy(getUsername());
        int count =  mmsAssetScrapMapper.insertMmsAssetScrap(mmsAssetScrap);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetScrap.getId(), mmsAssetScrap.getAssetIds(), MmsAssetRelationEnum.BORROW.getType());
        return count;
    }

    /**
     * 修改资产报废
     * 
     * @param mmsAssetScrap 资产报废
     * @return 结果
     */
    @Override
    public int updateMmsAssetScrap(MmsAssetScrap mmsAssetScrap)
    {
        Optional.ofNullable(mmsAssetScrap).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetScrap.getDisposalDate()).orElseThrow(() -> new RuntimeException("报废日期为空"));
        Optional.ofNullable(mmsAssetScrap.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetScrap.setUpdateTime(DateUtils.getNowDate());
        mmsAssetScrap.setUpdateBy(getUsername());
        return mmsAssetScrapMapper.updateMmsAssetScrap(mmsAssetScrap);
    }

    /**
     * 批量删除资产报废
     * 
     * @param ids 需要删除的资产报废主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetScrapByIds(Long[] ids)
    {
        return mmsAssetScrapMapper.deleteMmsAssetScrapByIds(ids);
    }

    /**
     * 删除资产报废信息
     * 
     * @param id 资产报废主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetScrapById(Long id)
    {
        return mmsAssetScrapMapper.deleteMmsAssetScrapById(id);
    }
}
