package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsInventoryOrderMapper;
import com.ruoyi.system.domain.MmsInventoryOrder;
import com.ruoyi.system.service.IMmsInventoryOrderService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 盘点管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsInventoryOrderServiceImpl implements IMmsInventoryOrderService 
{
    @Autowired
    private MmsInventoryOrderMapper mmsInventoryOrderMapper;

    /**
     * 查询盘点管理
     * 
     * @param id 盘点管理主键
     * @return 盘点管理
     */
    @Override
    public MmsInventoryOrder selectMmsInventoryOrderById(Long id)
    {
        return mmsInventoryOrderMapper.selectMmsInventoryOrderById(id);
    }

    /**
     * 查询盘点管理列表
     * 
     * @param mmsInventoryOrder 盘点管理
     * @return 盘点管理
     */
    @Override
    public List<MmsInventoryOrder> selectMmsInventoryOrderList(MmsInventoryOrder mmsInventoryOrder)
    {
        return mmsInventoryOrderMapper.selectMmsInventoryOrderList(mmsInventoryOrder);
    }

    /**
     * 新增盘点管理
     * 
     * @param mmsInventoryOrder 盘点管理
     * @return 结果
     */
    @Override
    public int insertMmsInventoryOrder(MmsInventoryOrder mmsInventoryOrder)
    {
        mmsInventoryOrder.setCreateTime(DateUtils.getNowDate());
        mmsInventoryOrder.setCreateBy(getUsername());
        return mmsInventoryOrderMapper.insertMmsInventoryOrder(mmsInventoryOrder);
    }

    /**
     * 修改盘点管理
     * 
     * @param mmsInventoryOrder 盘点管理
     * @return 结果
     */
    @Override
    public int updateMmsInventoryOrder(MmsInventoryOrder mmsInventoryOrder)
    {
        mmsInventoryOrder.setUpdateTime(DateUtils.getNowDate());
        mmsInventoryOrder.setUpdateBy(getUsername());
        return mmsInventoryOrderMapper.updateMmsInventoryOrder(mmsInventoryOrder);
    }

    /**
     * 批量删除盘点管理
     * 
     * @param ids 需要删除的盘点管理主键
     * @return 结果
     */
    @Override
    public int deleteMmsInventoryOrderByIds(Long[] ids)
    {
        return mmsInventoryOrderMapper.deleteMmsInventoryOrderByIds(ids);
    }

    /**
     * 删除盘点管理信息
     * 
     * @param id 盘点管理主键
     * @return 结果
     */
    @Override
    public int deleteMmsInventoryOrderById(Long id)
    {
        return mmsInventoryOrderMapper.deleteMmsInventoryOrderById(id);
    }
}
