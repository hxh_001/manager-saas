package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetBorrow;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.mapper.MmsAssetBorrowMapper;
import com.ruoyi.system.service.IMmsAssetBorrowService;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产借用Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetBorrowServiceImpl implements IMmsAssetBorrowService {
    @Autowired
    private MmsAssetBorrowMapper mmsAssetBorrowMapper;

    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;

    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产借用
     *
     * @param id 资产借用主键
     * @return 资产借用
     */
    @Override
    public MmsAssetBorrow selectMmsAssetBorrowById(Long id) {
        MmsAssetBorrow model = mmsAssetBorrowMapper.selectMmsAssetBorrowById(id);
        List<MmsAsset> assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.BORROW.getType());
        model.setAssetList(assetList);
        return model;
    }

    /**
     * 查询资产借用列表
     *
     * @param mmsAssetBorrow 资产借用
     * @return 资产借用
     */
    @Override
    public List<MmsAssetBorrow> selectMmsAssetBorrowList(MmsAssetBorrow mmsAssetBorrow) {
        return mmsAssetBorrowMapper.selectMmsAssetBorrowList(mmsAssetBorrow);
    }

    /**
     * 新增资产借用
     *
     * @param mmsAssetBorrow 资产借用
     * @return 结果
     */
    @Override
    public int insertMmsAssetBorrow(MmsAssetBorrow mmsAssetBorrow) {
        Optional.ofNullable(mmsAssetBorrow).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetBorrow.getBorrowDate()).orElseThrow(() -> new RuntimeException("借用日期为空"));
        Optional.ofNullable(mmsAssetBorrow.getUserId()).orElseThrow(() -> new RuntimeException("借用人为空"));
        Optional.ofNullable(mmsAssetBorrow.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetBorrow.setBorrowNo(generator.generateDocumentNumber(MmsAssetRelationEnum.BORROW.getType()));
        mmsAssetBorrow.setCreateTime(DateUtils.getNowDate());
        mmsAssetBorrow.setCreateBy(getUsername());
        int count = mmsAssetBorrowMapper.insertMmsAssetBorrow(mmsAssetBorrow);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetBorrow.getId(), mmsAssetBorrow.getAssetIds(), MmsAssetRelationEnum.BORROW.getType());
        return count;
    }

    /**
     * 修改资产借用
     *
     * @param mmsAssetBorrow 资产借用
     * @return 结果
     */
    @Override
    public int updateMmsAssetBorrow(MmsAssetBorrow mmsAssetBorrow) {
        Optional.ofNullable(mmsAssetBorrow).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetBorrow.getBorrowDate()).orElseThrow(() -> new RuntimeException("借用日期为空"));
        Optional.ofNullable(mmsAssetBorrow.getUserId()).orElseThrow(() -> new RuntimeException("借用人为空"));
        Optional.ofNullable(mmsAssetBorrow.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetBorrow.setUpdateTime(DateUtils.getNowDate());
        mmsAssetBorrow.setUpdateBy(getUsername());
        return mmsAssetBorrowMapper.updateMmsAssetBorrow(mmsAssetBorrow);
    }

    /**
     * 批量删除资产借用
     *
     * @param ids 需要删除的资产借用主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetBorrowByIds(Long[] ids) {
        return mmsAssetBorrowMapper.deleteMmsAssetBorrowByIds(ids);
    }

    /**
     * 删除资产借用信息
     *
     * @param id 资产借用主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetBorrowById(Long id) {
        return mmsAssetBorrowMapper.deleteMmsAssetBorrowById(id);
    }
}
