package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetTransfer;

/**
 * 资产调拨Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetTransferService 
{
    /**
     * 查询资产调拨
     * 
     * @param id 资产调拨主键
     * @return 资产调拨
     */
    public MmsAssetTransfer selectMmsAssetTransferById(Long id);

    /**
     * 查询资产调拨列表
     * 
     * @param mmsAssetTransfer 资产调拨
     * @return 资产调拨集合
     */
    public List<MmsAssetTransfer> selectMmsAssetTransferList(MmsAssetTransfer mmsAssetTransfer);

    /**
     * 新增资产调拨
     * 
     * @param mmsAssetTransfer 资产调拨
     * @return 结果
     */
    public int insertMmsAssetTransfer(MmsAssetTransfer mmsAssetTransfer);

    /**
     * 修改资产调拨
     * 
     * @param mmsAssetTransfer 资产调拨
     * @return 结果
     */
    public int updateMmsAssetTransfer(MmsAssetTransfer mmsAssetTransfer);

    /**
     * 批量删除资产调拨
     * 
     * @param ids 需要删除的资产调拨主键集合
     * @return 结果
     */
    public int deleteMmsAssetTransferByIds(Long[] ids);

    /**
     * 删除资产调拨信息
     * 
     * @param id 资产调拨主键
     * @return 结果
     */
    public int deleteMmsAssetTransferById(Long id);
}
