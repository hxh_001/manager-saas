package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetAssetout;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.mapper.MmsAssetAssetoutMapper;
import com.ruoyi.system.service.IMmsAssetAssetoutService;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 外出管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Service
public class MmsAssetAssetoutServiceImpl implements IMmsAssetAssetoutService 
{
    @Autowired
    private MmsAssetAssetoutMapper mmsAssetAssetoutMapper;

    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;

    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询外出管理
     * 
     * @param id 外出管理主键
     * @return 外出管理
     */
    @Override
    public MmsAssetAssetout selectMmsAssetAssetoutById(Long id)
    {
        MmsAssetAssetout model =  mmsAssetAssetoutMapper.selectMmsAssetAssetoutById(id);
        List<MmsAsset>  assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.OUTGOINT.getType());
        model.setAssetList(assetList);
        return model;
    }

    /**
     * 查询外出管理列表
     * 
     * @param mmsAssetAssetout 外出管理
     * @return 外出管理
     */
    @Override
    public List<MmsAssetAssetout> selectMmsAssetAssetoutList(MmsAssetAssetout mmsAssetAssetout)
    {
        return mmsAssetAssetoutMapper.selectMmsAssetAssetoutList(mmsAssetAssetout);
    }

    /**
     * 新增外出管理
     * 
     * @param mmsAssetAssetout 外出管理
     * @return 结果
     */
    @Override
    public int insertMmsAssetAssetout(MmsAssetAssetout mmsAssetAssetout)
    {
        Optional.ofNullable(mmsAssetAssetout).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetAssetout.getOutTime()).orElseThrow(() -> new RuntimeException("外出日期为空"));
        Optional.ofNullable(mmsAssetAssetout.getUserId()).orElseThrow(() -> new RuntimeException("申请人为空"));
        Optional.ofNullable(mmsAssetAssetout.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetAssetout.setAssetoutNo(generator.generateDocumentNumber(MmsAssetRelationEnum.OUTGOINT.getType()));
        mmsAssetAssetout.setCreateTime(DateUtils.getNowDate());
        mmsAssetAssetout.setCreateBy(getUsername());
        int count = mmsAssetAssetoutMapper.insertMmsAssetAssetout(mmsAssetAssetout);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetAssetout.getId(), mmsAssetAssetout.getAssetIds(), MmsAssetRelationEnum.OUTGOINT.getType());
        return count;
    }

    /**
     * 修改外出管理
     * 
     * @param mmsAssetAssetout 外出管理
     * @return 结果
     */
    @Override
    public int updateMmsAssetAssetout(MmsAssetAssetout mmsAssetAssetout)
    {
        Optional.ofNullable(mmsAssetAssetout).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetAssetout.getOutTime()).orElseThrow(() -> new RuntimeException("外出日期为空"));
        Optional.ofNullable(mmsAssetAssetout.getUserId()).orElseThrow(() -> new RuntimeException("申请人为空"));
        Optional.ofNullable(mmsAssetAssetout.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetAssetout.setUpdateTime(DateUtils.getNowDate());
        mmsAssetAssetout.setUpdateBy(getUsername());
        return mmsAssetAssetoutMapper.updateMmsAssetAssetout(mmsAssetAssetout);
    }

    /**
     * 批量删除外出管理
     * 
     * @param ids 需要删除的外出管理主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetAssetoutByIds(Long[] ids)
    {
        return mmsAssetAssetoutMapper.deleteMmsAssetAssetoutByIds(ids);
    }

    /**
     * 删除外出管理信息
     * 
     * @param id 外出管理主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetAssetoutById(Long id)
    {
        return mmsAssetAssetoutMapper.deleteMmsAssetAssetoutById(id);
    }
}
