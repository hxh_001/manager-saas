package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetRetire;

/**
 * 资产退库Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetRetireService 
{
    /**
     * 查询资产退库
     * 
     * @param id 资产退库主键
     * @return 资产退库
     */
    public MmsAssetRetire selectMmsAssetRetireById(Long id);

    /**
     * 查询资产退库列表
     * 
     * @param mmsAssetRetire 资产退库
     * @return 资产退库集合
     */
    public List<MmsAssetRetire> selectMmsAssetRetireList(MmsAssetRetire mmsAssetRetire);

    /**
     * 新增资产退库
     * 
     * @param mmsAssetRetire 资产退库
     * @return 结果
     */
    public int insertMmsAssetRetire(MmsAssetRetire mmsAssetRetire);

    /**
     * 修改资产退库
     * 
     * @param mmsAssetRetire 资产退库
     * @return 结果
     */
    public int updateMmsAssetRetire(MmsAssetRetire mmsAssetRetire);

    /**
     * 批量删除资产退库
     * 
     * @param ids 需要删除的资产退库主键集合
     * @return 结果
     */
    public int deleteMmsAssetRetireByIds(Long[] ids);

    /**
     * 删除资产退库信息
     * 
     * @param id 资产退库主键
     * @return 结果
     */
    public int deleteMmsAssetRetireById(Long id);
}
