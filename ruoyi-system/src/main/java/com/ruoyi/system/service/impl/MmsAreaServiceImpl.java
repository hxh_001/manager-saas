package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAreaMapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsArea;
import com.ruoyi.system.service.IMmsAreaService;

/**
 * 区域Service业务层处理
 *
 * @author ruoyi
 * @date 2024-08-26
 */
@Service
public class MmsAreaServiceImpl extends ServiceImpl<MmsAreaMapper, MmsArea> implements IMmsAreaService {
    @Autowired
    private MmsAreaMapper mmsAreaMapper;

    /**
     * 查询区域
     *
     * @param id 区域主键
     * @return 区域
     */
    @Override
    public MmsArea selectMmsAreaById(Long id) {
        return mmsAreaMapper.selectMmsAreaById(id);
    }

    /**
     * 查询区域列表
     *
     * @param mmsArea 区域
     * @return 区域
     */
    @Override
    public List<MmsArea> selectMmsAreaList(MmsArea mmsArea) {
        return mmsAreaMapper.selectMmsAreaList(mmsArea);
    }

    /**
     * 新增区域
     *
     * @param mmsArea 区域
     * @return 结果
     */
    @Override
    public int insertMmsArea(MmsArea mmsArea) {
        mmsArea.setCreateTime(DateUtils.getNowDate());
        return mmsAreaMapper.insertMmsArea(mmsArea);
    }

    /**
     * 修改区域
     *
     * @param mmsArea 区域
     * @return 结果
     */
    @Override
    public int updateMmsArea(MmsArea mmsArea) {
        return mmsAreaMapper.updateMmsArea(mmsArea);
    }

    /**
     * 批量删除区域
     *
     * @param ids 需要删除的区域主键
     * @return 结果
     */
    @Override
    public int deleteMmsAreaByIds(Long[] ids) {
        return mmsAreaMapper.deleteMmsAreaByIds(ids);
    }

    /**
     * 删除区域信息
     *
     * @param id 区域主键
     * @return 结果
     */
    @Override
    public int deleteMmsAreaById(Long id) {
        return mmsAreaMapper.deleteMmsAreaById(id);
    }
}