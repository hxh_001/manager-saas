package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetAssetin;
import com.ruoyi.system.domain.MmsAssetRelation;
import com.ruoyi.system.domain.vo.MmsAssetStat;
import com.ruoyi.system.domain.vo.MmsStatMonthAsset;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.enums.MmsAssetStatusEnum;
import com.ruoyi.system.mapper.MmsAssetAssetinMapper;
import com.ruoyi.system.mapper.MmsAssetClassifyMapper;
import com.ruoyi.system.mapper.MmsAssetMapper;
import com.ruoyi.system.mapper.MmsAssetRelationMapper;
import com.ruoyi.system.service.IMmsAssetService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.Validator;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-18
 */
@Service
public class MmsAssetServiceImpl extends ServiceImpl<MmsAssetMapper, MmsAsset> implements IMmsAssetService {
    private static final Logger log = LoggerFactory.getLogger(MmsAssetServiceImpl.class);

    @Resource
    private MmsAssetMapper mmsAssetMapper;

    @Autowired
    protected Validator validator;

    @Resource
    private MmsAssetMapper assetMapper;

    @Resource
    private MmsAssetAssetinMapper assetinMapper;

    @Resource
    private MmsAssetClassifyMapper classifyMapper;

    @Resource
    private MmsAssetRelationMapper relationMapper;
    
    
    @Autowired
    private MmsAssetNoRecordService mmsAssetNoRecordService;

    /**
     * 查询资产信息
     *
     * @param id 资产信息主键
     * @return 资产信息
     */
    @Override
    public MmsAsset selectMmsAssetById(Long id) {
        return mmsAssetMapper.selectMmsAssetById(id);
    }

    @Override
    public List<MmsAsset> selectMmsAssetByRelationId(Long functionId, String assetFunction) {
        return mmsAssetMapper.selectMmsAssetByRelationId(functionId, assetFunction);
    }

    /**
     * 查询资产信息列表
     *
     * @param mmsAsset 资产信息
     * @return 资产信息
     */
    @Override
    public List<MmsAsset> selectMmsAssetList(MmsAsset mmsAsset) {
        return mmsAssetMapper.selectMmsAssetList(mmsAsset);
    }

    /**
     * 新增资产信息
     *
     * @param mmsAsset 资产信息
     * @return 结果
     */
    @Override
    public int insertMmsAsset(MmsAsset mmsAsset) {
        if (StringUtils.isEmpty(mmsAsset.getAssetNo())) {
        	String assetNo = mmsAssetNoRecordService.queryNextNo();
            mmsAsset.setAssetNo(assetNo);
        }
        Optional.ofNullable(mmsAsset).orElseThrow(() -> new RuntimeException("参数为空"));
        //Optional.ofNullable(mmsAsset.getClassifyId()).orElseThrow(() -> new RuntimeException("资产分类为空"));
        
        if(StringUtils.isNotBlank(mmsAsset.getInStock()) && "Y".equalsIgnoreCase(mmsAsset.getInStock())) {
        	   mmsAsset.setStatus(MmsAssetStatusEnum.IN_STOCK.getStatus());
        }else {
        	 mmsAsset.setStatus(MmsAssetStatusEnum.WAITING_ACCEPTANCE.getStatus());
        }
     

      
        
        mmsAsset.setCreateTime(DateUtils.getNowDate());
        mmsAsset.setCreateBy(getUsername());
        return mmsAssetMapper.insertMmsAsset(mmsAsset);
    }

    /**
     * 修改资产信息
     *
     * @param mmsAsset 资产信息
     * @return 结果
     */
    @Override
    public int updateMmsAsset(MmsAsset mmsAsset) {
        mmsAsset.setUpdateTime(DateUtils.getNowDate());
        mmsAsset.setUpdateBy(getUsername());
        return mmsAssetMapper.updateMmsAsset(mmsAsset);
    }

    /**
     * 批量删除资产信息
     *
     * @param ids 需要删除的资产信息主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetByIds(Long[] ids) {
        return mmsAssetMapper.deleteMmsAssetByIds(ids);
    }

    /**
     * 删除资产信息信息
     *
     * @param id 资产信息主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetById(Long id) {
        return mmsAssetMapper.deleteMmsAssetById(id);
    }

//    /**						
//     * 导入资产数据
//     *
//     * @param assetList 资产数据列表
//     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
//     * @param operName 操作用户
//     * @return 结果
//     * @throws Exception 
//     */
//    @Override
//    public void importAsset(List<MmsAsset> assetList, Boolean isUpdateSupport) throws Exception
//    {
//        System.out.println("start:"+new Date());
//    	if (StringUtils.isNull(assetList) || assetList.size() == 0)
//        {
//            throw new ServiceException("导入资产数据不能为空！");
//        }
//       // SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
//        try {
//        	//MmsAssetMapper assetMapper = sqlSession.getMapper(MmsAssetMapper.class);
//        	assetList.stream().forEach(mmsAsset -> mmsAssetMapper.insertMmsAsset(mmsAsset));
//            // 提交数据
//           /// sqlSession.commit();
//        } catch (Exception e) {
//            sqlSession.rollback();
//        } finally {
//            sqlSession.close();
//        }
//        System.out.println("end:"+new Date());
//
//    }
//    

    /**
     * 导入资产数据
     *
     * @param assetList 资产数据列表
     * @return 结果
     * @throws Exception
     */
    @Async
    public void importAsset(List<MmsAsset> assetList, CountDownLatch countDownLatch) {
        try {
            //log.warn("start executeAsync");
        	log.warn("thread:"+Thread.currentThread().getName());
        	this.saveBatch(assetList);
            //log.warn("end executeAsync");
        } finally {
            countDownLatch.countDown();// 很关键, 无论上面程序是否异常必须执行countDown,否则await无法释放
        }
    }


    /**
     * 生成资产编号
     */
    public String createAssetNo() {
    	
        return UUID.randomUUID().toString();
    }


    /**
     * 首页统计固定资产
     *
     * @return
     */
    @Override
    public MmsAssetStat statAsset() {
        MmsAssetStat ret = new MmsAssetStat();
        //统计资产清单
        List<MmsAsset> assetList = assetMapper.selectMmsAssetList(new MmsAsset());
        ret.setTotal(assetList.size());

        //入库
        List<MmsAssetAssetin> assetins = assetinMapper.selectMmsAssetAssetinList(new MmsAssetAssetin());
        ret.setInStock(assetins.size());

        //全部单据和资产数据
        List<MmsAssetRelation> relations = relationMapper.selectMmsAssetRelationList(new MmsAssetRelation());
        if (relations.size() == 0) {
            return ret;
        }
        relations = relations.stream().filter(e -> e.getAssetFunction() != null).collect(Collectors.toList());
        if (relations.size() == 0) {
            return ret;
        }

        //统计资产关联表中的计算
        Map<String, List<MmsAssetRelation>> functionMap = relations.stream().collect(Collectors.groupingBy(MmsAssetRelation::getAssetFunction));
        functionMap.forEach((key, value) -> {
            if (MmsAssetRelationEnum.BORROW.getType().equals(key)) {
                ret.setInBorrow(value.size()); //在借
            } else if (MmsAssetRelationEnum.REPAIR.getType().equals(key)) {
                ret.setInRepair(value.size()); //在修
            } else if (MmsAssetRelationEnum.SCRAP.getType().equals(key)) {
                ret.setScrap(value.size()); //报废
            } else if (MmsAssetRelationEnum.OUTGOINT.getType().equals(key)) {
                ret.setInOut(value.size()); //外出
            }
        });

        //在用：TODO 公式是什么
        Integer inUse = 0;

        ret.setInUse(inUse);
        return ret;
    }

    /**
     * 首页统计月资产
     *
     * @return
     */
    @Override
    public MmsStatMonthAsset statMonth() {
        MmsStatMonthAsset ret = new MmsStatMonthAsset();
        //统计资产清单
        List<MmsAsset> assetList = assetMapper.selectMmsAssetList(new MmsAsset());
        if (assetList.size() == 0) {
            return ret;
        }
        assetList = assetList.stream().filter(e -> e.getCreateTime() != null).collect(Collectors.toList());


        DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("MMMM", Locale.getDefault());
        Map<String, Integer> monthCounts = new TreeMap<>();
        Map<String, Integer> createCounts = new TreeMap<>();
        List<String> month = new ArrayList<>();


        // 初始化前12个月的计数为0
        LocalDate currentMonth = LocalDate.now().withDayOfMonth(1);
        for (int i = 0; i < 12; i++) {
            String monthName = currentMonth.minusMonths(i).format(MONTH_FORMATTER);
            monthCounts.put(monthName, 0);
            createCounts.put(monthName, 0);
            month.add(monthName);
        }
        ret.setMonth(month);
        //资产如果为空，则全部返回空
        if (assetList.size() == 0) {
            ret.setAddCount(new ArrayList<>(monthCounts.values()));
            ret.setCreateCount(new ArrayList<>(createCounts.values()));
            return ret;
        }

        for (MmsAsset asset : assetList) {
            Instant instant = asset.getCreateTime().toInstant();
            LocalDate createTime = instant.atZone(ZoneId.systemDefault()).toLocalDate();
            if (createTime != null) {
                String monthName = createTime.withDayOfMonth(1).format(MONTH_FORMATTER);
                monthCounts.put(monthName, monthCounts.getOrDefault(monthName, 0) + 1);

                // 更新每月创建的资产数量
                if (createTime.isAfter(currentMonth.minusMonths(12)) && createTime.isBefore(currentMonth.plusMonths(1))) {
                    createCounts.put(monthName, createCounts.getOrDefault(monthName, 0) + 1);
                }
            }
        }

        //统计数量，按照月度数据
        List<Integer> addCount = new ArrayList<>();
        List<Integer> createCount = new ArrayList<>();
        month.forEach(e -> {
            addCount.add(monthCounts.get(e));
            createCount.add(createCounts.get(e));
        });
        ret.setAddCount(addCount);
        ret.setCreateCount(createCount);
        return ret;
    }
}
