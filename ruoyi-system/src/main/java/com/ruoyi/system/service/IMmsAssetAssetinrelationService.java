package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetAssetinrelation;

/**
 * 资产入库单-关联资产idService接口
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public interface IMmsAssetAssetinrelationService 
{
    /**
     * 查询资产入库单-关联资产id
     * 
     * @param id 资产入库单-关联资产id主键
     * @return 资产入库单-关联资产id
     */
    public MmsAssetAssetinrelation selectMmsAssetAssetinrelationById(Long id);

    /**
     * 查询资产入库单-关联资产id列表
     * 
     * @param mmsAssetAssetinrelation 资产入库单-关联资产id
     * @return 资产入库单-关联资产id集合
     */
    public List<MmsAssetAssetinrelation> selectMmsAssetAssetinrelationList(MmsAssetAssetinrelation mmsAssetAssetinrelation);

    /**
     * 新增资产入库单-关联资产id
     * 
     * @param mmsAssetAssetinrelation 资产入库单-关联资产id
     * @return 结果
     */
    public int insertMmsAssetAssetinrelation(MmsAssetAssetinrelation mmsAssetAssetinrelation);

    /**
     * 修改资产入库单-关联资产id
     * 
     * @param mmsAssetAssetinrelation 资产入库单-关联资产id
     * @return 结果
     */
    public int updateMmsAssetAssetinrelation(MmsAssetAssetinrelation mmsAssetAssetinrelation);

    /**
     * 批量删除资产入库单-关联资产id
     * 
     * @param ids 需要删除的资产入库单-关联资产id主键集合
     * @return 结果
     */
    public int deleteMmsAssetAssetinrelationByIds(Long[] ids);

    /**
     * 删除资产入库单-关联资产id信息
     * 
     * @param id 资产入库单-关联资产id主键
     * @return 结果
     */
    public int deleteMmsAssetAssetinrelationById(Long id);
}
