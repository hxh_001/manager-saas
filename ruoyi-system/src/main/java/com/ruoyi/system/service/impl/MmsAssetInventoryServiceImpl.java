package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetInventory;
import com.ruoyi.system.domain.MmsAssetInventoryrelation;
import com.ruoyi.system.enums.*;
import com.ruoyi.system.mapper.MmsAssetInventoryMapper;
import com.ruoyi.system.service.IMmsAssetInventoryService;
import com.ruoyi.system.service.IMmsAssetInventoryrelationService;
import com.ruoyi.system.service.IMmsAssetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产盘点Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@Service
@Slf4j
public class MmsAssetInventoryServiceImpl extends ServiceImpl<MmsAssetInventoryMapper, MmsAssetInventory> implements IMmsAssetInventoryService {
    @Resource
    private MmsAssetInventoryMapper mmsAssetInventoryMapper;

    @Resource
    private IMmsAssetService assetService;

    @Resource
    private IMmsAssetInventoryrelationService inventoryrelationService;

    @Resource
    private Generator generator;

    /**
     * 查询资产盘点
     *
     * @param id 资产盘点主键
     * @return 资产盘点
     */
    @Override
    public MmsAssetInventory selectMmsAssetInventoryById(Long id) {
        return mmsAssetInventoryMapper.selectMmsAssetInventoryById(id);
    }

    /**
     * 查询资产盘点列表
     *
     * @param mmsAssetInventory 资产盘点
     * @return 资产盘点
     */
    @Override
    public List<MmsAssetInventory> selectMmsAssetInventoryList(MmsAssetInventory mmsAssetInventory) {
        return mmsAssetInventoryMapper.selectMmsAssetInventoryList(mmsAssetInventory);
    }

    private boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        // 比较年、月、日
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 新增资产盘点
     *
     * @param mmsAssetInventory 资产盘点
     * @return 结果
     */
    @Override
    public int insertMmsAssetInventory(MmsAssetInventory mmsAssetInventory) {
        Optional.ofNullable(mmsAssetInventory.getInventoryType()).orElseThrow(() -> new RuntimeException("盘点类型为空"));
        Optional.ofNullable(mmsAssetInventory.getTaskName()).orElseThrow(() -> new RuntimeException("任务名称为空"));
        Optional.ofNullable(mmsAssetInventory.getStartDate()).orElseThrow(() -> new RuntimeException("盘点开始时间为空"));

        //和今天比较，开始盘点时间必须晚于今天
        if (mmsAssetInventory.getStartDate().before(new Date())) {
            throw new RuntimeException("开始时间必须晚于今天");
        }
        Optional.ofNullable(mmsAssetInventory.getInventoryUser()).orElseThrow(() -> new RuntimeException("盘点人为空"));

        if (mmsAssetInventory.getInventoryType() == InventoryType.part.getCode()) {
            Optional.ofNullable(mmsAssetInventory.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        }

        if (mmsAssetInventory.getInventoryType() == InventoryType.all.getCode()) {
            Optional.ofNullable(mmsAssetInventory.getEndDate()).orElseThrow(() -> new RuntimeException("盘点结束时间为空"));
            if (isSameDay(mmsAssetInventory.getStartDate(), mmsAssetInventory.getEndDate()) || mmsAssetInventory.getStartDate().after(mmsAssetInventory.getEndDate())) {
                throw new RuntimeException("开始时间必须早于结束时间");
            }
        }
        mmsAssetInventory.setInventoryStatus(InventoryStatus.undo.getCode()); //默认为未开始
        mmsAssetInventory.setInventoryNo(generator.generateDocumentNumber(MmsAssetRelationEnum.INVENTORY.getType()));
        mmsAssetInventory.setCreateTime(DateUtils.getNowDate());
        mmsAssetInventory.setCreateBy(getUsername());
        int id = mmsAssetInventoryMapper.insertMmsAssetInventory(mmsAssetInventory);

        //如果今天就是盘点开始时间
        if (isSameDay(mmsAssetInventory.getStartDate(), new Date())) {
            List<MmsAsset> assetList = new ArrayList<>();
            int inventorySituation = InventorySituation.undo.getCode();
            int inventoryStatus = InventoryStatus.undo.getCode();
            //如果部分盘点
            if (mmsAssetInventory.getInventoryType() == InventoryType.part.getCode()) {
                List<String> list = Arrays.asList(mmsAssetInventory.getAssetIds().split(","));
                List<Long> queryList = new ArrayList<>();
                list.forEach(e -> {
                    queryList.add(Long.valueOf(e));
                });
                assetList = assetService.list(new QueryWrapper<MmsAsset>().in("id", queryList));
                if (assetList.size() == 0) {
                    log.info("查询资产为空. InventoryId:[{}], assetIds:[{}]", mmsAssetInventory.getId(), StringUtils.join(mmsAssetInventory.getAssetIds(), ","));
                }
                inventorySituation = InventorySituation.done.getCode();
                inventoryStatus = InventoryStatus.done.getCode();
            }
            //如果是全部盘点
            else {
                //获取所有在库的资产
                MmsAsset asset = new MmsAsset();
                asset.setStatus(MmsAssetStatusEnum.IN_STOCK.getStatus());
                assetList = assetService.selectMmsAssetList(asset);
                inventorySituation = InventorySituation.undo.getCode();
                inventoryStatus = InventoryStatus.doing.getCode();
            }

            //增加资产信息
            if (assetList.size() > 0) {
                List<MmsAssetInventoryrelation> inventoryrelations = new ArrayList<>();
                for (MmsAsset e : assetList) {
                    MmsAssetInventoryrelation temp = new MmsAssetInventoryrelation();
                    temp.setAssetId(e.getId());
                    temp.setInventoryId(mmsAssetInventory.getId());
                    temp.setMeasurementUnit(e.getMeasureUnit());
                    temp.setCreateBy(getUsername());
                    temp.setCreateTime(DateUtils.getNowDate());
                    temp.setInventorySituation(inventorySituation);
                    inventoryrelations.add(temp);
                }
                inventoryrelationService.saveBatch(inventoryrelations);
            }

            //修改盘点状态
            mmsAssetInventory.setInventoryStatus(inventoryStatus);
            mmsAssetInventoryMapper.updateById(mmsAssetInventory);
        }
        return id;
    }

    /**
     * 修改资产盘点
     *
     * @param mmsAssetInventory 资产盘点
     * @return 结果
     */
    @Override
    public int updateMmsAssetInventory(MmsAssetInventory mmsAssetInventory) {
        mmsAssetInventory.setUpdateTime(DateUtils.getNowDate());
        mmsAssetInventory.setUpdateBy(getUsername());
        return mmsAssetInventoryMapper.updateMmsAssetInventory(mmsAssetInventory);
    }

    /**
     * 批量删除资产盘点
     *
     * @param ids 需要删除的资产盘点主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetInventoryByIds(Long[] ids) {
        return mmsAssetInventoryMapper.deleteMmsAssetInventoryByIds(ids);
    }

    /**
     * 删除资产盘点信息
     *
     * @param id 资产盘点主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetInventoryById(Long id) {
        return mmsAssetInventoryMapper.deleteMmsAssetInventoryById(id);
    }

    /**
     * 新增盘点资产明细信息
     *
     * @param inventoryId
     * @param inventorySituation
     * @param assetList
     */
    private void newInventoryrelation(Long inventoryId, int inventorySituation, List<MmsAsset> assetList) {
        List<MmsAssetInventoryrelation> inventoryrelations = new ArrayList<>();
        for (MmsAsset e : assetList) {
            MmsAssetInventoryrelation temp = new MmsAssetInventoryrelation();
            temp.setAssetId(e.getId());
            temp.setInventoryId(inventoryId);
            temp.setMeasurementUnit(e.getMeasureUnit());
            temp.setCreateBy(getUsername());
            temp.setCreateTime(DateUtils.getNowDate());
            temp.setInventorySituation(inventorySituation);
            inventoryrelations.add(temp);
        }
        inventoryrelationService.saveBatch(inventoryrelations);
    }

    /**
     * 新增盘点资产明细信息
     *
     * @param inventoryId
     * @param inventorySituation
     * @param assetids
     */
    private void newInventoryrelationByAssetIds(Long inventoryId, int inventorySituation, List<Long> assetids) {
        List<MmsAssetInventoryrelation> inventoryrelations = new ArrayList<>();
        for (Long e : assetids) {
            MmsAssetInventoryrelation temp = new MmsAssetInventoryrelation();
            temp.setAssetId(e);
            temp.setInventoryId(inventoryId);
            temp.setMeasurementUnit(null);
            temp.setCreateBy(getUsername());
            temp.setCreateTime(DateUtils.getNowDate());
            temp.setInventorySituation(inventorySituation);
            inventoryrelations.add(temp);
        }
        inventoryrelationService.saveBatch(inventoryrelations);
    }

    //定时盘点
    //定时任务，每天凌晨2点执行
    @Scheduled(cron = "0 0 2 * * ?")
    public void scheduleInventory() {
        log.info("welcome! 自动盘点!   时间:[{}]", DateUtils.getNowDate());
        //查询所有没有结束的盘点
        List<MmsAssetInventory> inventories = this.list(new QueryWrapper<MmsAssetInventory>()
                .ne("inventory_status", InventoryStatus.done.getCode()));
        if (inventories.size() == 0) {
            log.info("bye ! 已全部盘点，无盘点任务，不需处理!   时间:[{}]", DateUtils.getNowDate());
            return;
        }

        //比较开始时间
        Date now = new Date();
        List<MmsAssetInventory> collect = inventories.stream().filter(e -> (null != e.getStartDate() && isSameDay(e.getStartDate(), now)) || (null != e.getEndDate() && isSameDay(e.getEndDate(), now))).collect(Collectors.toList());
        if (collect.size() == 0) {
            log.info("bye ! 无今天盘点的任务，不需处理!   时间:[{}]", DateUtils.getNowDate());
            return;
        }

        //获取所有在库的资产
        MmsAsset asset = new MmsAsset();
        asset.setStatus(MmsAssetStatusEnum.IN_STOCK.getStatus());
        List<MmsAsset> nowAssetList = assetService.selectMmsAssetList(asset);

        //开始盘点
        for (MmsAssetInventory inventory : collect) {
            if (inventory.getInventoryType().intValue() == InventoryType.part.getCode()) {
                //部分盘点
                List<String> list = Arrays.asList(inventory.getAssetIds().split(","));
                List<Long> queryList = new ArrayList<>();
                list.forEach(e -> {
                    queryList.add(Long.valueOf(e));
                });
                List<MmsAsset> assetList = assetService.list(new QueryWrapper<MmsAsset>().in("id", queryList));
                if (assetList.size() == 0) {
                    log.info("查询资产为空. InventoryId:[{}], assetIds:[{}]", inventory.getId(), StringUtils.join(inventory.getAssetIds(), ","));
                }
                int inventoryStatus = InventoryStatus.done.getCode();
                //增加资产信息
                if (assetList.size() > 0) {
                    this.newInventoryrelation(inventory.getId(), InventorySituation.done.getCode(), assetList);
                }
                //修改盘点状态
                inventory.setInventoryStatus(inventoryStatus);
            } else if (inventory.getInventoryType().intValue() == InventoryType.all.getCode()) {
                //如果是全盘
                if (inventory.getInventoryStatus().intValue() == InventoryStatus.undo.getCode() && isSameDay(inventory.getStartDate(), now)) {
                    //如果是开始盘点，增加资产信息
                    if (nowAssetList.size() > 0) {
                        this.newInventoryrelation(inventory.getId(), InventorySituation.undo.getCode(), nowAssetList);
                    }

                    //修改盘点状态
                    inventory.setInventoryStatus(InventoryStatus.doing.getCode());
                } else if (inventory.getInventoryStatus().intValue() == InventoryStatus.doing.getCode() && isSameDay(inventory.getEndDate(), now)) {
                    //如果是结束盘点, 查询之前的盘点
                    List<MmsAssetInventoryrelation> assetInventoryrelations = inventoryrelationService.list(new QueryWrapper<MmsAssetInventoryrelation>().eq("inventory_id", inventory.getId()));
                    //开始盘点的时候资产都为空
                    if (assetInventoryrelations.size() == 0) {
                        if (nowAssetList.size() > 0) {
                            //开始盘点时资产为空，则所有都是盘盈
                            this.newInventoryrelation(inventory.getId(), InventorySituation.gain.getCode(), nowAssetList);
                        }
                    }
                    //如果开始盘点有数据，则进行比较
                    else {
                        if (nowAssetList.size() == 0) {
                            //开始盘点有数据，结束没数据，则所有都是盘亏
                            this.newInventoryrelation(inventory.getId(), InventorySituation.loss.getCode(), nowAssetList);
                        } else {
                            //开始和结束盘点都有数据
                            List<Long> startAssetids = assetInventoryrelations.stream().map(MmsAssetInventoryrelation::getAssetId).collect(Collectors.toList());
                            List<Long> endAssetids = nowAssetList.stream().map(MmsAsset::getId).collect(Collectors.toList());
                            List<Long> unionAssetids = Stream.concat(startAssetids.stream(), endAssetids.stream()).distinct().collect(Collectors.toList()); //取并集
                            for (MmsAssetInventoryrelation relation : assetInventoryrelations) {
                                if (endAssetids.contains(relation.getAssetId())) {
                                    //结束盘点时也有，即已盘
                                    relation.setInventorySituation(InventorySituation.done.getCode());
                                } else {
                                    //结束盘点时没有，则盘亏
                                    relation.setInventorySituation(InventorySituation.loss.getCode());
                                }
                                unionAssetids.remove(relation.getAssetId());//移除开始盘点有的资产
                            }
                            //更新状态
                            inventoryrelationService.updateBatchById(assetInventoryrelations);

                            //剩余全部是盘盈
                            if (unionAssetids.size() > 0) {
                                this.newInventoryrelationByAssetIds(inventory.getId(), InventorySituation.gain.getCode(), unionAssetids);
                            }
                        }
                    }

                    //修改盘点状态
                    inventory.setInventoryStatus(InventoryStatus.done.getCode());
                }
            }
        }
        //更新盘点表
        this.updateBatchById(collect);

        log.info("bye %s! 执行盘点完成! 时间:[{}], InventoryIds:[{}]", now, StringUtils.join(collect.stream().map(MmsAssetInventory::getId).collect(Collectors.toList()), ","));
    }
}

