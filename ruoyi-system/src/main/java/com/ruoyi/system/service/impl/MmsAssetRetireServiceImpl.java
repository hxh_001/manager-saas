package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetRetire;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.mapper.MmsAssetRetireMapper;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetRetireService;
import com.ruoyi.system.service.IMmsAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产退库Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetRetireServiceImpl implements IMmsAssetRetireService {
    @Autowired
    private MmsAssetRetireMapper mmsAssetRetireMapper;

    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;

    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产退库
     *
     * @param id 资产退库主键
     * @return 资产退库
     */
    @Override
    public MmsAssetRetire selectMmsAssetRetireById(Long id) {
        MmsAssetRetire model = mmsAssetRetireMapper.selectMmsAssetRetireById(id);
        List<MmsAsset> assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.RETIRE.getType());
        model.setAssetList(assetList);
        return model;
    }

    /**
     * 查询资产退库列表
     *
     * @param mmsAssetRetire 资产退库
     * @return 资产退库
     */
    @Override
    public List<MmsAssetRetire> selectMmsAssetRetireList(MmsAssetRetire mmsAssetRetire) {
        return mmsAssetRetireMapper.selectMmsAssetRetireList(mmsAssetRetire);
    }

    /**
     * 新增资产退库
     *
     * @param mmsAssetRetire 资产退库
     * @return 结果
     */
    @Override
    public int insertMmsAssetRetire(MmsAssetRetire mmsAssetRetire) {
        Optional.ofNullable(mmsAssetRetire).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetRetire.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetRetire.setRetireNo(generator.generateDocumentNumber(MmsAssetRelationEnum.RETIRE.getType()));
        mmsAssetRetire.setCreateTime(DateUtils.getNowDate());
        mmsAssetRetire.setCreateBy(getUsername());
        int count = mmsAssetRetireMapper.insertMmsAssetRetire(mmsAssetRetire);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetRetire.getId(), mmsAssetRetire.getAssetIds(), MmsAssetRelationEnum.RETIRE.getType());
        return count;
    }

    /**
     * 修改资产退库
     *
     * @param mmsAssetRetire 资产退库
     * @return 结果
     */
    @Override
    public int updateMmsAssetRetire(MmsAssetRetire mmsAssetRetire) {
        Optional.ofNullable(mmsAssetRetire).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetRetire.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetRetire.setUpdateTime(DateUtils.getNowDate());
        mmsAssetRetire.setUpdateBy(getUsername());
        return mmsAssetRetireMapper.updateMmsAssetRetire(mmsAssetRetire);
    }

    /**
     * 批量删除资产退库
     *
     * @param ids 需要删除的资产退库主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetRetireByIds(Long[] ids) {
        return mmsAssetRetireMapper.deleteMmsAssetRetireByIds(ids);
    }

    /**
     * 删除资产退库信息
     *
     * @param id 资产退库主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetRetireById(Long id) {
        return mmsAssetRetireMapper.deleteMmsAssetRetireById(id);
    }
}
