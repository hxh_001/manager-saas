package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetDepreciation;
import com.ruoyi.system.mapper.MmsAssetDepreciationMapper;
import com.ruoyi.system.service.IMmsAssetDepreciationService;
import com.ruoyi.system.service.IMmsAssetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产折旧Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@Slf4j
@Service
public class MmsAssetDepreciationServiceImpl extends ServiceImpl<MmsAssetDepreciationMapper, MmsAssetDepreciation> implements IMmsAssetDepreciationService {
    @Resource
    private MmsAssetDepreciationMapper mmsAssetDepreciationMapper;

    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    /**
     * 查询资产折旧
     *
     * @param id 资产折旧主键
     * @return 资产折旧
     */
    @Override
    public MmsAssetDepreciation selectMmsAssetDepreciationById(Long id) {
        return mmsAssetDepreciationMapper.selectMmsAssetDepreciationById(id);
    }

    /**
     * 查询资产折旧列表
     *
     * @param mmsAssetDepreciation 资产折旧
     * @return 资产折旧
     */
    @Override
    public List<MmsAssetDepreciation> selectMmsAssetDepreciationList(MmsAssetDepreciation mmsAssetDepreciation) {
        return mmsAssetDepreciationMapper.selectMmsAssetDepreciationList(mmsAssetDepreciation);
    }

    /**
     * 新增资产折旧
     *
     * @param mmsAssetDepreciation 资产折旧
     * @return 结果
     */
    @Override
    public int insertMmsAssetDepreciation(MmsAssetDepreciation mmsAssetDepreciation) {
        mmsAssetDepreciation.setCreateTime(DateUtils.getNowDate());
        mmsAssetDepreciation.setCreateBy(getUsername());
        return mmsAssetDepreciationMapper.insertMmsAssetDepreciation(mmsAssetDepreciation);
    }

    /**
     * 修改资产折旧
     *
     * @param mmsAssetDepreciation 资产折旧
     * @return 结果
     */
    @Override
    public int updateMmsAssetDepreciation(MmsAssetDepreciation mmsAssetDepreciation) {
        mmsAssetDepreciation.setUpdateTime(DateUtils.getNowDate());
        mmsAssetDepreciation.setUpdateBy(getUsername());
        return mmsAssetDepreciationMapper.updateMmsAssetDepreciation(mmsAssetDepreciation);
    }

    /**
     * 批量删除资产折旧
     *
     * @param ids 需要删除的资产折旧主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetDepreciationByIds(Long[] ids) {
        return mmsAssetDepreciationMapper.deleteMmsAssetDepreciationByIds(ids);
    }

    /**
     * 删除资产折旧信息
     *
     * @param id 资产折旧主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetDepreciationById(Long id) {
        return mmsAssetDepreciationMapper.deleteMmsAssetDepreciationById(id);
    }

    /**
     * 查询资产信息
     *
     * @return
     */
    private List<MmsAsset> getAsset() {
        MmsAsset queryAsset = new MmsAsset();
        //购置日期purchaseDate
        List<MmsAsset> assetList = mmsAssetServiceImpl.selectMmsAssetList(queryAsset);
        if (assetList.isEmpty()) {
            return new ArrayList<>();
        }
        // 获取当前月份的第一天
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfCurrentMonth = calendar.getTime();

        // 获取当前月份的最后一天
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date lastDayOfCurrentMonth = calendar.getTime();

        // 过滤掉购置日期为当前月的数据
        List<MmsAsset> destList = assetList.stream()
                .filter(asset -> null != asset.getPurchaseDate() && !(asset.getPurchaseDate().after(firstDayOfCurrentMonth) && asset.getPurchaseDate().before(lastDayOfCurrentMonth)))
                .collect(Collectors.toList());
        if (destList.isEmpty()) {
            return new ArrayList<>();
        }
        return destList;
    }


    /**
     * 月折旧额，剩余使用期限，累积折旧，净值，到期日，已计提期数。折旧定时执行，每月1号启动（查询资产数据，资产过滤条件为，购置日期大于当前月份。）资产数据的折旧，
     * 【折旧算法】：
     * 月折旧额=（金额-累积折旧）/剩余使用期限
     * 剩余使用期限=使用期限-已计提期数
     * 累积折旧=月折旧额每月相加
     * 净值=金额-累积折旧
     * 到期日=购置日期+使用期限（资产会录入，这里是多少个月）
     */
    //定时任务，“累积折旧”算的时候，是取上月的，定时器每月1号执行。意思就是2月1号执行的时候，累积折旧取1月的
    @Scheduled(cron = "0 0 1 1 * ?")
    public void scheduInsert() {
        //1、查询资产信息
        List<MmsAsset> destList = this.getAsset();
        if (destList.isEmpty()) {
            return;
        }

        //2.查询折旧信息，只取上月的折旧信息
        Calendar calendar1 = Calendar.getInstance();
        calendar1.add(Calendar.MONTH, -1); // 减去一个月
        Date lastMonth = calendar1.getTime();

        //key:assetId, value:MmsAssetDepreciation
        Map<Long, MmsAssetDepreciation> assetDepMap = mmsAssetDepreciationMapper.selectEqMonth(lastMonth)
                .stream().collect(Collectors.toMap(MmsAssetDepreciation::getAssetId, MmsAssetDepreciation -> MmsAssetDepreciation));
        List<MmsAssetDepreciation> depreciations = new ArrayList<>();
        if (assetDepMap.size() == 0) {
            //无折旧信息，即第一次折旧
            destList.forEach(e -> {
                MmsAssetDepreciation depreciation = new MmsAssetDepreciation();
                depreciation.setDepStatus(1);
                depreciation.setAssetId(e.getId());
                depreciation.setCreateTime(new Date());
                //到期日
                depreciation.setDueDate(DateUtils.addMonths(e.getPurchaseDate(), e.getUsefulLife().intValue()));

                //剩余使用期限
                depreciation.setRemainDate(e.getUsefulLife());
                //金额
                BigDecimal price = e.getAssetOriginalValue()
                        .multiply(new BigDecimal((e.getTax())))
                        .setScale(2, BigDecimal.ROUND_HALF_DOWN);
                //月折旧额
                depreciation.setMonthAmount(price.divide(new BigDecimal(depreciation.getRemainDate())).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                //累积折旧
                depreciation.setTotalAmount(depreciation.getMonthAmount());
                //净值
                depreciation.setNetWorth(price.subtract(depreciation.getTotalAmount()));
                //已计提期数
                depreciation.setPeriodsNo(1L);
                depreciations.add(depreciation);
            });
        } else {
            //有折旧信息
            destList.forEach(e -> {
                MmsAssetDepreciation depreciation = new MmsAssetDepreciation();
                depreciation.setDepStatus(1);
                depreciation.setAssetId(e.getId());
                depreciation.setCreateTime(new Date());
                //到期日
                depreciation.setDueDate(DateUtils.addMonths(e.getPurchaseDate(), e.getUsefulLife().intValue()));
                //金额
                BigDecimal price = e.getAssetOriginalValue().multiply(new BigDecimal(e.getTax())).setScale(2, BigDecimal.ROUND_HALF_DOWN);
                if (assetDepMap.containsKey(e.getId())) {
                    MmsAssetDepreciation oldDep = assetDepMap.get(e.getId());

                    //剩余使用期限
                    depreciation.setRemainDate(oldDep.getRemainDate() - 1);
                    //已计提期数
                    depreciation.setPeriodsNo(oldDep.getPeriodsNo() + 1);
                    //剩余金额
                    BigDecimal remainAmount = price.subtract(oldDep.getTotalAmount());

                    if (depreciation.getRemainDate().intValue() == 0 || remainAmount.compareTo(BigDecimal.ZERO) <= 0) {
                        //如果是最后一个月了
                        depreciation.setDepStatus(2);
                        //月折旧额
                        depreciation.setMonthAmount(BigDecimal.ZERO);
                        //累积折旧
                        depreciation.setTotalAmount(price);
                        //净值
                        depreciation.setNetWorth(BigDecimal.ZERO);
                    } else { //如果不是最后一个月
                        //月折旧额
                        depreciation.setMonthAmount((price.subtract(oldDep.getTotalAmount()).divide(new BigDecimal(oldDep.getRemainDate())).setScale(2, BigDecimal.ROUND_HALF_DOWN)));
                        //累积折旧
                        depreciation.setTotalAmount(depreciation.getMonthAmount());
                        //净值
                        depreciation.setNetWorth(price.subtract(depreciation.getTotalAmount()));
                    }
                } else {
                    //剩余使用期限
                    depreciation.setRemainDate(e.getUsefulLife());
                    //月折旧额
                    depreciation.setMonthAmount(price.divide(new BigDecimal(depreciation.getRemainDate())).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    //累积折旧
                    depreciation.setTotalAmount(depreciation.getMonthAmount());
                    //净值
                    depreciation.setNetWorth(price.subtract(depreciation.getTotalAmount()));
                    //到期日
                    depreciation.setDueDate(DateUtils.addMonths(e.getPurchaseDate(), e.getUsefulLife().intValue()));
                    //已计提期数
                    depreciation.setPeriodsNo(1L);
                }
                depreciations.add(depreciation);
            });
        }

        if (depreciations.size() > 0) {
            this.saveOrUpdateBatch(depreciations);
        }
    }

    /**
     * 程序启动时，执行折旧
     * 目的：将历史未折旧的资产，进行折旧，直到本月
     */
    @PostConstruct
    public void initDepreciation() {
        //查询所有资产
        List<MmsAsset> assetList = mmsAssetServiceImpl.selectMmsAssetList(new MmsAsset());
        if (assetList.isEmpty()) {
            return;
        }
        Set<Long> assedIds = assetList.stream().map(MmsAsset::getId).collect(Collectors.toSet());

        //1 按照常规折旧，即取这个月开始折旧的资产，或者之前以及存在折旧记录的资产，继续折旧
        this.scheduInsert();

        //2 查询是否有未折旧的资产，比如采购日期在很久之前的，且还未折旧的资产
        List<MmsAssetDepreciation> allDepres = this.selectMmsAssetDepreciationList(new MmsAssetDepreciation());
        if (allDepres.size() > 0) {
            Set<Long> oldAssedIds = allDepres.stream().map(MmsAssetDepreciation::getAssetId).collect(Collectors.toSet());
            assedIds.removeAll(oldAssedIds);
        }
        if (assedIds.size() == 0) {
            log.info("[MmsAssetDepreciationServiceImpl::initDepreciation]没有未折旧的资产，不执行后续操作");
            return;
        }

        //3 开始折旧，
        // 范围：只折旧上上个月之前采购日期之前的未参与折旧的资产
        // 计算过程：从采购日期的上个月开始折旧，折旧计算到当前这个月
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -2);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date lastDayOfPreviousMonth = calendar.getTime();
        List<MmsAsset> destList = assetList.stream().filter(e -> null != e.getPurchaseDate() && assedIds.contains(e.getId()) && e.getPurchaseDate().before(lastDayOfPreviousMonth)).collect(Collectors.toList());
        if (destList.size() == 0) {
            return;
        }

        //取当前时间
        Date now = new Date();
        List<MmsAssetDepreciation> depreciations = new ArrayList<>();
        destList.forEach(e -> {
            BigDecimal totalAmount = BigDecimal.ZERO;
            //采购时间
            Date baseTime = e.getPurchaseDate();
            for (int i = 1; i <= e.getUsefulLife().intValue(); ) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(baseTime);
                calendar1.add(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                Date newDate = calendar1.getTime();
                if (newDate.after(now)) {
                    //如果超过了当前月，则不进行折旧了
                    continue;
                }
                MmsAssetDepreciation depreciation = new MmsAssetDepreciation();
                depreciation.setDepStatus(1);
                depreciation.setAssetId(e.getId());
                depreciation.setCreateTime(newDate);
                //到期日
                depreciation.setDueDate(DateUtils.addMonths(e.getPurchaseDate(), e.getUsefulLife().intValue()));
                //金额
                BigDecimal price = e.getAssetOriginalValue().multiply(new BigDecimal(e.getTax())).setScale(2, BigDecimal.ROUND_HALF_DOWN);
                //剩余使用期限
                depreciation.setRemainDate(e.getUsefulLife() - i);
                //已计提期数
                depreciation.setPeriodsNo(i - 1L);
                //剩余金额
                BigDecimal remainAmount = price.subtract(totalAmount);

                if (depreciation.getRemainDate().intValue() == 0 || remainAmount.compareTo(BigDecimal.ZERO) <= 0) {
                    //如果是最后一个月了
                    depreciation.setDepStatus(2);
                    //月折旧额
                    depreciation.setMonthAmount(BigDecimal.ZERO);
                    //累积折旧
                    depreciation.setTotalAmount(price);
                    //净值
                    depreciation.setNetWorth(BigDecimal.ZERO);
                } else { //如果不是最后一个月
                    //月折旧额
                    depreciation.setMonthAmount((price.subtract(totalAmount).divide(new BigDecimal(depreciation.getRemainDate())).setScale(2, BigDecimal.ROUND_HALF_DOWN)));
                    //累积折旧
                    depreciation.setTotalAmount(depreciation.getMonthAmount());
                    //净值
                    depreciation.setNetWorth(price.subtract(depreciation.getTotalAmount()));
                }
                depreciations.add(depreciation);
            }
        });
        if (depreciations.size() > 0) {
            this.saveOrUpdateBatch(depreciations);
        }
    }
}
