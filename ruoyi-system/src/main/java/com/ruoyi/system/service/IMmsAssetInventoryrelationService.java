package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetInventoryrelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 资产盘库单-关联资产idService接口
 *
 * @author ruoyi
 * @date 2024-07-20
 */
public interface IMmsAssetInventoryrelationService extends IService<MmsAssetInventoryrelation> {
    /**
     * 查询资产盘库单-关联资产id
     *
     * @param id 资产盘库单-关联资产id主键
     * @return 资产盘库单-关联资产id
     */
    public MmsAssetInventoryrelation selectMmsAssetInventoryrelationById(Long id);

    /**
     * 查询资产盘库单-关联资产id列表
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 资产盘库单-关联资产id集合
     */
    public List<MmsAssetInventoryrelation> selectMmsAssetInventoryrelationList(MmsAssetInventoryrelation mmsAssetInventoryrelation);

    /**
     * 新增资产盘库单-关联资产id
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 结果
     */
    public int insertMmsAssetInventoryrelation(MmsAssetInventoryrelation mmsAssetInventoryrelation);

    /**
     * 修改资产盘库单-关联资产id
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 结果
     */
    public int updateMmsAssetInventoryrelation(MmsAssetInventoryrelation mmsAssetInventoryrelation);

    /**
     * 批量删除资产盘库单-关联资产id
     *
     * @param ids 需要删除的资产盘库单-关联资产id主键集合
     * @return 结果
     */
    public int deleteMmsAssetInventoryrelationByIds(Long[] ids);

    /**
     * 删除资产盘库单-关联资产id信息
     *
     * @param id 资产盘库单-关联资产id主键
     * @return 结果
     */
    public int deleteMmsAssetInventoryrelationById(Long id);
}
