package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetInfoChange;

/**
 * 资产信息变更Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetInfoChangeService 
{
    /**
     * 查询资产信息变更
     * 
     * @param id 资产信息变更主键
     * @return 资产信息变更
     */
    public MmsAssetInfoChange selectMmsAssetInfoChangeById(Long id);

    /**
     * 查询资产信息变更列表
     * 
     * @param mmsAssetInfoChange 资产信息变更
     * @return 资产信息变更集合
     */
    public List<MmsAssetInfoChange> selectMmsAssetInfoChangeList(MmsAssetInfoChange mmsAssetInfoChange);

    /**
     * 新增资产信息变更
     * 
     * @param mmsAssetInfoChange 资产信息变更
     * @return 结果
     */
    public int insertMmsAssetInfoChange(MmsAssetInfoChange mmsAssetInfoChange);

    /**
     * 修改资产信息变更
     * 
     * @param mmsAssetInfoChange 资产信息变更
     * @return 结果
     */
    public int updateMmsAssetInfoChange(MmsAssetInfoChange mmsAssetInfoChange);

    /**
     * 批量删除资产信息变更
     * 
     * @param ids 需要删除的资产信息变更主键集合
     * @return 结果
     */
    public int deleteMmsAssetInfoChangeByIds(Long[] ids);

    /**
     * 删除资产信息变更信息
     * 
     * @param id 资产信息变更主键
     * @return 结果
     */
    public int deleteMmsAssetInfoChangeById(Long id);
}
