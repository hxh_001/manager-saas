package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Optional;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAssetTransferMapper;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetTransfer;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetService;
import com.ruoyi.system.service.IMmsAssetTransferService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产调拨Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetTransferServiceImpl implements IMmsAssetTransferService 
{
    @Autowired
    private MmsAssetTransferMapper mmsAssetTransferMapper;
    
    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;
    
    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产调拨
     * 
     * @param id 资产调拨主键
     * @return 资产调拨
     */
    @Override
    public MmsAssetTransfer selectMmsAssetTransferById(Long id)
    {
    	MmsAssetTransfer model =  mmsAssetTransferMapper.selectMmsAssetTransferById(id);
        List<MmsAsset> assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.TRANSFER.getType());
        model.setAssetList(assetList);
        return model;
    }

    /**
     * 查询资产调拨列表
     * 
     * @param mmsAssetTransfer 资产调拨
     * @return 资产调拨
     */
    @Override
    public List<MmsAssetTransfer> selectMmsAssetTransferList(MmsAssetTransfer mmsAssetTransfer)
    {
        return mmsAssetTransferMapper.selectMmsAssetTransferList(mmsAssetTransfer);
        
    }

    /**
     * 新增资产调拨
     * 
     * @param mmsAssetTransfer 资产调拨
     * @return 结果
     */
    @Override
    public int insertMmsAssetTransfer(MmsAssetTransfer mmsAssetTransfer)
    {
        Optional.ofNullable(mmsAssetTransfer).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetTransfer.getTransferDate()).orElseThrow(() -> new RuntimeException("调拨日期为空"));
        Optional.ofNullable(mmsAssetTransfer.getUserId()).orElseThrow(() -> new RuntimeException("调拨人为空"));
        Optional.ofNullable(mmsAssetTransfer.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetTransfer.setTransferNo(generator.generateDocumentNumber(MmsAssetRelationEnum.TRANSFER.getType()));
        mmsAssetTransfer.setCreateTime(DateUtils.getNowDate());
        mmsAssetTransfer.setCreateBy(getUsername());
        int count = mmsAssetTransferMapper.insertMmsAssetTransfer(mmsAssetTransfer);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetTransfer.getId(), mmsAssetTransfer.getAssetIds(), MmsAssetRelationEnum.TRANSFER.getType());
        return count;
    }

    /**
     * 修改资产调拨
     * 
     * @param mmsAssetTransfer 资产调拨
     * @return 结果
     */
    @Override
    public int updateMmsAssetTransfer(MmsAssetTransfer mmsAssetTransfer)
    {
        Optional.ofNullable(mmsAssetTransfer).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetTransfer.getTransferDate()).orElseThrow(() -> new RuntimeException("调拨日期为空"));
        Optional.ofNullable(mmsAssetTransfer.getUserId()).orElseThrow(() -> new RuntimeException("调拨人为空"));
        Optional.ofNullable(mmsAssetTransfer.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetTransfer.setUpdateTime(DateUtils.getNowDate());
        mmsAssetTransfer.setUpdateBy(getUsername());
        return mmsAssetTransferMapper.updateMmsAssetTransfer(mmsAssetTransfer);
    }

    /**
     * 批量删除资产调拨
     * 
     * @param ids 需要删除的资产调拨主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetTransferByIds(Long[] ids)
    {
        return mmsAssetTransferMapper.deleteMmsAssetTransferByIds(ids);
    }

    /**
     * 删除资产调拨信息
     * 
     * @param id 资产调拨主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetTransferById(Long id)
    {
        return mmsAssetTransferMapper.deleteMmsAssetTransferById(id);
    }
}
