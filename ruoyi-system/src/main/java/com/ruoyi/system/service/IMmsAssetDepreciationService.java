package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.MmsAssetDepreciation;

import java.util.List;

/**
 * 资产折旧Service接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface IMmsAssetDepreciationService  extends IService<MmsAssetDepreciation>
{
    /**
     * 查询资产折旧
     * 
     * @param id 资产折旧主键
     * @return 资产折旧
     */
    public MmsAssetDepreciation selectMmsAssetDepreciationById(Long id);

    /**
     * 查询资产折旧列表
     * 
     * @param mmsAssetDepreciation 资产折旧
     * @return 资产折旧集合
     */
    public List<MmsAssetDepreciation> selectMmsAssetDepreciationList(MmsAssetDepreciation mmsAssetDepreciation);

    /**
     * 新增资产折旧
     * 
     * @param mmsAssetDepreciation 资产折旧
     * @return 结果
     */
    public int insertMmsAssetDepreciation(MmsAssetDepreciation mmsAssetDepreciation);

    /**
     * 修改资产折旧
     * 
     * @param mmsAssetDepreciation 资产折旧
     * @return 结果
     */
    public int updateMmsAssetDepreciation(MmsAssetDepreciation mmsAssetDepreciation);

    /**
     * 批量删除资产折旧
     * 
     * @param ids 需要删除的资产折旧主键集合
     * @return 结果
     */
    public int deleteMmsAssetDepreciationByIds(Long[] ids);

    /**
     * 删除资产折旧信息
     * 
     * @param id 资产折旧主键
     * @return 结果
     */
    public int deleteMmsAssetDepreciationById(Long id);
}
