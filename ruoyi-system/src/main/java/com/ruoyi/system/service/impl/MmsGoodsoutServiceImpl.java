package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsGoodsoutMapper;
import com.ruoyi.system.domain.MmsGoodsout;
import com.ruoyi.system.service.IMmsGoodsoutService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 物品出库单（关联入库单）Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Service
public class MmsGoodsoutServiceImpl implements IMmsGoodsoutService 
{
    @Autowired
    private MmsGoodsoutMapper mmsGoodsoutMapper;

    /**
     * 查询物品出库单（关联入库单）
     * 
     * @param id 物品出库单（关联入库单）主键
     * @return 物品出库单（关联入库单）
     */
    @Override
    public MmsGoodsout selectMmsGoodsoutById(Long id)
    {
        return mmsGoodsoutMapper.selectMmsGoodsoutById(id);
    }

    /**
     * 查询物品出库单（关联入库单）列表
     * 
     * @param mmsGoodsout 物品出库单（关联入库单）
     * @return 物品出库单（关联入库单）
     */
    @Override
    public List<MmsGoodsout> selectMmsGoodsoutList(MmsGoodsout mmsGoodsout)
    {
        return mmsGoodsoutMapper.selectMmsGoodsoutList(mmsGoodsout);
    }

    /**
     * 新增物品出库单（关联入库单）
     * 
     * @param mmsGoodsout 物品出库单（关联入库单）
     * @return 结果
     */
    @Override
    public int insertMmsGoodsout(MmsGoodsout mmsGoodsout)
    {
        mmsGoodsout.setCreateTime(DateUtils.getNowDate());
        mmsGoodsout.setCreateBy(getUsername());
        return mmsGoodsoutMapper.insertMmsGoodsout(mmsGoodsout);
    }

    /**
     * 修改物品出库单（关联入库单）
     * 
     * @param mmsGoodsout 物品出库单（关联入库单）
     * @return 结果
     */
    @Override
    public int updateMmsGoodsout(MmsGoodsout mmsGoodsout)
    {
        mmsGoodsout.setUpdateTime(DateUtils.getNowDate());
        mmsGoodsout.setUpdateBy(getUsername());
        return mmsGoodsoutMapper.updateMmsGoodsout(mmsGoodsout);
    }

    /**
     * 批量删除物品出库单（关联入库单）
     * 
     * @param ids 需要删除的物品出库单（关联入库单）主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsoutByIds(Long[] ids)
    {
        return mmsGoodsoutMapper.deleteMmsGoodsoutByIds(ids);
    }

    /**
     * 删除物品出库单（关联入库单）信息
     * 
     * @param id 物品出库单（关联入库单）主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsoutById(Long id)
    {
        return mmsGoodsoutMapper.deleteMmsGoodsoutById(id);
    }
}
