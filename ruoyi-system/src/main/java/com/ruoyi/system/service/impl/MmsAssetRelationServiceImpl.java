package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruoyi.system.mapper.MmsAssetRelationMapper;
import com.ruoyi.system.domain.MmsAssetRelation;
import com.ruoyi.system.service.IMmsAssetRelationService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产和功能项关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetRelationServiceImpl implements IMmsAssetRelationService 
{
    @Autowired
    private MmsAssetRelationMapper mmsAssetRelationMapper;
    
    
    @Override
    @Transactional
    public void addRelation(Long id, String assetIds,String assetFunction) {
    	
    	if(StringUtils.isNotBlank(assetIds)) {
    		List<MmsAssetRelation> list = new ArrayList<MmsAssetRelation>();
    		String[]ids = assetIds.split(",");
    		for (int i = 0; i < ids.length; i++) {
    			MmsAssetRelation model = new MmsAssetRelation();
    			model.setFunctionId(id);
    			model.setAssetId(Long.parseLong(ids[i]));
    			model.setAssetFunction(assetFunction);
    			list.add(model);
    			
			}
    		mmsAssetRelationMapper.batchInsert(list);
    	}
    	
    }

    /**
     * 查询资产和功能项关联
     * 
     * @param id 资产和功能项关联主键
     * @return 资产和功能项关联
     */
    @Override
    public MmsAssetRelation selectMmsAssetRelationById(Long id)
    {
        return mmsAssetRelationMapper.selectMmsAssetRelationById(id);
    }

    /**
     * 查询资产和功能项关联列表
     * 
     * @param mmsAssetRelation 资产和功能项关联
     * @return 资产和功能项关联
     */
    @Override
    public List<MmsAssetRelation> selectMmsAssetRelationList(MmsAssetRelation mmsAssetRelation)
    {
        return mmsAssetRelationMapper.selectMmsAssetRelationList(mmsAssetRelation);
    }

    /**
     * 新增资产和功能项关联
     * 
     * @param mmsAssetRelation 资产和功能项关联
     * @return 结果
     */
    @Override
    public int insertMmsAssetRelation(MmsAssetRelation mmsAssetRelation)
    {
        mmsAssetRelation.setCreateTime(DateUtils.getNowDate());
        mmsAssetRelation.setCreateBy(getUsername());
        return mmsAssetRelationMapper.insertMmsAssetRelation(mmsAssetRelation);
    }

    /**
     * 修改资产和功能项关联
     * 
     * @param mmsAssetRelation 资产和功能项关联
     * @return 结果
     */
    @Override
    public int updateMmsAssetRelation(MmsAssetRelation mmsAssetRelation)
    {
        mmsAssetRelation.setUpdateTime(DateUtils.getNowDate());
        mmsAssetRelation.setUpdateBy(getUsername());
        return mmsAssetRelationMapper.updateMmsAssetRelation(mmsAssetRelation);
    }

    /**
     * 批量删除资产和功能项关联
     * 
     * @param ids 需要删除的资产和功能项关联主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetRelationByIds(Long[] ids)
    {
        return mmsAssetRelationMapper.deleteMmsAssetRelationByIds(ids);
    }

    /**
     * 删除资产和功能项关联信息
     * 
     * @param id 资产和功能项关联主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetRelationById(Long id)
    {
        return mmsAssetRelationMapper.deleteMmsAssetRelationById(id);
    }
}
