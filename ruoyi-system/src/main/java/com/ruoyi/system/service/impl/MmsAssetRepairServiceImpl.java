package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Optional;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAssetRepairMapper;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetRepair;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetRepairService;
import com.ruoyi.system.service.IMmsAssetService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产维修Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetRepairServiceImpl implements IMmsAssetRepairService 
{
    @Autowired
    private MmsAssetRepairMapper mmsAssetRepairMapper;
    
    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;
    
    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产维修
     * 
     * @param id 资产维修主键
     * @return 资产维修
     */
    @Override
    public MmsAssetRepair selectMmsAssetRepairById(Long id)
    {
    	MmsAssetRepair model =  mmsAssetRepairMapper.selectMmsAssetRepairById(id);
        List<MmsAsset>  assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.REPAIR.getType());
        model.setAssetList(assetList);
    	return model;
    }

    /**
     * 查询资产维修列表
     * 
     * @param mmsAssetRepair 资产维修
     * @return 资产维修
     */
    @Override
    public List<MmsAssetRepair> selectMmsAssetRepairList(MmsAssetRepair mmsAssetRepair)
    {
        return mmsAssetRepairMapper.selectMmsAssetRepairList(mmsAssetRepair);
    }

    /**
     * 新增资产维修
     * 
     * @param mmsAssetRepair 资产维修
     * @return 结果
     */
    @Override
    public int insertMmsAssetRepair(MmsAssetRepair mmsAssetRepair)
    {
        Optional.ofNullable(mmsAssetRepair).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetRepair.getRepairDate()).orElseThrow(() -> new RuntimeException("保修日期为空"));
        Optional.ofNullable(mmsAssetRepair.getUserId()).orElseThrow(() -> new RuntimeException("报修人为空"));
        Optional.ofNullable(mmsAssetRepair.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetRepair.setRepairNo(generator.generateDocumentNumber(MmsAssetRelationEnum.REPAIR.getType()));
        mmsAssetRepair.setCreateTime(DateUtils.getNowDate());
        mmsAssetRepair.setCreateBy(getUsername());
        int count = mmsAssetRepairMapper.insertMmsAssetRepair(mmsAssetRepair);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetRepair.getId(), mmsAssetRepair.getAssetIds(), MmsAssetRelationEnum.REPAIR.getType());
        return count;
    }

    /**
     * 修改资产维修
     * 
     * @param mmsAssetRepair 资产维修
     * @return 结果
     */
    @Override
    public int updateMmsAssetRepair(MmsAssetRepair mmsAssetRepair)
    {
        Optional.ofNullable(mmsAssetRepair).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetRepair.getRepairDate()).orElseThrow(() -> new RuntimeException("保修日期为空"));
        Optional.ofNullable(mmsAssetRepair.getUserId()).orElseThrow(() -> new RuntimeException("报修人为空"));
        Optional.ofNullable(mmsAssetRepair.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetRepair.setUpdateTime(DateUtils.getNowDate());
        mmsAssetRepair.setUpdateBy(getUsername());
        return mmsAssetRepairMapper.updateMmsAssetRepair(mmsAssetRepair);
    }

    /**
     * 批量删除资产维修
     * 
     * @param ids 需要删除的资产维修主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetRepairByIds(Long[] ids)
    {
        return mmsAssetRepairMapper.deleteMmsAssetRepairByIds(ids);
    }

    /**
     * 删除资产维修信息
     * 
     * @param id 资产维修主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetRepairById(Long id)
    {
        return mmsAssetRepairMapper.deleteMmsAssetRepairById(id);
    }
}
