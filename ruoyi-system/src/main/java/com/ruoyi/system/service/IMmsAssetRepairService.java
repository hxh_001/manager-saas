package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetRepair;

/**
 * 资产维修Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetRepairService 
{
    /**
     * 查询资产维修
     * 
     * @param id 资产维修主键
     * @return 资产维修
     */
    public MmsAssetRepair selectMmsAssetRepairById(Long id);

    /**
     * 查询资产维修列表
     * 
     * @param mmsAssetRepair 资产维修
     * @return 资产维修集合
     */
    public List<MmsAssetRepair> selectMmsAssetRepairList(MmsAssetRepair mmsAssetRepair);

    /**
     * 新增资产维修
     * 
     * @param mmsAssetRepair 资产维修
     * @return 结果
     */
    public int insertMmsAssetRepair(MmsAssetRepair mmsAssetRepair);

    /**
     * 修改资产维修
     * 
     * @param mmsAssetRepair 资产维修
     * @return 结果
     */
    public int updateMmsAssetRepair(MmsAssetRepair mmsAssetRepair);

    /**
     * 批量删除资产维修
     * 
     * @param ids 需要删除的资产维修主键集合
     * @return 结果
     */
    public int deleteMmsAssetRepairByIds(Long[] ids);

    /**
     * 删除资产维修信息
     * 
     * @param id 资产维修主键
     * @return 结果
     */
    public int deleteMmsAssetRepairById(Long id);
}
