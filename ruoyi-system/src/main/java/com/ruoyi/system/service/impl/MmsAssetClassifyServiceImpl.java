package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetClassify;
import com.ruoyi.system.domain.vo.MmsStatClassify;
import com.ruoyi.system.mapper.MmsAssetClassifyMapper;
import com.ruoyi.system.mapper.MmsAssetMapper;
import com.ruoyi.system.service.IMmsAssetClassifyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产分类Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@Service
public class MmsAssetClassifyServiceImpl implements IMmsAssetClassifyService {
    @Resource
    private MmsAssetClassifyMapper mmsAssetClassifyMapper;

    @Resource
    private MmsAssetMapper assetMapper;

    /**
     * 查询资产分类
     *
     * @param id 资产分类主键
     * @return 资产分类
     */
    @Override
    public MmsAssetClassify selectMmsAssetClassifyById(Long id) {
        return mmsAssetClassifyMapper.selectMmsAssetClassifyById(id);
    }

    /**
     * 查询资产分类列表
     *
     * @param mmsAssetClassify 资产分类
     * @return 资产分类
     */
    @Override
    public List<MmsAssetClassify> selectMmsAssetClassifyList(MmsAssetClassify mmsAssetClassify) {
        return mmsAssetClassifyMapper.selectMmsAssetClassifyList(mmsAssetClassify);
    }

    /**
     * 新增资产分类
     *
     * @param mmsAssetClassify 资产分类
     * @return 结果
     */
    @Override
    public int insertMmsAssetClassify(MmsAssetClassify mmsAssetClassify) {
        Optional.ofNullable(mmsAssetClassify).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetClassify.getClassifyName()).orElseThrow(() -> new RuntimeException("资产分类名称为空"));
        mmsAssetClassify.setCreateTime(DateUtils.getNowDate());
        mmsAssetClassify.setCreateBy(getUsername());
        return mmsAssetClassifyMapper.insertMmsAssetClassify(mmsAssetClassify);
    }

    /**
     * 修改资产分类
     *
     * @param mmsAssetClassify 资产分类
     * @return 结果
     */
    @Override
    public int updateMmsAssetClassify(MmsAssetClassify mmsAssetClassify) {
        mmsAssetClassify.setUpdateTime(DateUtils.getNowDate());
        mmsAssetClassify.setUpdateBy(getUsername());
        return mmsAssetClassifyMapper.updateMmsAssetClassify(mmsAssetClassify);
    }

    /**
     * 批量删除资产分类
     *
     * @param ids 需要删除的资产分类主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetClassifyByIds(Long[] ids) {
        return mmsAssetClassifyMapper.deleteMmsAssetClassifyByIds(ids);
    }

    /**
     * 删除资产分类信息
     *
     * @param id 资产分类主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetClassifyById(Long id) {
        return mmsAssetClassifyMapper.deleteMmsAssetClassifyById(id);
    }

    /**
     * 资产分类统计
     *
     * @return
     */
    @Override
    public List<MmsStatClassify> statClassify() {
        List<MmsStatClassify> ret = new ArrayList<>();
        //查询所有分类
        List<MmsAssetClassify> classifies = mmsAssetClassifyMapper.selectMmsAssetClassifyList(new MmsAssetClassify());
        if (classifies.size() == 0) {
            return ret;
        }

        //统计资产清单
        List<MmsAsset> assetList = assetMapper.selectMmsAssetList(new MmsAsset());
        if (assetList.size() == 0) {
            return ret;
        }
        assetList = assetList.stream().filter(e->e.getClassifyId() != null).collect(Collectors.toList());
        if (assetList.size() == 0) {
            return ret;
        }

        //分类统计
        Map<Long, List<MmsAsset>> classifyMap = assetList.stream().collect(Collectors.groupingBy(MmsAsset::getClassifyId));
        Map<Long, String> classifyIdMap = classifies.stream().collect(Collectors.toMap(MmsAssetClassify::getId, MmsAssetClassify::getClassifyName));
        classifyIdMap.forEach((key, value) -> {
            if (classifyMap.containsKey(key)) {
                ret.add(new MmsStatClassify(value, classifyMap.get(key).size()));
            }
        });
        return ret;
    }
}
