package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAssetAssetinrelationMapper;
import com.ruoyi.system.domain.MmsAssetAssetinrelation;
import com.ruoyi.system.service.IMmsAssetAssetinrelationService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产入库单-关联资产idService业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Service
public class MmsAssetAssetinrelationServiceImpl implements IMmsAssetAssetinrelationService 
{
    @Autowired
    private MmsAssetAssetinrelationMapper mmsAssetAssetinrelationMapper;

    /**
     * 查询资产入库单-关联资产id
     * 
     * @param id 资产入库单-关联资产id主键
     * @return 资产入库单-关联资产id
     */
    @Override
    public MmsAssetAssetinrelation selectMmsAssetAssetinrelationById(Long id)
    {
        return mmsAssetAssetinrelationMapper.selectMmsAssetAssetinrelationById(id);
    }

    /**
     * 查询资产入库单-关联资产id列表
     * 
     * @param mmsAssetAssetinrelation 资产入库单-关联资产id
     * @return 资产入库单-关联资产id
     */
    @Override
    public List<MmsAssetAssetinrelation> selectMmsAssetAssetinrelationList(MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        return mmsAssetAssetinrelationMapper.selectMmsAssetAssetinrelationList(mmsAssetAssetinrelation);
    }

    /**
     * 新增资产入库单-关联资产id
     * 
     * @param mmsAssetAssetinrelation 资产入库单-关联资产id
     * @return 结果
     */
    @Override
    public int insertMmsAssetAssetinrelation(MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        mmsAssetAssetinrelation.setCreateTime(DateUtils.getNowDate());
        mmsAssetAssetinrelation.setCreateBy(getUsername());
        return mmsAssetAssetinrelationMapper.insertMmsAssetAssetinrelation(mmsAssetAssetinrelation);
    }

    /**
     * 修改资产入库单-关联资产id
     * 
     * @param mmsAssetAssetinrelation 资产入库单-关联资产id
     * @return 结果
     */
    @Override
    public int updateMmsAssetAssetinrelation(MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        mmsAssetAssetinrelation.setUpdateTime(DateUtils.getNowDate());
        mmsAssetAssetinrelation.setUpdateBy(getUsername());
        return mmsAssetAssetinrelationMapper.updateMmsAssetAssetinrelation(mmsAssetAssetinrelation);
    }

    /**
     * 批量删除资产入库单-关联资产id
     * 
     * @param ids 需要删除的资产入库单-关联资产id主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetAssetinrelationByIds(Long[] ids)
    {
        return mmsAssetAssetinrelationMapper.deleteMmsAssetAssetinrelationByIds(ids);
    }

    /**
     * 删除资产入库单-关联资产id信息
     * 
     * @param id 资产入库单-关联资产id主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetAssetinrelationById(Long id)
    {
        return mmsAssetAssetinrelationMapper.deleteMmsAssetAssetinrelationById(id);
    }
}
