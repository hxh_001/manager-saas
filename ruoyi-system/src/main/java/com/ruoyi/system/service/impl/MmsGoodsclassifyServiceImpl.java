package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsGoodsclassifyMapper;
import com.ruoyi.system.domain.MmsGoodsclassify;
import com.ruoyi.system.service.IMmsGoodsclassifyService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 物品分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@Service
public class MmsGoodsclassifyServiceImpl implements IMmsGoodsclassifyService 
{
    @Autowired
    private MmsGoodsclassifyMapper mmsGoodsclassifyMapper;

    /**
     * 查询物品分类
     * 
     * @param id 物品分类主键
     * @return 物品分类
     */
    @Override
    public MmsGoodsclassify selectMmsGoodsclassifyById(Long id)
    {
        return mmsGoodsclassifyMapper.selectMmsGoodsclassifyById(id);
    }

    /**
     * 查询物品分类列表
     * 
     * @param mmsGoodsclassify 物品分类
     * @return 物品分类
     */
    @Override
    public List<MmsGoodsclassify> selectMmsGoodsclassifyList(MmsGoodsclassify mmsGoodsclassify)
    {
        return mmsGoodsclassifyMapper.selectMmsGoodsclassifyList(mmsGoodsclassify);
    }

    /**
     * 新增物品分类
     * 
     * @param mmsGoodsclassify 物品分类
     * @return 结果
     */
    @Override
    public int insertMmsGoodsclassify(MmsGoodsclassify mmsGoodsclassify)
    {
        mmsGoodsclassify.setCreateTime(DateUtils.getNowDate());
        mmsGoodsclassify.setCreateBy(getUsername());
        return mmsGoodsclassifyMapper.insertMmsGoodsclassify(mmsGoodsclassify);
    }

    /**
     * 修改物品分类
     * 
     * @param mmsGoodsclassify 物品分类
     * @return 结果
     */
    @Override
    public int updateMmsGoodsclassify(MmsGoodsclassify mmsGoodsclassify)
    {
        mmsGoodsclassify.setUpdateTime(DateUtils.getNowDate());
        mmsGoodsclassify.setUpdateBy(getUsername());
        return mmsGoodsclassifyMapper.updateMmsGoodsclassify(mmsGoodsclassify);
    }

    /**
     * 批量删除物品分类
     * 
     * @param ids 需要删除的物品分类主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsclassifyByIds(Long[] ids)
    {
        return mmsGoodsclassifyMapper.deleteMmsGoodsclassifyByIds(ids);
    }

    /**
     * 删除物品分类信息
     * 
     * @param id 物品分类主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsclassifyById(Long id)
    {
        return mmsGoodsclassifyMapper.deleteMmsGoodsclassifyById(id);
    }
}
