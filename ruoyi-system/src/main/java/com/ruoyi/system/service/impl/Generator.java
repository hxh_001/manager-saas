package com.ruoyi.system.service.impl;

import com.ruoyi.system.generator.GeneratorEnum;
import com.ruoyi.system.mapper.NumberMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class Generator {
    private static final ConcurrentHashMap<String, Integer> sequenceCache = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, Lock> lockMap = new ConcurrentHashMap<>();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");


    /**
     * 定时清理本地缓存，防止缓存过大，占用内存
     */
    @Scheduled(cron = "0 0 12 * * ?")
    public void clearCache() {
        sequenceCache.clear();
        lockMap.clear();
        System.out.println("sequenceCache cleared at 12:00 PM.");
    }

    /**
     * 生成单据编号
     *
     * @param type 单据类型
     * @return 单据编号
     */
    public synchronized String generateDocumentNumber(String type) {
        String datePrefix = dateFormat.format(new Date());
        int sequence = getSequence(type, datePrefix);

        // 生成单据编号
        return type + datePrefix + String.format("%04d", sequence);
    }

    /**
     * 获取流水号
     *
     * @param type       单据类型
     * @param datePrefix 日期前缀
     * @return 流水号
     */
    private int getSequence(String type, String datePrefix) {
        String key = type + datePrefix;
        // 获取对应key的锁对象，如果不存在则创建一个新的锁对象并放入map中
        lockMap.computeIfAbsent(key, k -> new ReentrantLock()).lock();
        try {
            if (sequenceCache.containsKey(key)) {
                int sequence = sequenceCache.get(key);
                sequenceCache.put(key, ++sequence);
                return sequence;
            } else {
                int maxSequenceFromDb = getMaxSequenceFromDatabase(type, datePrefix);
                sequenceCache.put(key, ++maxSequenceFromDb);
                return maxSequenceFromDb;
            }
        } finally {
            // 释放锁
            lockMap.get(key).unlock();
        }
    }

    /**
     * 从数据库获取最大流水号
     *
     * @param type 单据类型、根据不同的类型查询对应的不同表单。后续需要修改或者添加直接修改AuditGeneratorEnum即可。
     * @param datePrefix 日期前缀
     * @return 最大流水号
     */
    @Resource
    private NumberMapper mapper;

    private int getMaxSequenceFromDatabase(String type, String datePrefix) {
        GeneratorEnum enumValue = GeneratorEnum.fromType(type);
        String tableName = enumValue.getTableName();
        String numberCol = enumValue.getNumberCol();
        // 查询数据库获取最大的流水号
        String numberStr = mapper.queryNumber(tableName, numberCol, type + datePrefix);
        return com.ruoyi.common.utils.StringUtils.isEmpty(numberStr) ? 1 : Integer.parseInt(numberStr.substring(numberStr.length() - 4));
    }
}
