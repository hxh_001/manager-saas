package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsGoodsinMapper;
import com.ruoyi.system.domain.MmsGoodsin;
import com.ruoyi.system.service.IMmsGoodsinService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 物品入库单Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Service
public class MmsGoodsinServiceImpl implements IMmsGoodsinService 
{
    @Autowired
    private MmsGoodsinMapper mmsGoodsinMapper;

    /**
     * 查询物品入库单
     * 
     * @param id 物品入库单主键
     * @return 物品入库单
     */
    @Override
    public MmsGoodsin selectMmsGoodsinById(Long id)
    {
        return mmsGoodsinMapper.selectMmsGoodsinById(id);
    }

    /**
     * 查询物品入库单列表
     * 
     * @param mmsGoodsin 物品入库单
     * @return 物品入库单
     */
    @Override
    public List<MmsGoodsin> selectMmsGoodsinList(MmsGoodsin mmsGoodsin)
    {
        return mmsGoodsinMapper.selectMmsGoodsinList(mmsGoodsin);
    }

    /**
     * 新增物品入库单
     * 
     * @param mmsGoodsin 物品入库单
     * @return 结果
     */
    @Override
    public int insertMmsGoodsin(MmsGoodsin mmsGoodsin)
    {
        mmsGoodsin.setCreateTime(DateUtils.getNowDate());
        mmsGoodsin.setCreateBy(getUsername());
        return mmsGoodsinMapper.insertMmsGoodsin(mmsGoodsin);
    }

    /**
     * 修改物品入库单
     * 
     * @param mmsGoodsin 物品入库单
     * @return 结果
     */
    @Override
    public int updateMmsGoodsin(MmsGoodsin mmsGoodsin)
    {
        mmsGoodsin.setUpdateTime(DateUtils.getNowDate());
        mmsGoodsin.setUpdateBy(getUsername());
        return mmsGoodsinMapper.updateMmsGoodsin(mmsGoodsin);
    }

    /**
     * 批量删除物品入库单
     * 
     * @param ids 需要删除的物品入库单主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsinByIds(Long[] ids)
    {
        return mmsGoodsinMapper.deleteMmsGoodsinByIds(ids);
    }

    /**
     * 删除物品入库单信息
     * 
     * @param id 物品入库单主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsinById(Long id)
    {
        return mmsGoodsinMapper.deleteMmsGoodsinById(id);
    }
}
