package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsGoodslistMapper;
import com.ruoyi.system.domain.MmsGoodslist;
import com.ruoyi.system.service.IMmsGoodslistService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 物品名目Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@Service
public class MmsGoodslistServiceImpl implements IMmsGoodslistService 
{
    @Autowired
    private MmsGoodslistMapper mmsGoodslistMapper;

    /**
     * 查询物品名目
     * 
     * @param id 物品名目主键
     * @return 物品名目
     */
    @Override
    public MmsGoodslist selectMmsGoodslistById(Long id)
    {
        return mmsGoodslistMapper.selectMmsGoodslistById(id);
    }

    /**
     * 查询物品名目列表
     * 
     * @param mmsGoodslist 物品名目
     * @return 物品名目
     */
    @Override
    public List<MmsGoodslist> selectMmsGoodslistList(MmsGoodslist mmsGoodslist)
    {
        return mmsGoodslistMapper.selectMmsGoodslistList(mmsGoodslist);
    }

    /**
     * 新增物品名目
     * 
     * @param mmsGoodslist 物品名目
     * @return 结果
     */
    @Override
    public int insertMmsGoodslist(MmsGoodslist mmsGoodslist)
    {
        mmsGoodslist.setCreateTime(DateUtils.getNowDate());
        mmsGoodslist.setCreateBy(getUsername());
        return mmsGoodslistMapper.insertMmsGoodslist(mmsGoodslist);
    }

    /**
     * 修改物品名目
     * 
     * @param mmsGoodslist 物品名目
     * @return 结果
     */
    @Override
    public int updateMmsGoodslist(MmsGoodslist mmsGoodslist)
    {
        mmsGoodslist.setUpdateTime(DateUtils.getNowDate());
        mmsGoodslist.setUpdateBy(getUsername());
        return mmsGoodslistMapper.updateMmsGoodslist(mmsGoodslist);
    }

    /**
     * 批量删除物品名目
     * 
     * @param ids 需要删除的物品名目主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodslistByIds(Long[] ids)
    {
        return mmsGoodslistMapper.deleteMmsGoodslistByIds(ids);
    }

    /**
     * 删除物品名目信息
     * 
     * @param id 物品名目主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodslistById(Long id)
    {
        return mmsGoodslistMapper.deleteMmsGoodslistById(id);
    }
}
