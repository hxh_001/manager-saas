package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsGoodsinrelationMapper;
import com.ruoyi.system.domain.MmsGoodsinrelation;
import com.ruoyi.system.service.IMmsGoodsinrelationService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 物品入库单-关联物品idService业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Service
public class MmsGoodsinrelationServiceImpl implements IMmsGoodsinrelationService 
{
    @Autowired
    private MmsGoodsinrelationMapper mmsGoodsinrelationMapper;

    /**
     * 查询物品入库单-关联物品id
     * 
     * @param id 物品入库单-关联物品id主键
     * @return 物品入库单-关联物品id
     */
    @Override
    public MmsGoodsinrelation selectMmsGoodsinrelationById(Long id)
    {
        return mmsGoodsinrelationMapper.selectMmsGoodsinrelationById(id);
    }

    /**
     * 查询物品入库单-关联物品id列表
     * 
     * @param mmsGoodsinrelation 物品入库单-关联物品id
     * @return 物品入库单-关联物品id
     */
    @Override
    public List<MmsGoodsinrelation> selectMmsGoodsinrelationList(MmsGoodsinrelation mmsGoodsinrelation)
    {
        return mmsGoodsinrelationMapper.selectMmsGoodsinrelationList(mmsGoodsinrelation);
    }

    /**
     * 新增物品入库单-关联物品id
     * 
     * @param mmsGoodsinrelation 物品入库单-关联物品id
     * @return 结果
     */
    @Override
    public int insertMmsGoodsinrelation(MmsGoodsinrelation mmsGoodsinrelation)
    {
        mmsGoodsinrelation.setCreateTime(DateUtils.getNowDate());
        mmsGoodsinrelation.setCreateBy(getUsername());
        return mmsGoodsinrelationMapper.insertMmsGoodsinrelation(mmsGoodsinrelation);
    }

    /**
     * 修改物品入库单-关联物品id
     * 
     * @param mmsGoodsinrelation 物品入库单-关联物品id
     * @return 结果
     */
    @Override
    public int updateMmsGoodsinrelation(MmsGoodsinrelation mmsGoodsinrelation)
    {
        mmsGoodsinrelation.setUpdateTime(DateUtils.getNowDate());
        mmsGoodsinrelation.setUpdateBy(getUsername());
        return mmsGoodsinrelationMapper.updateMmsGoodsinrelation(mmsGoodsinrelation);
    }

    /**
     * 批量删除物品入库单-关联物品id
     * 
     * @param ids 需要删除的物品入库单-关联物品id主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsinrelationByIds(Long[] ids)
    {
        return mmsGoodsinrelationMapper.deleteMmsGoodsinrelationByIds(ids);
    }

    /**
     * 删除物品入库单-关联物品id信息
     * 
     * @param id 物品入库单-关联物品id主键
     * @return 结果
     */
    @Override
    public int deleteMmsGoodsinrelationById(Long id)
    {
        return mmsGoodsinrelationMapper.deleteMmsGoodsinrelationById(id);
    }
}
