package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetReceive;

/**
 * 资产领用Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetReceiveService 
{
    /**
     * 查询资产领用
     * 
     * @param id 资产领用主键
     * @return 资产领用
     */
    public MmsAssetReceive selectMmsAssetReceiveById(Long id);

    /**
     * 查询资产领用列表
     * 
     * @param mmsAssetReceive 资产领用
     * @return 资产领用集合
     */
    public List<MmsAssetReceive> selectMmsAssetReceiveList(MmsAssetReceive mmsAssetReceive);

    /**
     * 新增资产领用
     * 
     * @param mmsAssetReceive 资产领用
     * @return 结果
     */
    public int insertMmsAssetReceive(MmsAssetReceive mmsAssetReceive);

    /**
     * 修改资产领用
     * 
     * @param mmsAssetReceive 资产领用
     * @return 结果
     */
    public int updateMmsAssetReceive(MmsAssetReceive mmsAssetReceive);

    /**
     * 批量删除资产领用
     * 
     * @param ids 需要删除的资产领用主键集合
     * @return 结果
     */
    public int deleteMmsAssetReceiveByIds(Long[] ids);

    /**
     * 删除资产领用信息
     * 
     * @param id 资产领用主键
     * @return 结果
     */
    public int deleteMmsAssetReceiveById(Long id);
}
