package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsSupplierMapper;
import com.ruoyi.system.domain.MmsSupplier;
import com.ruoyi.system.service.IMmsSupplierService;

/**
 * 供应商管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-08-30
 */
@Service
public class MmsSupplierServiceImpl extends ServiceImpl<MmsSupplierMapper, MmsSupplier> implements IMmsSupplierService {
    @Autowired
    private MmsSupplierMapper mmsSupplierMapper;

    /**
     * 查询供应商管理
     *
     * @param id 供应商管理主键
     * @return 供应商管理
     */
    @Override
    public MmsSupplier selectMmsSupplierById(Long id) {
        return mmsSupplierMapper.selectMmsSupplierById(id);
    }

    /**
     * 查询供应商管理列表
     *
     * @param mmsSupplier 供应商管理
     * @return 供应商管理
     */
    @Override
    public List<MmsSupplier> selectMmsSupplierList(MmsSupplier mmsSupplier) {
        return mmsSupplierMapper.selectMmsSupplierList(mmsSupplier);
    }

    /**
     * 新增供应商管理
     *
     * @param mmsSupplier 供应商管理
     * @return 结果
     */
    @Override
    public int insertMmsSupplier(MmsSupplier mmsSupplier) {
        mmsSupplier.setCreateTime(new Date());
        return mmsSupplierMapper.insertMmsSupplier(mmsSupplier);
    }

    /**
     * 修改供应商管理
     *
     * @param mmsSupplier 供应商管理
     * @return 结果
     */
    @Override
    public int updateMmsSupplier(MmsSupplier mmsSupplier) {
        mmsSupplier.setUpdateTime(new Date());
        return mmsSupplierMapper.updateMmsSupplier(mmsSupplier);
    }

    /**
     * 批量删除供应商管理
     *
     * @param ids 需要删除的供应商管理主键
     * @return 结果
     */
    @Override
    public int deleteMmsSupplierByIds(Long[] ids) {
        return mmsSupplierMapper.deleteMmsSupplierByIds(ids);
    }

    /**
     * 删除供应商管理信息
     *
     * @param id 供应商管理主键
     * @return 结果
     */
    @Override
    public int deleteMmsSupplierById(Long id) {
        return mmsSupplierMapper.deleteMmsSupplierById(id);
    }
}