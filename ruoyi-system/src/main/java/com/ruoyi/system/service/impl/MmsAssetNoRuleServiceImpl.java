package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAssetNoRuleMapper;
import com.ruoyi.system.domain.MmsAssetNoRule;
import com.ruoyi.system.service.IMmsAssetNoRuleService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 编码规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetNoRuleServiceImpl implements IMmsAssetNoRuleService 
{
    @Autowired
    private MmsAssetNoRuleMapper mmsAssetNoRuleMapper;

    /**
     * 查询编码规则
     * 
     * @param id 编码规则主键
     * @return 编码规则
     */
    @Override
    public MmsAssetNoRule selectMmsAssetNoRuleById(Long id)
    {
        return mmsAssetNoRuleMapper.selectMmsAssetNoRuleById(id);
    }

    /**
     * 查询编码规则列表
     * 
     * @param mmsAssetNoRule 编码规则
     * @return 编码规则
     */
    @Override
    public List<MmsAssetNoRule> selectMmsAssetNoRuleList(MmsAssetNoRule mmsAssetNoRule)
    {
        return mmsAssetNoRuleMapper.selectMmsAssetNoRuleList(mmsAssetNoRule);
    }

    /**
     * 新增编码规则
     * 
     * @param mmsAssetNoRule 编码规则
     * @return 结果
     */
    @Override
    public int insertMmsAssetNoRule(MmsAssetNoRule mmsAssetNoRule)
    {
        mmsAssetNoRule.setCreateTime(DateUtils.getNowDate());
        mmsAssetNoRule.setCreateBy(getUsername());
        return mmsAssetNoRuleMapper.insertMmsAssetNoRule(mmsAssetNoRule);
    }

    /**
     * 修改编码规则
     * 
     * @param mmsAssetNoRule 编码规则
     * @return 结果
     */
    @Override
    public int updateMmsAssetNoRule(MmsAssetNoRule mmsAssetNoRule)
    {
        mmsAssetNoRule.setUpdateTime(DateUtils.getNowDate());
        mmsAssetNoRule.setUpdateBy(getUsername());
        return mmsAssetNoRuleMapper.updateMmsAssetNoRule(mmsAssetNoRule);
    }

    /**
     * 批量删除编码规则
     * 
     * @param ids 需要删除的编码规则主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetNoRuleByIds(Long[] ids)
    {
        return mmsAssetNoRuleMapper.deleteMmsAssetNoRuleByIds(ids);
    }

    /**
     * 删除编码规则信息
     * 
     * @param id 编码规则主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetNoRuleById(Long id)
    {
        return mmsAssetNoRuleMapper.deleteMmsAssetNoRuleById(id);
    }
}
