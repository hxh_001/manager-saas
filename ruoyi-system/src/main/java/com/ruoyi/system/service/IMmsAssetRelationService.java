package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetRelation;

/**
 * 资产和功能项关联Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetRelationService 
{
    /**
     * 查询资产和功能项关联
     * 
     * @param id 资产和功能项关联主键
     * @return 资产和功能项关联
     */
    public MmsAssetRelation selectMmsAssetRelationById(Long id);

    /**
     * 查询资产和功能项关联列表
     * 
     * @param mmsAssetRelation 资产和功能项关联
     * @return 资产和功能项关联集合
     */
    public List<MmsAssetRelation> selectMmsAssetRelationList(MmsAssetRelation mmsAssetRelation);

    /**
     * 新增资产和功能项关联
     * 
     * @param mmsAssetRelation 资产和功能项关联
     * @return 结果
     */
    public int insertMmsAssetRelation(MmsAssetRelation mmsAssetRelation);

    /**
     * 修改资产和功能项关联
     * 
     * @param mmsAssetRelation 资产和功能项关联
     * @return 结果
     */
    public int updateMmsAssetRelation(MmsAssetRelation mmsAssetRelation);

    /**
     * 批量删除资产和功能项关联
     * 
     * @param ids 需要删除的资产和功能项关联主键集合
     * @return 结果
     */
    public int deleteMmsAssetRelationByIds(Long[] ids);

    /**
     * 删除资产和功能项关联信息
     * 
     * @param id 资产和功能项关联主键
     * @return 结果
     */
    public int deleteMmsAssetRelationById(Long id);

	void addRelation(Long id, String assetIds, String assetFunction);
}
