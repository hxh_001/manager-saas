package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.MmsAssetInventory;

import java.util.List;

/**
 * 资产盘点Service接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface IMmsAssetInventoryService extends IService<MmsAssetInventory>
{
    /**
     * 查询资产盘点
     * 
     * @param id 资产盘点主键
     * @return 资产盘点
     */
    public MmsAssetInventory selectMmsAssetInventoryById(Long id);

    /**
     * 查询资产盘点列表
     * 
     * @param mmsAssetInventory 资产盘点
     * @return 资产盘点集合
     */
    public List<MmsAssetInventory> selectMmsAssetInventoryList(MmsAssetInventory mmsAssetInventory);

    /**
     * 新增资产盘点
     * 
     * @param mmsAssetInventory 资产盘点
     * @return 结果
     */
    public int insertMmsAssetInventory(MmsAssetInventory mmsAssetInventory);

    /**
     * 修改资产盘点
     * 
     * @param mmsAssetInventory 资产盘点
     * @return 结果
     */
    public int updateMmsAssetInventory(MmsAssetInventory mmsAssetInventory);

    /**
     * 批量删除资产盘点
     * 
     * @param ids 需要删除的资产盘点主键集合
     * @return 结果
     */
    public int deleteMmsAssetInventoryByIds(Long[] ids);

    /**
     * 删除资产盘点信息
     * 
     * @param id 资产盘点主键
     * @return 结果
     */
    public int deleteMmsAssetInventoryById(Long id);
}
