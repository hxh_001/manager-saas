package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Optional;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAssetReceiveMapper;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetReceive;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.service.IMmsAssetReceiveService;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产领用Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetReceiveServiceImpl implements IMmsAssetReceiveService 
{
    @Resource
    private MmsAssetReceiveMapper mmsAssetReceiveMapper;
    
    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;
    
    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产领用
     * 
     * @param id 资产领用主键
     * @return 资产领用
     */
    @Override
    public MmsAssetReceive selectMmsAssetReceiveById(Long id)
    {
    	MmsAssetReceive model =  mmsAssetReceiveMapper.selectMmsAssetReceiveById(id);
    	List<MmsAsset>  assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.RECEIVE.getType());
    	model.setAssetList(assetList);
    	return model;
    }

    /**
     * 查询资产领用列表
     * 
     * @param mmsAssetReceive 资产领用
     * @return 资产领用
     */
    @Override
    public List<MmsAssetReceive> selectMmsAssetReceiveList(MmsAssetReceive mmsAssetReceive)
    {
        return mmsAssetReceiveMapper.selectMmsAssetReceiveList(mmsAssetReceive);
    }

    /**
     * 新增资产领用
     * 
     * @param mmsAssetReceive 资产领用
     * @return 结果
     */
    @Override
    public int insertMmsAssetReceive(MmsAssetReceive mmsAssetReceive)
    {
        Optional.ofNullable(mmsAssetReceive).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetReceive.getReceiveDate()).orElseThrow(() -> new RuntimeException("领用日期为空"));
        Optional.ofNullable(mmsAssetReceive.getUserId()).orElseThrow(() -> new RuntimeException("领用人为空"));
        Optional.ofNullable(mmsAssetReceive.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetReceive.setCreateTime(DateUtils.getNowDate());
        mmsAssetReceive.setCreateBy(getUsername());
        mmsAssetReceive.setTransferNo(generator.generateDocumentNumber(MmsAssetRelationEnum.RECEIVE.getType()));
        
        int count = mmsAssetReceiveMapper.insertMmsAssetReceive(mmsAssetReceive);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetReceive.getId(), mmsAssetReceive.getAssetIds(), MmsAssetRelationEnum.RECEIVE.getType());
        return count;
    }

    /**
     * 修改资产领用
     * 
     * @param mmsAssetReceive 资产领用
     * @return 结果
     */
    @Override
    public int updateMmsAssetReceive(MmsAssetReceive mmsAssetReceive)
    {
        Optional.ofNullable(mmsAssetReceive).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetReceive.getReceiveDate()).orElseThrow(() -> new RuntimeException("领用日期为空"));
        Optional.ofNullable(mmsAssetReceive.getReceiveUse()).orElseThrow(() -> new RuntimeException("领用人为空"));
        Optional.ofNullable(mmsAssetReceive.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetReceive.setUpdateTime(DateUtils.getNowDate());
        mmsAssetReceive.setUpdateBy(getUsername());
        return mmsAssetReceiveMapper.updateMmsAssetReceive(mmsAssetReceive);
    }

    /**
     * 批量删除资产领用
     * 
     * @param ids 需要删除的资产领用主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetReceiveByIds(Long[] ids)
    {
        return mmsAssetReceiveMapper.deleteMmsAssetReceiveByIds(ids);
    }

    /**
     * 删除资产领用信息
     * 
     * @param id 资产领用主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetReceiveById(Long id)
    {
        return mmsAssetReceiveMapper.deleteMmsAssetReceiveById(id);
    }
}
