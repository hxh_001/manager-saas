package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsInventoryOrder;

/**
 * 盘点管理Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsInventoryOrderService 
{
    /**
     * 查询盘点管理
     * 
     * @param id 盘点管理主键
     * @return 盘点管理
     */
    public MmsInventoryOrder selectMmsInventoryOrderById(Long id);

    /**
     * 查询盘点管理列表
     * 
     * @param mmsInventoryOrder 盘点管理
     * @return 盘点管理集合
     */
    public List<MmsInventoryOrder> selectMmsInventoryOrderList(MmsInventoryOrder mmsInventoryOrder);

    /**
     * 新增盘点管理
     * 
     * @param mmsInventoryOrder 盘点管理
     * @return 结果
     */
    public int insertMmsInventoryOrder(MmsInventoryOrder mmsInventoryOrder);

    /**
     * 修改盘点管理
     * 
     * @param mmsInventoryOrder 盘点管理
     * @return 结果
     */
    public int updateMmsInventoryOrder(MmsInventoryOrder mmsInventoryOrder);

    /**
     * 批量删除盘点管理
     * 
     * @param ids 需要删除的盘点管理主键集合
     * @return 结果
     */
    public int deleteMmsInventoryOrderByIds(Long[] ids);

    /**
     * 删除盘点管理信息
     * 
     * @param id 盘点管理主键
     * @return 结果
     */
    public int deleteMmsInventoryOrderById(Long id);
}
