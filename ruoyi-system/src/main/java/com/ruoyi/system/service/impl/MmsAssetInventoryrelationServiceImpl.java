package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAssetInventoryrelation;
import com.ruoyi.system.mapper.MmsAssetInventoryrelationMapper;
import com.ruoyi.system.service.IMmsAssetInventoryrelationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产盘库单-关联资产idService业务层处理
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Service
public class MmsAssetInventoryrelationServiceImpl extends ServiceImpl<MmsAssetInventoryrelationMapper, MmsAssetInventoryrelation> implements IMmsAssetInventoryrelationService {
    @Resource
    private MmsAssetInventoryrelationMapper mmsAssetInventoryrelationMapper;

    /**
     * 查询资产盘库单-关联资产id
     *
     * @param id 资产盘库单-关联资产id主键
     * @return 资产盘库单-关联资产id
     */
    @Override
    public MmsAssetInventoryrelation selectMmsAssetInventoryrelationById(Long id) {
        return mmsAssetInventoryrelationMapper.selectMmsAssetInventoryrelationById(id);
    }

    /**
     * 查询资产盘库单-关联资产id列表
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 资产盘库单-关联资产id
     */
    @Override
    public List<MmsAssetInventoryrelation> selectMmsAssetInventoryrelationList(MmsAssetInventoryrelation mmsAssetInventoryrelation) {
        return mmsAssetInventoryrelationMapper.selectMmsAssetInventoryrelationList(mmsAssetInventoryrelation);
    }

    /**
     * 新增资产盘库单-关联资产id
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 结果
     */
    @Override
    public int insertMmsAssetInventoryrelation(MmsAssetInventoryrelation mmsAssetInventoryrelation) {
        mmsAssetInventoryrelation.setCreateTime(DateUtils.getNowDate());
        mmsAssetInventoryrelation.setCreateBy(getUsername());
        return mmsAssetInventoryrelationMapper.insertMmsAssetInventoryrelation(mmsAssetInventoryrelation);
    }

    /**
     * 修改资产盘库单-关联资产id
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 结果
     */
    @Override
    public int updateMmsAssetInventoryrelation(MmsAssetInventoryrelation mmsAssetInventoryrelation) {
        mmsAssetInventoryrelation.setUpdateTime(DateUtils.getNowDate());
        mmsAssetInventoryrelation.setUpdateBy(getUsername());
        return mmsAssetInventoryrelationMapper.updateMmsAssetInventoryrelation(mmsAssetInventoryrelation);
    }

    /**
     * 批量删除资产盘库单-关联资产id
     *
     * @param ids 需要删除的资产盘库单-关联资产id主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetInventoryrelationByIds(Long[] ids) {
        return mmsAssetInventoryrelationMapper.deleteMmsAssetInventoryrelationByIds(ids);
    }

    /**
     * 删除资产盘库单-关联资产id信息
     *
     * @param id 资产盘库单-关联资产id主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetInventoryrelationById(Long id) {
        return mmsAssetInventoryrelationMapper.deleteMmsAssetInventoryrelationById(id);
    }
}