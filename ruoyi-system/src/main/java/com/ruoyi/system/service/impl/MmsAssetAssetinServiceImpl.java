package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Optional;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MmsAssetAssetinMapper;
import com.ruoyi.system.domain.MmsAssetAssetin;
import com.ruoyi.system.service.IMmsAssetAssetinService;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产入库单Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@Service
public class MmsAssetAssetinServiceImpl implements IMmsAssetAssetinService 
{
    @Autowired
    private MmsAssetAssetinMapper mmsAssetAssetinMapper;

    /**
     * 查询资产入库单
     * 
     * @param id 资产入库单主键
     * @return 资产入库单
     */
    @Override
    public MmsAssetAssetin selectMmsAssetAssetinById(Long id)
    {
        return mmsAssetAssetinMapper.selectMmsAssetAssetinById(id);
    }

    /**
     * 查询资产入库单列表
     * 
     * @param mmsAssetAssetin 资产入库单
     * @return 资产入库单
     */
    @Override
    public List<MmsAssetAssetin> selectMmsAssetAssetinList(MmsAssetAssetin mmsAssetAssetin)
    {
        return mmsAssetAssetinMapper.selectMmsAssetAssetinList(mmsAssetAssetin);
    }

    /**
     * 新增资产入库单
     * 
     * @param mmsAssetAssetin 资产入库单
     * @return 结果
     */
    @Override
    public int insertMmsAssetAssetin(MmsAssetAssetin mmsAssetAssetin)
    {
        Optional.ofNullable(mmsAssetAssetin).orElseThrow(() -> new RuntimeException("参数为空"));
      //  Optional.ofNullable(mmsAssetAssetin.getInTime()).orElseThrow(() -> new RuntimeException("入库日期为空"));
     //   Optional.ofNullable(mmsAssetAssetin.getInUserId()).orElseThrow(() -> new RuntimeException("入库人为空"));
        mmsAssetAssetin.setCreateTime(DateUtils.getNowDate());
        mmsAssetAssetin.setCreateBy(getUsername());
        return mmsAssetAssetinMapper.insertMmsAssetAssetin(mmsAssetAssetin);
    }

    /**
     * 修改资产入库单
     * 
     * @param mmsAssetAssetin 资产入库单
     * @return 结果
     */
    @Override
    public int updateMmsAssetAssetin(MmsAssetAssetin mmsAssetAssetin)
    {
        Optional.ofNullable(mmsAssetAssetin).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetAssetin.getInTime()).orElseThrow(() -> new RuntimeException("入库日期为空"));
        Optional.ofNullable(mmsAssetAssetin.getInUserId()).orElseThrow(() -> new RuntimeException("入库人为空"));
        mmsAssetAssetin.setUpdateTime(DateUtils.getNowDate());
        mmsAssetAssetin.setUpdateBy(getUsername());
        return mmsAssetAssetinMapper.updateMmsAssetAssetin(mmsAssetAssetin);
    }

    /**
     * 批量删除资产入库单
     * 
     * @param ids 需要删除的资产入库单主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetAssetinByIds(Long[] ids)
    {
        return mmsAssetAssetinMapper.deleteMmsAssetAssetinByIds(ids);
    }

    /**
     * 删除资产入库单信息
     * 
     * @param id 资产入库单主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetAssetinById(Long id)
    {
        return mmsAssetAssetinMapper.deleteMmsAssetAssetinById(id);
    }
}
