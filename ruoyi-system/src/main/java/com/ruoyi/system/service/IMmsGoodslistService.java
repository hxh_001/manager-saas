package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsGoodslist;

/**
 * 物品名目Service接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface IMmsGoodslistService 
{
    /**
     * 查询物品名目
     * 
     * @param id 物品名目主键
     * @return 物品名目
     */
    public MmsGoodslist selectMmsGoodslistById(Long id);

    /**
     * 查询物品名目列表
     * 
     * @param mmsGoodslist 物品名目
     * @return 物品名目集合
     */
    public List<MmsGoodslist> selectMmsGoodslistList(MmsGoodslist mmsGoodslist);

    /**
     * 新增物品名目
     * 
     * @param mmsGoodslist 物品名目
     * @return 结果
     */
    public int insertMmsGoodslist(MmsGoodslist mmsGoodslist);

    /**
     * 修改物品名目
     * 
     * @param mmsGoodslist 物品名目
     * @return 结果
     */
    public int updateMmsGoodslist(MmsGoodslist mmsGoodslist);

    /**
     * 批量删除物品名目
     * 
     * @param ids 需要删除的物品名目主键集合
     * @return 结果
     */
    public int deleteMmsGoodslistByIds(Long[] ids);

    /**
     * 删除物品名目信息
     * 
     * @param id 物品名目主键
     * @return 结果
     */
    public int deleteMmsGoodslistById(Long id);
}
