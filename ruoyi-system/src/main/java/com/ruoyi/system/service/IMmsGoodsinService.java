package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsGoodsin;

/**
 * 物品入库单Service接口
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public interface IMmsGoodsinService 
{
    /**
     * 查询物品入库单
     * 
     * @param id 物品入库单主键
     * @return 物品入库单
     */
    public MmsGoodsin selectMmsGoodsinById(Long id);

    /**
     * 查询物品入库单列表
     * 
     * @param mmsGoodsin 物品入库单
     * @return 物品入库单集合
     */
    public List<MmsGoodsin> selectMmsGoodsinList(MmsGoodsin mmsGoodsin);

    /**
     * 新增物品入库单
     * 
     * @param mmsGoodsin 物品入库单
     * @return 结果
     */
    public int insertMmsGoodsin(MmsGoodsin mmsGoodsin);

    /**
     * 修改物品入库单
     * 
     * @param mmsGoodsin 物品入库单
     * @return 结果
     */
    public int updateMmsGoodsin(MmsGoodsin mmsGoodsin);

    /**
     * 批量删除物品入库单
     * 
     * @param ids 需要删除的物品入库单主键集合
     * @return 结果
     */
    public int deleteMmsGoodsinByIds(Long[] ids);

    /**
     * 删除物品入库单信息
     * 
     * @param id 物品入库单主键
     * @return 结果
     */
    public int deleteMmsGoodsinById(Long id);
}
