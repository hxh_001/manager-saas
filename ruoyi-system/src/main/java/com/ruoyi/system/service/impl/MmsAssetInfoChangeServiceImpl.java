package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetInfoChange;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.mapper.MmsAssetInfoChangeMapper;
import com.ruoyi.system.service.IMmsAssetInfoChangeService;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产信息变更Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetInfoChangeServiceImpl implements IMmsAssetInfoChangeService 
{
    @Autowired
    private MmsAssetInfoChangeMapper mmsAssetInfoChangeMapper;


    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;

    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    /**
     * 查询资产信息变更
     * 
     * @param id 资产信息变更主键
     * @return 资产信息变更
     */
    @Override
    public MmsAssetInfoChange selectMmsAssetInfoChangeById(Long id)
    {
        MmsAssetInfoChange model = mmsAssetInfoChangeMapper.selectMmsAssetInfoChangeById(id);
        List<MmsAsset>  assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.INFO_EXCHANGE.getType());
        model.setAssetList(assetList);
        return model;
    }

    /**
     * 查询资产信息变更列表
     * 
     * @param mmsAssetInfoChange 资产信息变更
     * @return 资产信息变更
     */
    @Override
    public List<MmsAssetInfoChange> selectMmsAssetInfoChangeList(MmsAssetInfoChange mmsAssetInfoChange)
    {
        return mmsAssetInfoChangeMapper.selectMmsAssetInfoChangeList(mmsAssetInfoChange);
    }

    /**
     * 新增资产信息变更
     * 
     * @param mmsAssetInfoChange 资产信息变更
     * @return 结果
     */
    @Override
    public int insertMmsAssetInfoChange(MmsAssetInfoChange mmsAssetInfoChange)
    {
        Optional.ofNullable(mmsAssetInfoChange).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetInfoChange.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetInfoChange.setCreateTime(DateUtils.getNowDate());
        mmsAssetInfoChange.setCreateBy(getUsername());
        int count = mmsAssetInfoChangeMapper.insertMmsAssetInfoChange(mmsAssetInfoChange);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetInfoChange.getId(), mmsAssetInfoChange.getAssetIds(), MmsAssetRelationEnum.INFO_EXCHANGE.getType());
        return count;
    }

    /**
     * 修改资产信息变更
     * 
     * @param mmsAssetInfoChange 资产信息变更
     * @return 结果
     */
    @Override
    public int updateMmsAssetInfoChange(MmsAssetInfoChange mmsAssetInfoChange)
    {
        Optional.ofNullable(mmsAssetInfoChange).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetInfoChange.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetInfoChange.setUpdateTime(DateUtils.getNowDate());
        mmsAssetInfoChange.setUpdateBy(getUsername());
        return mmsAssetInfoChangeMapper.updateMmsAssetInfoChange(mmsAssetInfoChange);
    }

    /**
     * 批量删除资产信息变更
     * 
     * @param ids 需要删除的资产信息变更主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetInfoChangeByIds(Long[] ids)
    {
        return mmsAssetInfoChangeMapper.deleteMmsAssetInfoChangeByIds(ids);
    }

    /**
     * 删除资产信息变更信息
     * 
     * @param id 资产信息变更主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetInfoChangeById(Long id)
    {
        return mmsAssetInfoChangeMapper.deleteMmsAssetInfoChangeById(id);
    }
}
