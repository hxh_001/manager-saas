package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsSupplier;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 供应商管理Service接口
 *
 * @author ruoyi
 * @date 2024-08-30
 */
public interface IMmsSupplierService extends IService<MmsSupplier> {
    /**
     * 查询供应商管理
     *
     * @param id 供应商管理主键
     * @return 供应商管理
     */
    public MmsSupplier selectMmsSupplierById(Long id);

    /**
     * 查询供应商管理列表
     *
     * @param mmsSupplier 供应商管理
     * @return 供应商管理集合
     */
    public List<MmsSupplier> selectMmsSupplierList(MmsSupplier mmsSupplier);

    /**
     * 新增供应商管理
     *
     * @param mmsSupplier 供应商管理
     * @return 结果
     */
    public int insertMmsSupplier(MmsSupplier mmsSupplier);

    /**
     * 修改供应商管理
     *
     * @param mmsSupplier 供应商管理
     * @return 结果
     */
    public int updateMmsSupplier(MmsSupplier mmsSupplier);

    /**
     * 批量删除供应商管理
     *
     * @param ids 需要删除的供应商管理主键集合
     * @return 结果
     */
    public int deleteMmsSupplierByIds(Long[] ids);

    /**
     * 删除供应商管理信息
     *
     * @param id 供应商管理主键
     * @return 结果
     */
    public int deleteMmsSupplierById(Long id);
}
