package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.MmsAssetReturn;
import com.ruoyi.system.enums.MmsAssetRelationEnum;
import com.ruoyi.system.mapper.MmsAssetReturnMapper;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.system.service.IMmsAssetReturnService;
import com.ruoyi.system.service.IMmsAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

/**
 * 资产归还Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class MmsAssetReturnServiceImpl implements IMmsAssetReturnService 
{
    @Autowired
    private MmsAssetReturnMapper mmsAssetReturnMapper;

    @Autowired
    private IMmsAssetRelationService mmsAssetRelationServiceImpl;

    @Autowired
    private IMmsAssetService mmsAssetServiceImpl;

    @Resource
    private Generator generator;

    /**
     * 查询资产归还
     * 
     * @param id 资产归还主键
     * @return 资产归还
     */
    @Override
    public MmsAssetReturn selectMmsAssetReturnById(Long id)
    {
        MmsAssetReturn model =  mmsAssetReturnMapper.selectMmsAssetReturnById(id);
        List<MmsAsset>  assetList = mmsAssetServiceImpl.selectMmsAssetByRelationId(id, MmsAssetRelationEnum.RETURN.getType());
        model.setAssetList(assetList);
        return model;
    }

    /**
     * 查询资产归还列表
     * 
     * @param mmsAssetReturn 资产归还
     * @return 资产归还
     */
    @Override
    public List<MmsAssetReturn> selectMmsAssetReturnList(MmsAssetReturn mmsAssetReturn)
    {
        return mmsAssetReturnMapper.selectMmsAssetReturnList(mmsAssetReturn);
    }

    /**
     * 新增资产归还
     * 
     * @param mmsAssetReturn 资产归还
     * @return 结果
     */
    @Override
    public int insertMmsAssetReturn(MmsAssetReturn mmsAssetReturn)
    {
        Optional.ofNullable(mmsAssetReturn).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetReturn.getBorrowDate()).orElseThrow(() -> new RuntimeException("归还日期为空"));
        Optional.ofNullable(mmsAssetReturn.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetReturn.setReturnNo(generator.generateDocumentNumber(MmsAssetRelationEnum.RETURN.getType()));
        mmsAssetReturn.setCreateTime(DateUtils.getNowDate());
        mmsAssetReturn.setCreateBy(getUsername());
        int count = mmsAssetReturnMapper.insertMmsAssetReturn(mmsAssetReturn);
        mmsAssetRelationServiceImpl.addRelation(mmsAssetReturn.getId(), mmsAssetReturn.getAssetIds(), MmsAssetRelationEnum.RETURN.getType());
        return count;
    }

    /**
     * 修改资产归还
     * 
     * @param mmsAssetReturn 资产归还
     * @return 结果
     */
    @Override
    public int updateMmsAssetReturn(MmsAssetReturn mmsAssetReturn)
    {
        Optional.ofNullable(mmsAssetReturn).orElseThrow(() -> new RuntimeException("参数为空"));
        Optional.ofNullable(mmsAssetReturn.getBorrowDate()).orElseThrow(() -> new RuntimeException("归还日期为空"));
        Optional.ofNullable(mmsAssetReturn.getAssetIds()).orElseThrow(() -> new RuntimeException("资产为空"));
        mmsAssetReturn.setUpdateTime(DateUtils.getNowDate());
        mmsAssetReturn.setUpdateBy(getUsername());
        return mmsAssetReturnMapper.updateMmsAssetReturn(mmsAssetReturn);
    }

    /**
     * 批量删除资产归还
     * 
     * @param ids 需要删除的资产归还主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetReturnByIds(Long[] ids)
    {
        return mmsAssetReturnMapper.deleteMmsAssetReturnByIds(ids);
    }

    /**
     * 删除资产归还信息
     * 
     * @param id 资产归还主键
     * @return 结果
     */
    @Override
    public int deleteMmsAssetReturnById(Long id)
    {
        return mmsAssetReturnMapper.deleteMmsAssetReturnById(id);
    }
}
