package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetClassify;
import com.ruoyi.system.domain.vo.MmsStatClassify;

/**
 * 资产分类Service接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface IMmsAssetClassifyService 
{
    /**
     * 查询资产分类
     * 
     * @param id 资产分类主键
     * @return 资产分类
     */
    public MmsAssetClassify selectMmsAssetClassifyById(Long id);

    /**
     * 查询资产分类列表
     * 
     * @param mmsAssetClassify 资产分类
     * @return 资产分类集合
     */
    public List<MmsAssetClassify> selectMmsAssetClassifyList(MmsAssetClassify mmsAssetClassify);

    /**
     * 新增资产分类
     * 
     * @param mmsAssetClassify 资产分类
     * @return 结果
     */
    public int insertMmsAssetClassify(MmsAssetClassify mmsAssetClassify);

    /**
     * 修改资产分类
     * 
     * @param mmsAssetClassify 资产分类
     * @return 结果
     */
    public int updateMmsAssetClassify(MmsAssetClassify mmsAssetClassify);

    /**
     * 批量删除资产分类
     * 
     * @param ids 需要删除的资产分类主键集合
     * @return 结果
     */
    public int deleteMmsAssetClassifyByIds(Long[] ids);

    /**
     * 删除资产分类信息
     * 
     * @param id 资产分类主键
     * @return 结果
     */
    public int deleteMmsAssetClassifyById(Long id);


    /**
     * 资产分类统计
     * @return
     */
    List<MmsStatClassify> statClassify();
}
