package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetNoRule;

/**
 * 编码规则Service接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface IMmsAssetNoRuleService 
{
    /**
     * 查询编码规则
     * 
     * @param id 编码规则主键
     * @return 编码规则
     */
    public MmsAssetNoRule selectMmsAssetNoRuleById(Long id);

    /**
     * 查询编码规则列表
     * 
     * @param mmsAssetNoRule 编码规则
     * @return 编码规则集合
     */
    public List<MmsAssetNoRule> selectMmsAssetNoRuleList(MmsAssetNoRule mmsAssetNoRule);

    /**
     * 新增编码规则
     * 
     * @param mmsAssetNoRule 编码规则
     * @return 结果
     */
    public int insertMmsAssetNoRule(MmsAssetNoRule mmsAssetNoRule);

    /**
     * 修改编码规则
     * 
     * @param mmsAssetNoRule 编码规则
     * @return 结果
     */
    public int updateMmsAssetNoRule(MmsAssetNoRule mmsAssetNoRule);

    /**
     * 批量删除编码规则
     * 
     * @param ids 需要删除的编码规则主键集合
     * @return 结果
     */
    public int deleteMmsAssetNoRuleByIds(Long[] ids);

    /**
     * 删除编码规则信息
     * 
     * @param id 编码规则主键
     * @return 结果
     */
    public int deleteMmsAssetNoRuleById(Long id);
}
