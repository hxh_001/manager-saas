package com.ruoyi.system.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.system.domain.MmsAssetNoRule;
import com.ruoyi.system.mapper.MmsAssetNoRuleMapper;
import com.ruoyi.system.utils.DateUtilCommon;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class MmsAssetNoRecordService {

  
    
    @Autowired
    private MmsAssetNoRuleMapper mmsAssetNoRuleMapper ;
    
    
   

    public synchronized String queryNextNo() {
    	QueryWrapper<MmsAssetNoRule> query = new QueryWrapper<MmsAssetNoRule>();
    	query.eq("is_on", 1);
    	MmsAssetNoRule ruleModel = mmsAssetNoRuleMapper.selectOne(query);
        if (Objects.isNull( ruleModel)) {
            //throw new BizException("请设置编码规则");
        }
        String noRule = ruleModel.getNoRule();
        String[] noRules = noRule.split("&&&");//编码规则
        StringBuffer sb = new StringBuffer();
        sb = sb.append("ZC");
    	sb.append(DateUtilCommon.getSimpleDateStr1(new Date()));
        int number = ruleModel.getAutoNum() == null ? 0 :  ruleModel.getAutoNum();
        number++;
        sb.append(number);
        ruleModel.setAutoNum(number);
    	mmsAssetNoRuleMapper.updateById(ruleModel);
        return sb.toString();
    }
    
	/**
	 * 导入专用-资产编码
	 * 
	 * @param length
	 * @return
	 */
	public synchronized JSONObject getImportAssetNo(int length) {
		
		QueryWrapper<MmsAssetNoRule> query = new QueryWrapper<MmsAssetNoRule>();
		query.eq("is_on", 1);
		MmsAssetNoRule ruleModel = mmsAssetNoRuleMapper.selectOne(query);
		String noRule = ruleModel.getNoRule();
		String[] noRules = noRule.split("&&&");// 编码规则
		StringBuffer sb = new StringBuffer();
		sb = sb.append("ZC");
		sb.append(DateUtilCommon.getSimpleDateStr1(new Date()));
		int number = ruleModel.getAutoNum() == null ? 0 : ruleModel.getAutoNum();
		ruleModel.setAutoNum(number + length);
		mmsAssetNoRuleMapper.updateById(ruleModel);
		JSONObject json = new JSONObject();
		json.put("start", sb.toString());
		json.put("size", number);
		return json;

	}
    

   


    private String generatorString(Integer size, Long no) {
        String noStr = String.valueOf(no);
        int noLength = noStr.length();

        String var1 = "";
        if (noLength < size) {
            for (int i = 0; i < (size - noLength); i++) {
                var1 += "0";
            }
        }

        return var1;
    }




    public static void main(String[] args) {

        String abc = "123456789";
        System.out.println(abc.substring(0, abc.length() -4));
    }
}
