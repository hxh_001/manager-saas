package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysMerchant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商户Service接口
 *
 * @author ruoyi
 * @date 2024-07-15
 */
public interface ISysMerchantService extends IService<SysMerchant> {
    /**
     * 查询商户
     *
     * @param merchantId 商户主键
     * @return 商户
     */
    public SysMerchant selectSysMerchantByMerchantId(Long merchantId);

    /**
     * 查询商户列表
     *
     * @param sysMerchant 商户
     * @return 商户集合
     */
    public List<SysMerchant> selectSysMerchantList(SysMerchant sysMerchant);

    /**
     * 新增商户
     *
     * @param sysMerchant 商户
     * @return 结果
     */
    public int insertSysMerchant(SysMerchant sysMerchant);

    /**
     * 修改商户
     *
     * @param sysMerchant 商户
     * @return 结果
     */
    public int updateSysMerchant(SysMerchant sysMerchant);

    /**
     * 批量删除商户
     *
     * @param merchantIds 需要删除的商户主键集合
     * @return 结果
     */
    public int deleteSysMerchantByMerchantIds(Long[] merchantIds);

    /**
     * 删除商户信息
     *
     * @param merchantId 商户主键
     * @return 结果
     */
    public int deleteSysMerchantByMerchantId(Long merchantId);
}
