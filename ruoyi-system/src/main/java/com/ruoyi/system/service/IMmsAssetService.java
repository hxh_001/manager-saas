package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.vo.MmsAssetStat;
import com.ruoyi.system.domain.vo.MmsStatMonthAsset;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * 资产信息Service接口
 * 
 * @author ruoyi
 * @date 2024-06-18
 */
public interface IMmsAssetService  extends IService<MmsAsset>
{
    /**
     * 查询资产信息
     * 
     * @param id 资产信息主键
     * @return 资产信息
     */
    public MmsAsset selectMmsAssetById(Long id);

    /**
     * 查询资产信息列表
     * 
     * @param mmsAsset 资产信息
     * @return 资产信息集合
     */
    public List<MmsAsset> selectMmsAssetList(MmsAsset mmsAsset);

    /**
     * 新增资产信息
     * 
     * @param mmsAsset 资产信息
     * @return 结果
     */
    public int insertMmsAsset(MmsAsset mmsAsset);

    /**
     * 修改资产信息
     * 
     * @param mmsAsset 资产信息
     * @return 结果
     */
    public int updateMmsAsset(MmsAsset mmsAsset);

    /**
     * 批量删除资产信息
     * 
     * @param ids 需要删除的资产信息主键集合
     * @return 结果
     */
    public int deleteMmsAssetByIds(Long[] ids);

    /**
     * 删除资产信息信息
     * 
     * @param id 资产信息主键
     * @return 结果
     */
    public int deleteMmsAssetById(Long id);

    /**
     * 导入资产数据
     *
     * @param assetList 资产数据列表
     * @return 结果
     * @throws Exception 
     */
    public void importAsset(List<MmsAsset> assetList, CountDownLatch countDownLatch) throws Exception;

	List<MmsAsset> selectMmsAssetByRelationId(Long functionId, String assetFunction);

    /**
     * 首页统计固定资产
     *
     * @return
     */
    MmsAssetStat statAsset();

    /**
     * 首页统计月资产
     *
     * @return
     */
    MmsStatMonthAsset statMonth();
}
