package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.MmsAsset;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 资产信息Mapper接口
 *
 * @author ruoyi
 * @date 2024-06-18
 */
public interface MmsAssetMapper extends BaseMapper<MmsAsset> {
    /**
     * 查询资产信息
     *
     * @param id 资产信息主键
     * @return 资产信息
     */
    public MmsAsset selectMmsAssetById(Long id);

    /**
     * 查询资产名称是否存在
     *
     * @param assetName
     * @return
     */
    public MmsAsset selectAssetByName(String assetName);

    /**
     * 查询资产信息列表
     *
     * @param mmsAsset 资产信息
     * @return 资产信息集合
     */
    public List<MmsAsset> selectMmsAssetList(MmsAsset mmsAsset);

    /**
     * 新增资产信息
     *
     * @param mmsAsset 资产信息
     * @return 结果
     */
    public int insertMmsAsset(MmsAsset mmsAsset);

    /**
     * 修改资产信息
     *
     * @param mmsAsset 资产信息
     * @return 结果
     */
    public int updateMmsAsset(MmsAsset mmsAsset);

    /**
     * 删除资产信息
     *
     * @param id 资产信息主键
     * @return 结果
     */
    public int deleteMmsAssetById(Long id);

    /**
     * 批量删除资产信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetByIds(Long[] ids);


    public List<MmsAsset> selectMmsAssetByRelationId(@Param("functionId") Long functionId, @Param("assetFunction") String assetFunction);
}
