package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetScrap;

/**
 * 资产报废Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface MmsAssetScrapMapper 
{
    /**
     * 查询资产报废
     * 
     * @param id 资产报废主键
     * @return 资产报废
     */
    public MmsAssetScrap selectMmsAssetScrapById(Long id);

    /**
     * 查询资产报废列表
     * 
     * @param mmsAssetScrap 资产报废
     * @return 资产报废集合
     */
    public List<MmsAssetScrap> selectMmsAssetScrapList(MmsAssetScrap mmsAssetScrap);

    /**
     * 新增资产报废
     * 
     * @param mmsAssetScrap 资产报废
     * @return 结果
     */
    public int insertMmsAssetScrap(MmsAssetScrap mmsAssetScrap);

    /**
     * 修改资产报废
     * 
     * @param mmsAssetScrap 资产报废
     * @return 结果
     */
    public int updateMmsAssetScrap(MmsAssetScrap mmsAssetScrap);

    /**
     * 删除资产报废
     * 
     * @param id 资产报废主键
     * @return 结果
     */
    public int deleteMmsAssetScrapById(Long id);

    /**
     * 批量删除资产报废
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetScrapByIds(Long[] ids);
}
