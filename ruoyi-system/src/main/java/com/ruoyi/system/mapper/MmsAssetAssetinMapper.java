package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetAssetin;

/**
 * 资产入库单Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public interface MmsAssetAssetinMapper 
{
    /**
     * 查询资产入库单
     * 
     * @param id 资产入库单主键
     * @return 资产入库单
     */
    public MmsAssetAssetin selectMmsAssetAssetinById(Long id);

    /**
     * 查询资产入库单列表
     * 
     * @param mmsAssetAssetin 资产入库单
     * @return 资产入库单集合
     */
    public List<MmsAssetAssetin> selectMmsAssetAssetinList(MmsAssetAssetin mmsAssetAssetin);

    /**
     * 新增资产入库单
     * 
     * @param mmsAssetAssetin 资产入库单
     * @return 结果
     */
    public int insertMmsAssetAssetin(MmsAssetAssetin mmsAssetAssetin);

    /**
     * 修改资产入库单
     * 
     * @param mmsAssetAssetin 资产入库单
     * @return 结果
     */
    public int updateMmsAssetAssetin(MmsAssetAssetin mmsAssetAssetin);

    /**
     * 删除资产入库单
     * 
     * @param id 资产入库单主键
     * @return 结果
     */
    public int deleteMmsAssetAssetinById(Long id);

    /**
     * 批量删除资产入库单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetAssetinByIds(Long[] ids);
}
