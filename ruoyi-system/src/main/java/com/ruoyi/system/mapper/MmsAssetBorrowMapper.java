package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetBorrow;

/**
 * 资产借用Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface MmsAssetBorrowMapper 
{
    /**
     * 查询资产借用
     * 
     * @param id 资产借用主键
     * @return 资产借用
     */
    public MmsAssetBorrow selectMmsAssetBorrowById(Long id);

    /**
     * 查询资产借用列表
     * 
     * @param mmsAssetBorrow 资产借用
     * @return 资产借用集合
     */
    public List<MmsAssetBorrow> selectMmsAssetBorrowList(MmsAssetBorrow mmsAssetBorrow);

    /**
     * 新增资产借用
     * 
     * @param mmsAssetBorrow 资产借用
     * @return 结果
     */
    public int insertMmsAssetBorrow(MmsAssetBorrow mmsAssetBorrow);

    /**
     * 修改资产借用
     * 
     * @param mmsAssetBorrow 资产借用
     * @return 结果
     */
    public int updateMmsAssetBorrow(MmsAssetBorrow mmsAssetBorrow);

    /**
     * 删除资产借用
     * 
     * @param id 资产借用主键
     * @return 结果
     */
    public int deleteMmsAssetBorrowById(Long id);

    /**
     * 批量删除资产借用
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetBorrowByIds(Long[] ids);
}
