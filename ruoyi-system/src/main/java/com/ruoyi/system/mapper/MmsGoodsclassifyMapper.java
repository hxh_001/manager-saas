package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsGoodsclassify;

/**
 * 物品分类Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface MmsGoodsclassifyMapper 
{
    /**
     * 查询物品分类
     * 
     * @param id 物品分类主键
     * @return 物品分类
     */
    public MmsGoodsclassify selectMmsGoodsclassifyById(Long id);

    /**
     * 查询物品分类列表
     * 
     * @param mmsGoodsclassify 物品分类
     * @return 物品分类集合
     */
    public List<MmsGoodsclassify> selectMmsGoodsclassifyList(MmsGoodsclassify mmsGoodsclassify);

    /**
     * 新增物品分类
     * 
     * @param mmsGoodsclassify 物品分类
     * @return 结果
     */
    public int insertMmsGoodsclassify(MmsGoodsclassify mmsGoodsclassify);

    /**
     * 修改物品分类
     * 
     * @param mmsGoodsclassify 物品分类
     * @return 结果
     */
    public int updateMmsGoodsclassify(MmsGoodsclassify mmsGoodsclassify);

    /**
     * 删除物品分类
     * 
     * @param id 物品分类主键
     * @return 结果
     */
    public int deleteMmsGoodsclassifyById(Long id);

    /**
     * 批量删除物品分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsGoodsclassifyByIds(Long[] ids);
}
