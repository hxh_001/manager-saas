package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.MmsAssetInventoryrelation;

/**
 * 资产盘库单-关联资产idMapper接口
 *
 * @author ruoyi
 * @date 2024-07-20
 */
public interface MmsAssetInventoryrelationMapper extends BaseMapper<MmsAssetInventoryrelation> {
    /**
     * 查询资产盘库单-关联资产id
     *
     * @param id 资产盘库单-关联资产id主键
     * @return 资产盘库单-关联资产id
     */
    public MmsAssetInventoryrelation selectMmsAssetInventoryrelationById(Long id);

    /**
     * 查询资产盘库单-关联资产id列表
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 资产盘库单-关联资产id集合
     */
    public List<MmsAssetInventoryrelation> selectMmsAssetInventoryrelationList(MmsAssetInventoryrelation mmsAssetInventoryrelation);

    /**
     * 新增资产盘库单-关联资产id
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 结果
     */
    public int insertMmsAssetInventoryrelation(MmsAssetInventoryrelation mmsAssetInventoryrelation);

    /**
     * 修改资产盘库单-关联资产id
     *
     * @param mmsAssetInventoryrelation 资产盘库单-关联资产id
     * @return 结果
     */
    public int updateMmsAssetInventoryrelation(MmsAssetInventoryrelation mmsAssetInventoryrelation);

    /**
     * 删除资产盘库单-关联资产id
     *
     * @param id 资产盘库单-关联资产id主键
     * @return 结果
     */
    public int deleteMmsAssetInventoryrelationById(Long id);

    /**
     * 批量删除资产盘库单-关联资产id
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetInventoryrelationByIds(Long[] ids);
}
