package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.MmsAssetDepreciation;

import java.util.Date;
import java.util.List;

/**
 * 资产折旧Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface MmsAssetDepreciationMapper extends BaseMapper<MmsAssetDepreciation>
{
    /**
     * 查询资产折旧
     * 
     * @param id 资产折旧主键
     * @return 资产折旧
     */
    public MmsAssetDepreciation selectMmsAssetDepreciationById(Long id);

    /**
     * 查询资产折旧列表
     * 
     * @param mmsAssetDepreciation 资产折旧
     * @return 资产折旧集合
     */
    public List<MmsAssetDepreciation> selectMmsAssetDepreciationList(MmsAssetDepreciation mmsAssetDepreciation);

    /**
     * 查询指定月的数据
     * @param month
     * @return
     */
    List<MmsAssetDepreciation> selectEqMonth(Date month);

    /**
     * 新增资产折旧
     * 
     * @param mmsAssetDepreciation 资产折旧
     * @return 结果
     */
    public int insertMmsAssetDepreciation(MmsAssetDepreciation mmsAssetDepreciation);

    /**
     * 修改资产折旧
     * 
     * @param mmsAssetDepreciation 资产折旧
     * @return 结果
     */
    public int updateMmsAssetDepreciation(MmsAssetDepreciation mmsAssetDepreciation);

    /**
     * 删除资产折旧
     * 
     * @param id 资产折旧主键
     * @return 结果
     */
    public int deleteMmsAssetDepreciationById(Long id);

    /**
     * 批量删除资产折旧
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetDepreciationByIds(Long[] ids);
}
