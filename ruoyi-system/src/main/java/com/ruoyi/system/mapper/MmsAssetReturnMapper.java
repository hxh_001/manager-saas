package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetReturn;

/**
 * 资产归还Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface MmsAssetReturnMapper 
{
    /**
     * 查询资产归还
     * 
     * @param id 资产归还主键
     * @return 资产归还
     */
    public MmsAssetReturn selectMmsAssetReturnById(Long id);

    /**
     * 查询资产归还列表
     * 
     * @param mmsAssetReturn 资产归还
     * @return 资产归还集合
     */
    public List<MmsAssetReturn> selectMmsAssetReturnList(MmsAssetReturn mmsAssetReturn);

    /**
     * 新增资产归还
     * 
     * @param mmsAssetReturn 资产归还
     * @return 结果
     */
    public int insertMmsAssetReturn(MmsAssetReturn mmsAssetReturn);

    /**
     * 修改资产归还
     * 
     * @param mmsAssetReturn 资产归还
     * @return 结果
     */
    public int updateMmsAssetReturn(MmsAssetReturn mmsAssetReturn);

    /**
     * 删除资产归还
     * 
     * @param id 资产归还主键
     * @return 结果
     */
    public int deleteMmsAssetReturnById(Long id);

    /**
     * 批量删除资产归还
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetReturnByIds(Long[] ids);
}
