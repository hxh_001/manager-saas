package com.ruoyi.system.mapper;

import com.ruoyi.system.generator.NumberMapperProvider;
import org.apache.ibatis.annotations.SelectProvider;

/**
 * 查询最大编号
 */

public interface NumberMapper {
    @SelectProvider(type = NumberMapperProvider.class, method = "queryNumber")
    String queryNumber(String tableName, String numberCol, String prefix);
}

