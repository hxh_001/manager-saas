package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsAssetRepair;

/**
 * 资产维修Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface MmsAssetRepairMapper 
{
    /**
     * 查询资产维修
     * 
     * @param id 资产维修主键
     * @return 资产维修
     */
    public MmsAssetRepair selectMmsAssetRepairById(Long id);

    /**
     * 查询资产维修列表
     * 
     * @param mmsAssetRepair 资产维修
     * @return 资产维修集合
     */
    public List<MmsAssetRepair> selectMmsAssetRepairList(MmsAssetRepair mmsAssetRepair);

    /**
     * 新增资产维修
     * 
     * @param mmsAssetRepair 资产维修
     * @return 结果
     */
    public int insertMmsAssetRepair(MmsAssetRepair mmsAssetRepair);

    /**
     * 修改资产维修
     * 
     * @param mmsAssetRepair 资产维修
     * @return 结果
     */
    public int updateMmsAssetRepair(MmsAssetRepair mmsAssetRepair);

    /**
     * 删除资产维修
     * 
     * @param id 资产维修主键
     * @return 结果
     */
    public int deleteMmsAssetRepairById(Long id);

    /**
     * 批量删除资产维修
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAssetRepairByIds(Long[] ids);
}
