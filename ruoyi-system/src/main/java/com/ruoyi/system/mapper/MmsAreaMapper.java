package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.MmsArea;

/**
 * 区域Mapper接口
 *
 * @author ruoyi
 * @date 2024-08-26
 */
public interface MmsAreaMapper extends BaseMapper<MmsArea> {
    /**
     * 查询区域
     *
     * @param id 区域主键
     * @return 区域
     */
    public MmsArea selectMmsAreaById(Long id);

    /**
     * 查询区域列表
     *
     * @param mmsArea 区域
     * @return 区域集合
     */
    public List<MmsArea> selectMmsAreaList(MmsArea mmsArea);

    /**
     * 新增区域
     *
     * @param mmsArea 区域
     * @return 结果
     */
    public int insertMmsArea(MmsArea mmsArea);

    /**
     * 修改区域
     *
     * @param mmsArea 区域
     * @return 结果
     */
    public int updateMmsArea(MmsArea mmsArea);

    /**
     * 删除区域
     *
     * @param id 区域主键
     * @return 结果
     */
    public int deleteMmsAreaById(Long id);

    /**
     * 批量删除区域
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsAreaByIds(Long[] ids);
}
