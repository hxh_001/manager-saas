package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsGoodsinrelation;

/**
 * 物品入库单-关联物品idMapper接口
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public interface MmsGoodsinrelationMapper 
{
    /**
     * 查询物品入库单-关联物品id
     * 
     * @param id 物品入库单-关联物品id主键
     * @return 物品入库单-关联物品id
     */
    public MmsGoodsinrelation selectMmsGoodsinrelationById(Long id);

    /**
     * 查询物品入库单-关联物品id列表
     * 
     * @param mmsGoodsinrelation 物品入库单-关联物品id
     * @return 物品入库单-关联物品id集合
     */
    public List<MmsGoodsinrelation> selectMmsGoodsinrelationList(MmsGoodsinrelation mmsGoodsinrelation);

    /**
     * 新增物品入库单-关联物品id
     * 
     * @param mmsGoodsinrelation 物品入库单-关联物品id
     * @return 结果
     */
    public int insertMmsGoodsinrelation(MmsGoodsinrelation mmsGoodsinrelation);

    /**
     * 修改物品入库单-关联物品id
     * 
     * @param mmsGoodsinrelation 物品入库单-关联物品id
     * @return 结果
     */
    public int updateMmsGoodsinrelation(MmsGoodsinrelation mmsGoodsinrelation);

    /**
     * 删除物品入库单-关联物品id
     * 
     * @param id 物品入库单-关联物品id主键
     * @return 结果
     */
    public int deleteMmsGoodsinrelationById(Long id);

    /**
     * 批量删除物品入库单-关联物品id
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsGoodsinrelationByIds(Long[] ids);
}
