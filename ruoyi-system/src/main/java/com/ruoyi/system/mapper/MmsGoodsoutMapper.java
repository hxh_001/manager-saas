package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MmsGoodsout;

/**
 * 物品出库单（关联入库单）Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
public interface MmsGoodsoutMapper 
{
    /**
     * 查询物品出库单（关联入库单）
     * 
     * @param id 物品出库单（关联入库单）主键
     * @return 物品出库单（关联入库单）
     */
    public MmsGoodsout selectMmsGoodsoutById(Long id);

    /**
     * 查询物品出库单（关联入库单）列表
     * 
     * @param mmsGoodsout 物品出库单（关联入库单）
     * @return 物品出库单（关联入库单）集合
     */
    public List<MmsGoodsout> selectMmsGoodsoutList(MmsGoodsout mmsGoodsout);

    /**
     * 新增物品出库单（关联入库单）
     * 
     * @param mmsGoodsout 物品出库单（关联入库单）
     * @return 结果
     */
    public int insertMmsGoodsout(MmsGoodsout mmsGoodsout);

    /**
     * 修改物品出库单（关联入库单）
     * 
     * @param mmsGoodsout 物品出库单（关联入库单）
     * @return 结果
     */
    public int updateMmsGoodsout(MmsGoodsout mmsGoodsout);

    /**
     * 删除物品出库单（关联入库单）
     * 
     * @param id 物品出库单（关联入库单）主键
     * @return 结果
     */
    public int deleteMmsGoodsoutById(Long id);

    /**
     * 批量删除物品出库单（关联入库单）
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmsGoodsoutByIds(Long[] ids);
}
