package com.ruoyi.system.utils;

import com.ruoyi.common.utils.uuid.UUID;

public class StringUtils {

	/**
	 * 生成订单号
	 * @param pr
	 * @return
	 */
	
	public  static  String createOrderNo(String pr) {
		return pr+UUID.randomUUID().toString();
	}
}
