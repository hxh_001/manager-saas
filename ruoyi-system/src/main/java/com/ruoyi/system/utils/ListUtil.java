package com.ruoyi.system.utils;





import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson2.JSONObject;

public class ListUtil {
    /**
     * 将list source 平均分割N分
     *
     * @param source
     * @param n
     * @param <T>
     * @return
     */
    public static <T> List<List<T>> averageAssign(List<T> source, int n) {
        List<List<T>> result = new ArrayList<List<T>>();
        int remaider = source.size() % n;  //(先计算出余数)
        int number = source.size() / n;  //然后是商
        int offset = 0;//偏移量
        for (int i = 0; i < n; i++) {
            List<T> value = null;
            if (remaider > 0) {
                value = source.subList(i * number + offset, (i + 1) * number + offset + 1);
                remaider--;
                offset++;
            } else {
                value = source.subList(i * number + offset, (i + 1) * number + offset);
            }
            result.add(value);
        }
        return result;
    }

    public static List<String> modelToString(List list) {
        if (null == list || list.size() <= 0) return null;
        List<String> strings = new ArrayList<>();
        for (Object t : list) {
            strings.add(JSONObject.toJSONString(t));
        }
        return strings;
    }

    public static <T> List<List<T>> splitList(List<T> list, int num) throws Exception {
        if (null == list || list.size() == 0) {
            throw new Exception("split zero size");
        }
        int fetchFull = list.size() / num;
        int fetchBeyond = list.size() % num;
        if (fetchBeyond != 0) {
            fetchFull = fetchFull + 1;
        }
        return ListUtil.averageAssign(list, fetchFull);
    }

}
