package com.ruoyi.system.enums;



import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * description: 关联表
 *
 * @author liub
 * @date 2019-08-22 00:06
 */
@AllArgsConstructor
@Getter
public enum MmsAssetRelationEnum {
	
	/**
	 * 领用
	 */
    RECEIVE("LY", "领用"),
    /**
	 * 借用
	 */
    BORROW("JY", "借用"),
    
    /**
	 * 处置
	 */
    DISPOSAL("CZ", "处置"),
    
    /**
   	 * 报废
   	 */
    SCRAP("BF", "报废"),
    /**
   	 * 维修
   	 */
    REPAIR("WX", "维修"),
    /**
   	 * 退库
   	 */
    RETIRE("TK", "退库"),
    
    /**
   	 * 归还
   	 */
    RETURN("GH", "归还"),
    
    /**
   	 * 调拨
   	 */
    TRANSFER("DB", "调拨"),
    
    /**
   	 * 盘点
   	 */
    INVENTORY("PD", "盘点"),
    
    /**
   	 * 调库
   	 */
    EXCHANGE("DK", "调库"),

	OUTGOINT("WC", "外出"),
    
    /**
   	 * 调库
   	 */
    INFO_EXCHANGE("INFO", "资产信息变更")

//    FINANCE_EXCHANGE("CW", "财务信息变更"),
//    
//    MAIN_TENENANCE_EXCHANGE("WB", "维保信息变更"),
//
//    ASSET_INCOME("ZRK", "资产入库"),
//    BUDGET("YS", "预算"),
//    PROCUREMENT("CG", "采购"),
//
//    MAIN_PLAN("MP", "维保计划"),
//
//    FIXED("ZG", "转固")
    ;

    private String type;
    
    private String description;
    
    /**
     * 根据type获取description
     * @param type
     * @return
     */
    public static String getDescByType(String type) {
    	for (MmsAssetRelationEnum item : MmsAssetRelationEnum.values()) {
			if(type.equals(item.getType())){
				return item.getDescription();
			}
		}
    	return null;
    }

	public void setType(String type) {
		this.type = type;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
	
    
}
