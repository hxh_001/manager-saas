package com.ruoyi.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 盘点类型
 *
 * @Author:gbm
 * @Date:2024/7/20
 */
@AllArgsConstructor
@Getter
public enum InventoryType {
    all(1, "全部盘点"),
    part(2, "部分盘点"),
    blind(3, "盲盘");

    private final Integer code;

    private final String desc;
}
