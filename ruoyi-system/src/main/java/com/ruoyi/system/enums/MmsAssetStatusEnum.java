package com.ruoyi.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * description: 资产状态
 *
 * @author liub
 * @date 2019-08-20 23:21
 */
@Getter
@AllArgsConstructor
public enum MmsAssetStatusEnum {

    WAITING_ACCEPTANCE(1, "入库"),
    IN_STOCK(3, "在库"),
    IN_USE(5, "在用"),
    IN_BORROW(7, "在借"),
    IN_REPAIR(9, "在修"),
    SCRAP(18, "已报废"),
    OUTGOING_FINISH(23, "已外出");


    private int status;
    private String description;


    public static String toEnumDesc(int status) {
        for (MmsAssetStatusEnum item : MmsAssetStatusEnum.values()) {
            if (item.getStatus() == (status)) {
                return item.getDescription();
            }
        }
        return "";
    }

    public static int toEnumStatus(String description) {
        for (MmsAssetStatusEnum item : MmsAssetStatusEnum.values()) {
            if (item.getDescription().equals(description)) {
                return item.getStatus();
            }
        }
        return 0;
    }

    public static String toDes(String description) {
        for (MmsAssetStatusEnum item : MmsAssetStatusEnum.values()) {
            if (item.getDescription().equals(description)) {
                return String.valueOf(item.getStatus());
            }
        }
        return description;
    }
}
