package com.ruoyi.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 盘点状态
 *
 * @Author:gbm
 * @Date:2024/7/27
 */
@AllArgsConstructor
@Getter
public enum InventoryStatus {
    undo(1, "未开始"),
    doing(2, "盘点中"),
    done(3, "盘点结束");

    private final Integer code;

    private final String desc;
}
