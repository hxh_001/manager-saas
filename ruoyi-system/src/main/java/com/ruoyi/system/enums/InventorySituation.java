package com.ruoyi.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 盘点情况(未盘、已盘、盘盈、盘亏)
 *
 * @Author:gbm
 * @Date:2024/7/27
 */
@AllArgsConstructor
@Getter
public enum InventorySituation {
    undo(1, "未盘"),
    done(2, "已盘"),
    gain(3, "盘盈"),
    loss(4, "盘亏");

    private final Integer code;

    private final String desc;
}
