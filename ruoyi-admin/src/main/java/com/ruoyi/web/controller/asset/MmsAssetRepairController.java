package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetRepair;
import com.ruoyi.system.service.IMmsAssetRepairService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产维修Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/repair")
public class MmsAssetRepairController extends BaseController
{
    @Autowired
    private IMmsAssetRepairService mmsAssetRepairService;

    /**
     * 查询资产维修列表
     */
    @PreAuthorize("@ss.hasPermi('system:repair:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetRepair mmsAssetRepair)
    {
        startPage();
        List<MmsAssetRepair> list = mmsAssetRepairService.selectMmsAssetRepairList(mmsAssetRepair);
        return getDataTable(list);
    }

    /**
     * 导出资产维修列表
     */
    @PreAuthorize("@ss.hasPermi('system:repair:export')")
    @Log(title = "资产维修", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetRepair mmsAssetRepair)
    {
        List<MmsAssetRepair> list = mmsAssetRepairService.selectMmsAssetRepairList(mmsAssetRepair);
        ExcelUtil<MmsAssetRepair> util = new ExcelUtil<MmsAssetRepair>(MmsAssetRepair.class);
        util.exportExcel(response, list, "资产维修数据");
    }

    /**
     * 获取资产维修详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:repair:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetRepairService.selectMmsAssetRepairById(id));
    }

    /**
     * 新增资产维修
     */
    @PreAuthorize("@ss.hasPermi('system:repair:add')")
    @Log(title = "资产维修", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetRepair mmsAssetRepair)
    {
        return toAjax(mmsAssetRepairService.insertMmsAssetRepair(mmsAssetRepair));
    }

    /**
     * 修改资产维修
     */
    @PreAuthorize("@ss.hasPermi('system:repair:edit')")
    @Log(title = "资产维修", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetRepair mmsAssetRepair)
    {
        return toAjax(mmsAssetRepairService.updateMmsAssetRepair(mmsAssetRepair));
    }

    /**
     * 删除资产维修
     */
    @PreAuthorize("@ss.hasPermi('system:repair:remove')")
    @Log(title = "资产维修", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetRepairService.deleteMmsAssetRepairByIds(ids));
    }
}
