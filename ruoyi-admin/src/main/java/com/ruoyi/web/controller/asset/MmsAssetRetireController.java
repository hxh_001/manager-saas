package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetRetire;
import com.ruoyi.system.service.IMmsAssetRetireService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产退库Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/retire")
public class MmsAssetRetireController extends BaseController
{
    @Autowired
    private IMmsAssetRetireService mmsAssetRetireService;

    /**
     * 查询资产退库列表
     */
    @PreAuthorize("@ss.hasPermi('system:retire:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetRetire mmsAssetRetire)
    {
        startPage();
        List<MmsAssetRetire> list = mmsAssetRetireService.selectMmsAssetRetireList(mmsAssetRetire);
        return getDataTable(list);
    }

    /**
     * 导出资产退库列表
     */
    @PreAuthorize("@ss.hasPermi('system:retire:export')")
    @Log(title = "资产退库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetRetire mmsAssetRetire)
    {
        List<MmsAssetRetire> list = mmsAssetRetireService.selectMmsAssetRetireList(mmsAssetRetire);
        ExcelUtil<MmsAssetRetire> util = new ExcelUtil<MmsAssetRetire>(MmsAssetRetire.class);
        util.exportExcel(response, list, "资产退库数据");
    }

    /**
     * 获取资产退库详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:retire:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetRetireService.selectMmsAssetRetireById(id));
    }

    /**
     * 新增资产退库
     */
    @PreAuthorize("@ss.hasPermi('system:retire:add')")
    @Log(title = "资产退库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetRetire mmsAssetRetire)
    {
        return toAjax(mmsAssetRetireService.insertMmsAssetRetire(mmsAssetRetire));
    }

    /**
     * 修改资产退库
     */
    @PreAuthorize("@ss.hasPermi('system:retire:edit')")
    @Log(title = "资产退库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetRetire mmsAssetRetire)
    {
        return toAjax(mmsAssetRetireService.updateMmsAssetRetire(mmsAssetRetire));
    }

    /**
     * 删除资产退库
     */
    @PreAuthorize("@ss.hasPermi('system:retire:remove')")
    @Log(title = "资产退库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetRetireService.deleteMmsAssetRetireByIds(ids));
    }
}
