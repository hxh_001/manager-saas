package com.ruoyi.web.controller.system;



import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.system.domain.SysMerchant;
import com.ruoyi.system.service.ISysMerchantService;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户Controller
 * 
 * @author ruoyi
 * @date 2024-07-15
 */
@Api("租户")
@RestController
@RequestMapping("/system/merchant")
public class SysMerchantController extends BaseController
{
    @Autowired
    private ISysMerchantService sysMerchantService;

    /**
     * 查询商户列表
     */
     @ApiOperation("查询商户列表")
    @PreAuthorize("@ss.hasPermi('system:merchant:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysMerchant sysMerchant)
    {
        startPage();
        List<SysMerchant> list = sysMerchantService.selectSysMerchantList(sysMerchant);
        return getDataTable(list);
    }
     
     
     /**
      * 查询商户列表
      */
     @ApiOperation("查询商户列表-下拉框-必须有用户列表权限才能调用")
     @PreAuthorize("@ss.hasPermi('system:user:list')")
     @GetMapping("/getSelect")
     public List<SysMerchant> getSelect()
     {
    	 SysUser sysUser =  getLoginUser().getUser();
    	 System.out.println(JSONObject.toJSONString(sysUser));
    	 QueryWrapper<SysMerchant> query = new QueryWrapper<SysMerchant>();
         List<SysMerchant> list = sysMerchantService.list(query);
		 return list;
     }
     
    /**
     * 导出商户列表
     */
     @ApiOperation("导出商户列表")
    @PreAuthorize("@ss.hasPermi('system:merchant:export')")
    @Log(title = "商户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysMerchant sysMerchant)
    {
        List<SysMerchant> list = sysMerchantService.selectSysMerchantList(sysMerchant);
        ExcelUtil<SysMerchant> util = new ExcelUtil<SysMerchant>(SysMerchant.class);
        util.exportExcel(response, list, "商户数据");
    }

    /**
     * 获取商户详细信息
     */
     @ApiOperation("获取商户列表")
    @PreAuthorize("@ss.hasPermi('system:merchant:query')")
    @GetMapping(value = "/{merchantId}")
    public AjaxResult getInfo(@PathVariable("merchantId") Long merchantId)
    {
        return success(sysMerchantService.selectSysMerchantByMerchantId(merchantId));
    }

    /**
     * 新增商户
     */
     @ApiOperation("新增商户列表")
    @PreAuthorize("@ss.hasPermi('system:merchant:add')")
    @Log(title = "商户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysMerchant sysMerchant)
    {
        return toAjax(sysMerchantService.insertSysMerchant(sysMerchant));
    }

    /**
     * 修改商户
     */
     @ApiOperation("修改商户列表")
    @PreAuthorize("@ss.hasPermi('system:merchant:edit')")
    @Log(title = "商户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysMerchant sysMerchant)
    {
        return toAjax(sysMerchantService.updateSysMerchant(sysMerchant));
    }

    /**
     * 删除商户
     */
      @ApiOperation("删除商户列表")
    @PreAuthorize("@ss.hasPermi('system:merchant:remove')")
    @Log(title = "商户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{merchantIds}")
    public AjaxResult remove(@PathVariable Long[] merchantIds)
    {
        return toAjax(sysMerchantService.deleteSysMerchantByMerchantIds(merchantIds));
    }
}
