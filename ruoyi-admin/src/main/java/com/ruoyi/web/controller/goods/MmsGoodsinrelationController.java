package com.ruoyi.web.controller.goods;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsGoodsinrelation;
import com.ruoyi.system.service.IMmsGoodsinrelationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品入库单-关联物品idController
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@RestController
@RequestMapping("/system/goodsinrelation")
public class MmsGoodsinrelationController extends BaseController
{
    @Autowired
    private IMmsGoodsinrelationService mmsGoodsinrelationService;

    /**
     * 查询物品入库单-关联物品id列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsinrelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsGoodsinrelation mmsGoodsinrelation)
    {
        startPage();
        List<MmsGoodsinrelation> list = mmsGoodsinrelationService.selectMmsGoodsinrelationList(mmsGoodsinrelation);
        return getDataTable(list);
    }

    /**
     * 导出物品入库单-关联物品id列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsinrelation:export')")
    @Log(title = "物品入库单-关联物品id", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsGoodsinrelation mmsGoodsinrelation)
    {
        List<MmsGoodsinrelation> list = mmsGoodsinrelationService.selectMmsGoodsinrelationList(mmsGoodsinrelation);
        ExcelUtil<MmsGoodsinrelation> util = new ExcelUtil<MmsGoodsinrelation>(MmsGoodsinrelation.class);
        util.exportExcel(response, list, "物品入库单-关联物品id数据");
    }

    /**
     * 获取物品入库单-关联物品id详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:goodsinrelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsGoodsinrelationService.selectMmsGoodsinrelationById(id));
    }

    /**
     * 新增物品入库单-关联物品id
     */
    @PreAuthorize("@ss.hasPermi('system:goodsinrelation:add')")
    @Log(title = "物品入库单-关联物品id", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsGoodsinrelation mmsGoodsinrelation)
    {
        return toAjax(mmsGoodsinrelationService.insertMmsGoodsinrelation(mmsGoodsinrelation));
    }

    /**
     * 修改物品入库单-关联物品id
     */
    @PreAuthorize("@ss.hasPermi('system:goodsinrelation:edit')")
    @Log(title = "物品入库单-关联物品id", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsGoodsinrelation mmsGoodsinrelation)
    {
        return toAjax(mmsGoodsinrelationService.updateMmsGoodsinrelation(mmsGoodsinrelation));
    }

    /**
     * 删除物品入库单-关联物品id
     */
    @PreAuthorize("@ss.hasPermi('system:goodsinrelation:remove')")
    @Log(title = "物品入库单-关联物品id", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsGoodsinrelationService.deleteMmsGoodsinrelationByIds(ids));
    }
}
