package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetReceive;
import com.ruoyi.system.service.IMmsAssetReceiveService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产领用Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/receive")
public class MmsAssetReceiveController extends BaseController
{
    @Autowired
    private IMmsAssetReceiveService mmsAssetReceiveService;

    /**
     * 查询资产领用列表
     */
    @PreAuthorize("@ss.hasPermi('system:receive:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetReceive mmsAssetReceive)
    {
        startPage();
        List<MmsAssetReceive> list = mmsAssetReceiveService.selectMmsAssetReceiveList(mmsAssetReceive);
        return getDataTable(list);
    }

    /**
     * 导出资产领用列表
     */
    @PreAuthorize("@ss.hasPermi('system:receive:export')")
    @Log(title = "资产领用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetReceive mmsAssetReceive)
    {
        List<MmsAssetReceive> list = mmsAssetReceiveService.selectMmsAssetReceiveList(mmsAssetReceive);
        ExcelUtil<MmsAssetReceive> util = new ExcelUtil<MmsAssetReceive>(MmsAssetReceive.class);
        util.exportExcel(response, list, "资产领用数据");
    }

    /**
     * 获取资产领用详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:receive:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetReceiveService.selectMmsAssetReceiveById(id));
    }

    /**
     * 新增资产领用
     */
    @PreAuthorize("@ss.hasPermi('system:receive:add')")
    @Log(title = "资产领用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetReceive mmsAssetReceive)
    {
        return toAjax(mmsAssetReceiveService.insertMmsAssetReceive(mmsAssetReceive));
    }

    /**
     * 修改资产领用
     */
    @PreAuthorize("@ss.hasPermi('system:receive:edit')")
    @Log(title = "资产领用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetReceive mmsAssetReceive)
    {
        return toAjax(mmsAssetReceiveService.updateMmsAssetReceive(mmsAssetReceive));
    }

    /**
     * 删除资产领用
     */
    @PreAuthorize("@ss.hasPermi('system:receive:remove')")
    @Log(title = "资产领用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetReceiveService.deleteMmsAssetReceiveByIds(ids));
    }
}
