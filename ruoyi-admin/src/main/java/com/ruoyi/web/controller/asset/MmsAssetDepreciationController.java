package com.ruoyi.web.controller.asset;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MmsAssetDepreciation;
import com.ruoyi.system.service.IMmsAssetDepreciationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 资产折旧Controller
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@RestController
@RequestMapping("/system/depreciation")
public class MmsAssetDepreciationController extends BaseController {
    @Autowired
    private IMmsAssetDepreciationService mmsAssetDepreciationService;

    /**
     * 查询资产折旧列表
     */
    @PreAuthorize("@ss.hasPermi('system:depreciation:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetDepreciation mmsAssetDepreciation) {
        startPage();
        List<MmsAssetDepreciation> list = mmsAssetDepreciationService.selectMmsAssetDepreciationList(mmsAssetDepreciation);
        return getDataTable(list);
    }

    /**
     * 导出资产折旧列表
     */
    @PreAuthorize("@ss.hasPermi('system:depreciation:export')")
    @Log(title = "资产折旧", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetDepreciation mmsAssetDepreciation) {
        List<MmsAssetDepreciation> list = mmsAssetDepreciationService.selectMmsAssetDepreciationList(mmsAssetDepreciation);
        ExcelUtil<MmsAssetDepreciation> util = new ExcelUtil<MmsAssetDepreciation>(MmsAssetDepreciation.class);
        util.exportExcel(response, list, "资产折旧数据");
    }

    /**
     * 获取资产折旧详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:depreciation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(mmsAssetDepreciationService.selectMmsAssetDepreciationById(id));
    }

    /**
     * 新增资产折旧
     */
    @PreAuthorize("@ss.hasPermi('system:depreciation:add')")
    @Log(title = "资产折旧", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetDepreciation mmsAssetDepreciation) {
        return toAjax(mmsAssetDepreciationService.insertMmsAssetDepreciation(mmsAssetDepreciation));
    }

    /**
     * 修改资产折旧
     */
    @PreAuthorize("@ss.hasPermi('system:depreciation:edit')")
    @Log(title = "资产折旧", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetDepreciation mmsAssetDepreciation) {
        return toAjax(mmsAssetDepreciationService.updateMmsAssetDepreciation(mmsAssetDepreciation));
    }

    /**
     * 删除资产折旧
     */
    @PreAuthorize("@ss.hasPermi('system:depreciation:remove')")
    @Log(title = "资产折旧", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(mmsAssetDepreciationService.deleteMmsAssetDepreciationByIds(ids));
    }
}
