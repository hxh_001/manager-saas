package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetRelation;
import com.ruoyi.system.service.IMmsAssetRelationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产和功能项关联Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/relation")
public class MmsAssetRelationController extends BaseController
{
    @Autowired
    private IMmsAssetRelationService mmsAssetRelationService;

    /**
     * 查询资产和功能项关联列表
     */
    @PreAuthorize("@ss.hasPermi('system:relation:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetRelation mmsAssetRelation)
    {
        startPage();
        List<MmsAssetRelation> list = mmsAssetRelationService.selectMmsAssetRelationList(mmsAssetRelation);
        return getDataTable(list);
    }

    /**
     * 导出资产和功能项关联列表
     */
    @PreAuthorize("@ss.hasPermi('system:relation:export')")
    @Log(title = "资产和功能项关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetRelation mmsAssetRelation)
    {
        List<MmsAssetRelation> list = mmsAssetRelationService.selectMmsAssetRelationList(mmsAssetRelation);
        ExcelUtil<MmsAssetRelation> util = new ExcelUtil<MmsAssetRelation>(MmsAssetRelation.class);
        util.exportExcel(response, list, "资产和功能项关联数据");
    }

    /**
     * 获取资产和功能项关联详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:relation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetRelationService.selectMmsAssetRelationById(id));
    }

    /**
     * 新增资产和功能项关联
     */
    @PreAuthorize("@ss.hasPermi('system:relation:add')")
    @Log(title = "资产和功能项关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetRelation mmsAssetRelation)
    {
        return toAjax(mmsAssetRelationService.insertMmsAssetRelation(mmsAssetRelation));
    }

    /**
     * 修改资产和功能项关联
     */
    @PreAuthorize("@ss.hasPermi('system:relation:edit')")
    @Log(title = "资产和功能项关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetRelation mmsAssetRelation)
    {
        return toAjax(mmsAssetRelationService.updateMmsAssetRelation(mmsAssetRelation));
    }

    /**
     * 删除资产和功能项关联
     */
    @PreAuthorize("@ss.hasPermi('system:relation:remove')")
    @Log(title = "资产和功能项关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetRelationService.deleteMmsAssetRelationByIds(ids));
    }
}
