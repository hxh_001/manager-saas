package com.ruoyi.web.controller.asset;


import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.config.AssetCommonCache;
import com.ruoyi.system.domain.MmsArea;
import com.ruoyi.system.domain.MmsAsset;
import com.ruoyi.system.domain.vo.MmsAssetExcel;
import com.ruoyi.system.enums.MmsAssetStatusEnum;
import com.ruoyi.system.service.IMmsAssetService;
import com.ruoyi.system.service.impl.MmsAssetNoRecordService;
import com.ruoyi.system.utils.ListUtil;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

import static com.ruoyi.common.utils.SecurityUtils.getUsername;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

/**
 * 资产信息Controller
 *
 * @author ruoyi
 * @date 2024-06-18
 */
@Slf4j
@RestController
@RequestMapping("/system/asset")
public class MmsAssetController extends BaseController {
    @Autowired
    private IMmsAssetService mmsAssetService;
    
    @Autowired
    private AssetCommonCache cache;
    
    @Autowired
    private MmsAssetNoRecordService mmsAssetNoRecordService;

    /**
     * 查询资产信息列表
     */
    //@PreAuthorize("@ss.hasPermi('system:asset:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAsset mmsAsset) {
        startPage();
        List<MmsAsset> list = mmsAssetService.selectMmsAssetList(mmsAsset);
        return getDataTable(list);
    }

    /**
     * 导出资产信息列表
     */
    //@PreAuthorize("@ss.hasPermi('system:asset:export')")
    @Log(title = "资产信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAsset mmsAsset) {
        List<MmsAsset> list = mmsAssetService.selectMmsAssetList(mmsAsset);
        List<MmsAssetExcel> exportList = new ArrayList<MmsAssetExcel>();
        
        
        //资产状态
        List<SysDictData>  assetStatus = DictUtils.getDictCache("asset_status");
        Map<String ,String> statusMap = new HashMap<>();
        for (SysDictData sysDictData : assetStatus) {
        	statusMap.put(sysDictData.getDictValue(), sysDictData.getDictLabel());
		}
        
        //资产性质
        List<SysDictData>  assetProp = DictUtils.getDictCache("asset_prop");
        Map<String ,String> propMap = new HashMap<>();
        for (SysDictData sysDictData : assetProp) {
        	propMap.put(sysDictData.getDictValue(), sysDictData.getDictLabel());
		}
        
        //资产性质
        List<SysDictData>  assetGet = DictUtils.getDictCache("asset_get");
        Map<String ,String> getMap = new HashMap<>();
        for (SysDictData sysDictData : assetGet) {
        	getMap.put(sysDictData.getDictValue(), sysDictData.getDictLabel());
		}
        
        for (MmsAsset item : list) {
        	MmsAssetExcel export = new MmsAssetExcel();
        	BeanUtils.copyProperties(item, export);
        	export.setAssetNature(propMap.get(item.getAssetNature()));//资产性质
        	export.setGainWay(getMap.get(item.getGainWay()));//资产取得方式
        	export.setStatus(statusMap.get(item.getStatus()+""));
        	
        	exportList.add(export);
		}
        ExcelUtil<MmsAssetExcel> util = new ExcelUtil<MmsAssetExcel>(MmsAssetExcel.class);
        util.exportExcel(response, exportList, "资产信息数据");
    }

    @Log(title = "资产信息", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:asset:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MmsAssetExcel> util = new ExcelUtil<MmsAssetExcel>(MmsAssetExcel.class);
        List<MmsAssetExcel> assetList = util.importExcel(file.getInputStream());
        if (assetList.isEmpty()) {
        	return success("导入数据为空");
		}
        List<MmsAsset> lists =  new ArrayList<MmsAsset>();
        Map<String,String> propMap = cache.getAssetProp();
        Map<String, SysDept> deptMap = cache.getUserDept();
        Map<String, MmsArea> areaMap = cache.getArea();
        Map<String,String> getMap  = cache.getAssetGet(); 
        Map<String,String> storageMap  = cache.getAssetStorage(); 
        Map<String,SysUser> userMap  = cache.getUser(); 
        
        JSONObject json = mmsAssetNoRecordService.getImportAssetNo(assetList.size());
        String ruleStart= json.getString("start");
        int size =  json.getIntValue("size");
        for (int i = 0; i < assetList.size(); i++) {
        	
        	MmsAssetExcel assetExcel = assetList.get(i);
        	MmsAsset model = new MmsAsset();
        	BeanUtils.copyProperties(assetExcel, model);
        	if(StringUtils.isBlank(assetExcel.getAssetNo())){//导入资产编码为空
        		size ++;
            	String assetNo = ruleStart + size;
            	model.setAssetNo(assetNo);
        	}
        	model.setStatus(MmsAssetStatusEnum.IN_STOCK.getStatus());
        	model.setCreateBy(getUsername());
        	model.setCreateTime(DateUtils.getNowDate());
        	if(StringUtils.isNotBlank(assetExcel.getAssetNature())) {//资产性质
        		model.setAssetNature(propMap.get(assetExcel.getAssetNature()));
        	}
        	if(StringUtils.isNotBlank(assetExcel.getGainWay())) {//取得方式
        		model.setGainWay(getMap.get(assetExcel.getGainWay()));
        	}
        	if(StringUtils.isNotBlank(assetExcel.getStorageLocationName())) {//存放地点
        		model.setStorageLocationName(storageMap.get(assetExcel.getStorageLocationName()));
        	}
        	if(StringUtils.isNotBlank(assetExcel.getUseDeptName())) {
        		SysDept dept = deptMap.get(assetExcel.getUseDeptName());
            	if(Objects.nonNull(dept)) {
            		model.setUseDeptId(dept.getDeptId());
            	}
            	dept = null;//释放对象
        	}
        	if(StringUtils.isNotBlank(assetExcel.getAreaName())) {
        		MmsArea area = areaMap.get(assetExcel.getAreaName());
            	if(Objects.nonNull(area)) {
            		model.setAreaId(area.getId());
            	}
            	area = null;//释放对象
        	}
        	
        	if(StringUtils.isNotBlank(assetExcel.getUserName())) {
        		SysUser user = userMap.get(assetExcel.getUserName());
            	if(Objects.nonNull(user)) {
            		model.setUserId(user.getUserId());
            	}
            	user = null;//释放对象
        	}
        	
        	lists.add(model);
        }
        log.warn("thread:"+lists.size());
        List<List<MmsAsset>> insertList = splitList(lists, 200);
        CountDownLatch countDownLatch = new CountDownLatch(insertList.size());
        for (List<MmsAsset> listSub:insertList) {
        	 mmsAssetService.importAsset(listSub, countDownLatch);
        }
        try {
            countDownLatch.await(); //保证之前的所有的线程都执行完成，才会走下面的；
            // 这样就可以在下面拿到所有线程执行完的集合结果
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success("success");
    }
    
    //测试每100条数据插入开一个线程
    public <T> List<List<T>> splitList(List<T> list, int num) throws Exception {
        if (null == list || list.size() == 0) {
            throw new Exception("split zero size");
        }
        int fetchFull = list.size() / num;
        int fetchBeyond = list.size() % num;
        if (fetchBeyond != 0) {
            fetchFull = fetchFull + 1;
        }
        return ListUtil.averageAssign(list, fetchFull);
    }
    
    
   
   
    
    
    
    
    

    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<MmsAsset> util = new ExcelUtil<MmsAsset>(MmsAsset.class);
        util.importTemplateExcel(response, "资产数据");
    }

    /**
     * 获取资产信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:asset:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(mmsAssetService.selectMmsAssetById(id));
    }

    /**
     * 新增资产信息
     */
    @PreAuthorize("@ss.hasPermi('system:asset:add')")
    @Log(title = "资产信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAsset mmsAsset) {
        if (null == mmsAsset.getPurchaseDate()) {
            return error("购置日期为空");
        }
        if (null == mmsAsset.getUsefulLife()) {
            return error("使用月限为空");
        }
        if (null == mmsAsset.getAssetOriginalValue()) {
            return error("资产原值为空");
        }
        if (null == mmsAsset.getTax()) {
            return error("税额为空");
        }
        return toAjax(mmsAssetService.insertMmsAsset(mmsAsset));
    }

    /**
     * 修改资产信息
     */
    @PreAuthorize("@ss.hasPermi('system:asset:edit')")
    @Log(title = "资产信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAsset mmsAsset) {
        return toAjax(mmsAssetService.updateMmsAsset(mmsAsset));
    }

    /**
     * 删除资产信息
     */
    @PreAuthorize("@ss.hasPermi('system:asset:remove')")
    @Log(title = "资产信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(mmsAssetService.deleteMmsAssetByIds(ids));
    }
}
