package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsInventoryOrder;
import com.ruoyi.system.service.IMmsInventoryOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 盘点管理Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/order")
public class MmsInventoryOrderController extends BaseController
{
    @Autowired
    private IMmsInventoryOrderService mmsInventoryOrderService;

    /**
     * 查询盘点管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsInventoryOrder mmsInventoryOrder)
    {
        startPage();
        List<MmsInventoryOrder> list = mmsInventoryOrderService.selectMmsInventoryOrderList(mmsInventoryOrder);
        return getDataTable(list);
    }

    /**
     * 导出盘点管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:order:export')")
    @Log(title = "盘点管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsInventoryOrder mmsInventoryOrder)
    {
        List<MmsInventoryOrder> list = mmsInventoryOrderService.selectMmsInventoryOrderList(mmsInventoryOrder);
        ExcelUtil<MmsInventoryOrder> util = new ExcelUtil<MmsInventoryOrder>(MmsInventoryOrder.class);
        util.exportExcel(response, list, "盘点管理数据");
    }

    /**
     * 获取盘点管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsInventoryOrderService.selectMmsInventoryOrderById(id));
    }

    /**
     * 新增盘点管理
     */
    @PreAuthorize("@ss.hasPermi('system:order:add')")
    @Log(title = "盘点管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsInventoryOrder mmsInventoryOrder)
    {
        return toAjax(mmsInventoryOrderService.insertMmsInventoryOrder(mmsInventoryOrder));
    }

    /**
     * 修改盘点管理
     */
    @PreAuthorize("@ss.hasPermi('system:order:edit')")
    @Log(title = "盘点管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsInventoryOrder mmsInventoryOrder)
    {
        return toAjax(mmsInventoryOrderService.updateMmsInventoryOrder(mmsInventoryOrder));
    }

    /**
     * 删除盘点管理
     */
    @PreAuthorize("@ss.hasPermi('system:order:remove')")
    @Log(title = "盘点管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsInventoryOrderService.deleteMmsInventoryOrderByIds(ids));
    }
}
