package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetScrap;
import com.ruoyi.system.service.IMmsAssetScrapService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产报废Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/scrap")
public class MmsAssetScrapController extends BaseController
{
    @Autowired
    private IMmsAssetScrapService mmsAssetScrapService;

    /**
     * 查询资产报废列表
     */
    @PreAuthorize("@ss.hasPermi('system:scrap:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetScrap mmsAssetScrap)
    {
        startPage();
        List<MmsAssetScrap> list = mmsAssetScrapService.selectMmsAssetScrapList(mmsAssetScrap);
        return getDataTable(list);
    }

    /**
     * 导出资产报废列表
     */
    @PreAuthorize("@ss.hasPermi('system:scrap:export')")
    @Log(title = "资产报废", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetScrap mmsAssetScrap)
    {
        List<MmsAssetScrap> list = mmsAssetScrapService.selectMmsAssetScrapList(mmsAssetScrap);
        ExcelUtil<MmsAssetScrap> util = new ExcelUtil<MmsAssetScrap>(MmsAssetScrap.class);
        util.exportExcel(response, list, "资产报废数据");
    }

    /**
     * 获取资产报废详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:scrap:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetScrapService.selectMmsAssetScrapById(id));
    }

    /**
     * 新增资产报废
     */
    @PreAuthorize("@ss.hasPermi('system:scrap:add')")
    @Log(title = "资产报废", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetScrap mmsAssetScrap)
    {
        return toAjax(mmsAssetScrapService.insertMmsAssetScrap(mmsAssetScrap));
    }

    /**
     * 修改资产报废
     */
    @PreAuthorize("@ss.hasPermi('system:scrap:edit')")
    @Log(title = "资产报废", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetScrap mmsAssetScrap)
    {
        return toAjax(mmsAssetScrapService.updateMmsAssetScrap(mmsAssetScrap));
    }

    /**
     * 删除资产报废
     */
    @PreAuthorize("@ss.hasPermi('system:scrap:remove')")
    @Log(title = "资产报废", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetScrapService.deleteMmsAssetScrapByIds(ids));
    }
}
