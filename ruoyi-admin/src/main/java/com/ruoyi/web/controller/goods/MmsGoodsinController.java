package com.ruoyi.web.controller.goods;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsGoodsin;
import com.ruoyi.system.service.IMmsGoodsinService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品入库单Controller
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@RestController
@RequestMapping("/system/goodsin")
public class MmsGoodsinController extends BaseController
{
    @Autowired
    private IMmsGoodsinService mmsGoodsinService;

    /**
     * 查询物品入库单列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsin:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsGoodsin mmsGoodsin)
    {
        startPage();
        List<MmsGoodsin> list = mmsGoodsinService.selectMmsGoodsinList(mmsGoodsin);
        return getDataTable(list);
    }

    /**
     * 导出物品入库单列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsin:export')")
    @Log(title = "物品入库单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsGoodsin mmsGoodsin)
    {
        List<MmsGoodsin> list = mmsGoodsinService.selectMmsGoodsinList(mmsGoodsin);
        ExcelUtil<MmsGoodsin> util = new ExcelUtil<MmsGoodsin>(MmsGoodsin.class);
        util.exportExcel(response, list, "物品入库单数据");
    }

    /**
     * 获取物品入库单详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:goodsin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsGoodsinService.selectMmsGoodsinById(id));
    }

    /**
     * 新增物品入库单
     */
    @PreAuthorize("@ss.hasPermi('system:goodsin:add')")
    @Log(title = "物品入库单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsGoodsin mmsGoodsin)
    {
        return toAjax(mmsGoodsinService.insertMmsGoodsin(mmsGoodsin));
    }

    /**
     * 修改物品入库单
     */
    @PreAuthorize("@ss.hasPermi('system:goodsin:edit')")
    @Log(title = "物品入库单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsGoodsin mmsGoodsin)
    {
        return toAjax(mmsGoodsinService.updateMmsGoodsin(mmsGoodsin));
    }

    /**
     * 删除物品入库单
     */
    @PreAuthorize("@ss.hasPermi('system:goodsin:remove')")
    @Log(title = "物品入库单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsGoodsinService.deleteMmsGoodsinByIds(ids));
    }
}
