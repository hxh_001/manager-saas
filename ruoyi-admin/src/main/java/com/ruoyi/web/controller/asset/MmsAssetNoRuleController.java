package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetNoRule;
import com.ruoyi.system.service.IMmsAssetNoRuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 编码规则Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/rule")
public class MmsAssetNoRuleController extends BaseController
{
    @Autowired
    private IMmsAssetNoRuleService mmsAssetNoRuleService;

    /**
     * 查询编码规则列表
     */
    @PreAuthorize("@ss.hasPermi('system:rule:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetNoRule mmsAssetNoRule)
    {
        startPage();
        List<MmsAssetNoRule> list = mmsAssetNoRuleService.selectMmsAssetNoRuleList(mmsAssetNoRule);
        return getDataTable(list);
    }

    /**
     * 导出编码规则列表
     */
    @PreAuthorize("@ss.hasPermi('system:rule:export')")
    @Log(title = "编码规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetNoRule mmsAssetNoRule)
    {
        List<MmsAssetNoRule> list = mmsAssetNoRuleService.selectMmsAssetNoRuleList(mmsAssetNoRule);
        ExcelUtil<MmsAssetNoRule> util = new ExcelUtil<MmsAssetNoRule>(MmsAssetNoRule.class);
        util.exportExcel(response, list, "编码规则数据");
    }

    /**
     * 获取编码规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:rule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetNoRuleService.selectMmsAssetNoRuleById(id));
    }

    /**
     * 新增编码规则
     */
    @PreAuthorize("@ss.hasPermi('system:rule:add')")
    @Log(title = "编码规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetNoRule mmsAssetNoRule)
    {
        return toAjax(mmsAssetNoRuleService.insertMmsAssetNoRule(mmsAssetNoRule));
    }

    /**
     * 修改编码规则
     */
    @PreAuthorize("@ss.hasPermi('system:rule:edit')")
    @Log(title = "编码规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetNoRule mmsAssetNoRule)
    {
        return toAjax(mmsAssetNoRuleService.updateMmsAssetNoRule(mmsAssetNoRule));
    }

    /**
     * 删除编码规则
     */
    @PreAuthorize("@ss.hasPermi('system:rule:remove')")
    @Log(title = "编码规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetNoRuleService.deleteMmsAssetNoRuleByIds(ids));
    }
}
