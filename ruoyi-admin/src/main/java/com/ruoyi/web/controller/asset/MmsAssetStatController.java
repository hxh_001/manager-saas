package com.ruoyi.web.controller.asset;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.vo.MmsAssetStat;
import com.ruoyi.system.domain.vo.MmsStatClassify;
import com.ruoyi.system.domain.vo.MmsStatMonthAsset;
import com.ruoyi.system.service.IMmsAssetClassifyService;
import com.ruoyi.system.service.IMmsAssetService;
import com.ruoyi.system.service.impl.MmsAssetInventoryServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 首页统计Controller
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@RestController
@RequestMapping("/system/stat")
@Api(tags = "首页统计")
public class MmsAssetStatController extends BaseController {
    @Autowired
    private IMmsAssetService mmsAssetService;

    @Autowired
    private IMmsAssetClassifyService classifyService;

    @Autowired
    private MmsAssetInventoryServiceImpl service;

   // @PreAuthorize("@ss.hasPermi('system:stat:asset')")
    @GetMapping("/asset")
    @ApiOperation("固定资产统计")
    public AjaxResult statAsset() {
        MmsAssetStat ret = mmsAssetService.statAsset();
        return success(ret);
    }

    //@PreAuthorize("@ss.hasPermi('system:stat:classify')")
    @GetMapping("/classify")
    @ApiOperation("资产分类统计")
    public TableDataInfo classify() {
        List<MmsStatClassify> list = classifyService.statClassify();
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('system:stat:month')")
    @GetMapping("/month")
    @ApiOperation("月资产数量统计")
    public AjaxResult month() {
        MmsStatMonthAsset ret = mmsAssetService.statMonth();
        return success(ret);
    }

    @ApiOperation("执行盘点（只盘点今天的数据）")
    @GetMapping(value = "/job")
    public AjaxResult job() {
        service.scheduleInventory();
        return success(true);
    }
}
