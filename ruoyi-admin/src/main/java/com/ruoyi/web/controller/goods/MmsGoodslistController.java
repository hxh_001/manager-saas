package com.ruoyi.web.controller.goods;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsGoodslist;
import com.ruoyi.system.service.IMmsGoodslistService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品名目Controller
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@RestController
@RequestMapping("/system/goodslist")
public class MmsGoodslistController extends BaseController
{
    @Autowired
    private IMmsGoodslistService mmsGoodslistService;

    /**
     * 查询物品名目列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodslist:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsGoodslist mmsGoodslist)
    {
        startPage();
        List<MmsGoodslist> list = mmsGoodslistService.selectMmsGoodslistList(mmsGoodslist);
        return getDataTable(list);
    }

    /**
     * 导出物品名目列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodslist:export')")
    @Log(title = "物品名目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsGoodslist mmsGoodslist)
    {
        List<MmsGoodslist> list = mmsGoodslistService.selectMmsGoodslistList(mmsGoodslist);
        ExcelUtil<MmsGoodslist> util = new ExcelUtil<MmsGoodslist>(MmsGoodslist.class);
        util.exportExcel(response, list, "物品名目数据");
    }

    /**
     * 获取物品名目详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:goodslist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsGoodslistService.selectMmsGoodslistById(id));
    }

    /**
     * 新增物品名目
     */
    @PreAuthorize("@ss.hasPermi('system:goodslist:add')")
    @Log(title = "物品名目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsGoodslist mmsGoodslist)
    {
        return toAjax(mmsGoodslistService.insertMmsGoodslist(mmsGoodslist));
    }

    /**
     * 修改物品名目
     */
    @PreAuthorize("@ss.hasPermi('system:goodslist:edit')")
    @Log(title = "物品名目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsGoodslist mmsGoodslist)
    {
        return toAjax(mmsGoodslistService.updateMmsGoodslist(mmsGoodslist));
    }

    /**
     * 删除物品名目
     */
    @PreAuthorize("@ss.hasPermi('system:goodslist:remove')")
    @Log(title = "物品名目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsGoodslistService.deleteMmsGoodslistByIds(ids));
    }
}
