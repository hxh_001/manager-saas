package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetAssetout;
import com.ruoyi.system.service.IMmsAssetAssetoutService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 外出管理Controller
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@RestController
@RequestMapping("/system/assetout")
public class MmsAssetAssetoutController extends BaseController
{
    @Autowired
    private IMmsAssetAssetoutService mmsAssetAssetoutService;

    /**
     * 查询外出管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:assetout:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetAssetout mmsAssetAssetout)
    {
        startPage();
        List<MmsAssetAssetout> list = mmsAssetAssetoutService.selectMmsAssetAssetoutList(mmsAssetAssetout);
        return getDataTable(list);
    }

    /**
     * 导出外出管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:assetout:export')")
    @Log(title = "外出管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetAssetout mmsAssetAssetout)
    {
        List<MmsAssetAssetout> list = mmsAssetAssetoutService.selectMmsAssetAssetoutList(mmsAssetAssetout);
        ExcelUtil<MmsAssetAssetout> util = new ExcelUtil<MmsAssetAssetout>(MmsAssetAssetout.class);
        util.exportExcel(response, list, "外出管理数据");
    }

    /**
     * 获取外出管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:assetout:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetAssetoutService.selectMmsAssetAssetoutById(id));
    }

    /**
     * 新增外出管理
     */
    @PreAuthorize("@ss.hasPermi('system:assetout:add')")
    @Log(title = "外出管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetAssetout mmsAssetAssetout)
    {
        return toAjax(mmsAssetAssetoutService.insertMmsAssetAssetout(mmsAssetAssetout));
    }

    /**
     * 修改外出管理
     */
    @PreAuthorize("@ss.hasPermi('system:assetout:edit')")
    @Log(title = "外出管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetAssetout mmsAssetAssetout)
    {
        return toAjax(mmsAssetAssetoutService.updateMmsAssetAssetout(mmsAssetAssetout));
    }

    /**
     * 删除外出管理
     */
    @PreAuthorize("@ss.hasPermi('system:assetout:remove')")
    @Log(title = "外出管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetAssetoutService.deleteMmsAssetAssetoutByIds(ids));
    }
}
