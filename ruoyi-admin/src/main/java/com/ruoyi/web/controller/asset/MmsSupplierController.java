package com.ruoyi.web.controller.asset;


import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.system.domain.MmsSupplier;
import com.ruoyi.system.service.IMmsSupplierService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 供应商管理Controller
 * 
 * @author ruoyi
 * @date 2024-08-30
 */
@Api("供应商管理")
@RestController
@RequestMapping("/system/supplier")
public class MmsSupplierController extends BaseController
{
    @Autowired
    private IMmsSupplierService mmsSupplierService;

    /**
     * 查询供应商管理列表
     */
     @ApiOperation("查询供应商管理列表")
    @PreAuthorize("@ss.hasPermi('system:supplier:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsSupplier mmsSupplier)
    {
        startPage();
        List<MmsSupplier> list = mmsSupplierService.selectMmsSupplierList(mmsSupplier);
        return getDataTable(list);
    }

    /**
     * 导出供应商管理列表
     */
     @ApiOperation("导出供应商管理列表")
    @PreAuthorize("@ss.hasPermi('system:supplier:export')")
    @Log(title = "供应商管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsSupplier mmsSupplier)
    {
        List<MmsSupplier> list = mmsSupplierService.selectMmsSupplierList(mmsSupplier);
        ExcelUtil<MmsSupplier> util = new ExcelUtil<MmsSupplier>(MmsSupplier.class);
        util.exportExcel(response, list, "供应商管理数据");
    }

    /**
     * 获取供应商管理详细信息
     */
     @ApiOperation("获取供应商管理列表")
    @PreAuthorize("@ss.hasPermi('system:supplier:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsSupplierService.selectMmsSupplierById(id));
    }

    /**
     * 新增供应商管理
     */
     @ApiOperation("新增供应商管理列表")
    @PreAuthorize("@ss.hasPermi('system:supplier:add')")
    @Log(title = "供应商管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsSupplier mmsSupplier)
    {
        return toAjax(mmsSupplierService.insertMmsSupplier(mmsSupplier));
    }

    /**
     * 修改供应商管理
     */
     @ApiOperation("修改供应商管理列表")
    @PreAuthorize("@ss.hasPermi('system:supplier:edit')")
    @Log(title = "供应商管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsSupplier mmsSupplier)
    {
        return toAjax(mmsSupplierService.updateMmsSupplier(mmsSupplier));
    }

    /**
     * 删除供应商管理
     */
      @ApiOperation("删除供应商管理列表")
    @PreAuthorize("@ss.hasPermi('system:supplier:remove')")
    @Log(title = "供应商管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsSupplierService.deleteMmsSupplierByIds(ids));
    }
}
