package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetAssetin;
import com.ruoyi.system.service.IMmsAssetAssetinService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产入库单Controller
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@RestController
@RequestMapping("/system/assetin")
public class MmsAssetAssetinController extends BaseController
{
    @Autowired
    private IMmsAssetAssetinService mmsAssetAssetinService;

    /**
     * 查询资产入库单列表
     */
    @PreAuthorize("@ss.hasPermi('system:assetin:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetAssetin mmsAssetAssetin)
    {
        startPage();
        List<MmsAssetAssetin> list = mmsAssetAssetinService.selectMmsAssetAssetinList(mmsAssetAssetin);
        return getDataTable(list);
    }

    /**
     * 导出资产入库单列表
     */
    @PreAuthorize("@ss.hasPermi('system:assetin:export')")
    @Log(title = "资产入库单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetAssetin mmsAssetAssetin)
    {
        List<MmsAssetAssetin> list = mmsAssetAssetinService.selectMmsAssetAssetinList(mmsAssetAssetin);
        ExcelUtil<MmsAssetAssetin> util = new ExcelUtil<MmsAssetAssetin>(MmsAssetAssetin.class);
        util.exportExcel(response, list, "资产入库单数据");
    }

    /**
     * 获取资产入库单详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:assetin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetAssetinService.selectMmsAssetAssetinById(id));
    }

    /**
     * 新增资产入库单
     */
    @PreAuthorize("@ss.hasPermi('system:assetin:add')")
    @Log(title = "资产入库单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetAssetin mmsAssetAssetin)
    {
        return toAjax(mmsAssetAssetinService.insertMmsAssetAssetin(mmsAssetAssetin));
    }

    /**
     * 修改资产入库单
     */
    @PreAuthorize("@ss.hasPermi('system:assetin:edit')")
    @Log(title = "资产入库单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetAssetin mmsAssetAssetin)
    {
        return toAjax(mmsAssetAssetinService.updateMmsAssetAssetin(mmsAssetAssetin));
    }

    /**
     * 删除资产入库单
     */
    @PreAuthorize("@ss.hasPermi('system:assetin:remove')")
    @Log(title = "资产入库单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetAssetinService.deleteMmsAssetAssetinByIds(ids));
    }
}
