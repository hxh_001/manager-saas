package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetBorrow;
import com.ruoyi.system.service.IMmsAssetBorrowService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产借用Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/borrow")
public class MmsAssetBorrowController extends BaseController
{
    @Autowired
    private IMmsAssetBorrowService mmsAssetBorrowService;

    /**
     * 查询资产借用列表
     */
    @PreAuthorize("@ss.hasPermi('system:borrow:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetBorrow mmsAssetBorrow)
    {
        startPage();
        List<MmsAssetBorrow> list = mmsAssetBorrowService.selectMmsAssetBorrowList(mmsAssetBorrow);
        return getDataTable(list);
    }

    /**
     * 导出资产借用列表
     */
    @PreAuthorize("@ss.hasPermi('system:borrow:export')")
    @Log(title = "资产借用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetBorrow mmsAssetBorrow)
    {
        List<MmsAssetBorrow> list = mmsAssetBorrowService.selectMmsAssetBorrowList(mmsAssetBorrow);
        ExcelUtil<MmsAssetBorrow> util = new ExcelUtil<MmsAssetBorrow>(MmsAssetBorrow.class);
        util.exportExcel(response, list, "资产借用数据");
    }

    /**
     * 获取资产借用详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:borrow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetBorrowService.selectMmsAssetBorrowById(id));
    }

    /**
     * 新增资产借用
     */
    @PreAuthorize("@ss.hasPermi('system:borrow:add')")
    @Log(title = "资产借用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetBorrow mmsAssetBorrow)
    {
        return toAjax(mmsAssetBorrowService.insertMmsAssetBorrow(mmsAssetBorrow));
    }

    /**
     * 修改资产借用
     */
    @PreAuthorize("@ss.hasPermi('system:borrow:edit')")
    @Log(title = "资产借用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetBorrow mmsAssetBorrow)
    {
        return toAjax(mmsAssetBorrowService.updateMmsAssetBorrow(mmsAssetBorrow));
    }

    /**
     * 删除资产借用
     */
    @PreAuthorize("@ss.hasPermi('system:borrow:remove')")
    @Log(title = "资产借用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetBorrowService.deleteMmsAssetBorrowByIds(ids));
    }
}
