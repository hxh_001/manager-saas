package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetClassify;
import com.ruoyi.system.service.IMmsAssetClassifyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产分类Controller
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@RestController
@RequestMapping("/system/classify")
public class MmsAssetClassifyController extends BaseController
{
    @Autowired
    private IMmsAssetClassifyService mmsAssetClassifyService;

    /**
     * 查询资产分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:classify:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetClassify mmsAssetClassify)
    {
        startPage();
        List<MmsAssetClassify> list = mmsAssetClassifyService.selectMmsAssetClassifyList(mmsAssetClassify);
        return getDataTable(list);
    }

    /**
     * 导出资产分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:classify:export')")
    @Log(title = "资产分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetClassify mmsAssetClassify)
    {
        List<MmsAssetClassify> list = mmsAssetClassifyService.selectMmsAssetClassifyList(mmsAssetClassify);
        ExcelUtil<MmsAssetClassify> util = new ExcelUtil<MmsAssetClassify>(MmsAssetClassify.class);
        util.exportExcel(response, list, "资产分类数据");
    }

    /**
     * 获取资产分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:classify:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetClassifyService.selectMmsAssetClassifyById(id));
    }

    /**
     * 新增资产分类
     */
    @PreAuthorize("@ss.hasPermi('system:classify:add')")
    @Log(title = "资产分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetClassify mmsAssetClassify)
    {
        return toAjax(mmsAssetClassifyService.insertMmsAssetClassify(mmsAssetClassify));
    }

    /**
     * 修改资产分类
     */
    @PreAuthorize("@ss.hasPermi('system:classify:edit')")
    @Log(title = "资产分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetClassify mmsAssetClassify)
    {
        return toAjax(mmsAssetClassifyService.updateMmsAssetClassify(mmsAssetClassify));
    }

    /**
     * 删除资产分类
     */
    @PreAuthorize("@ss.hasPermi('system:classify:remove')")
    @Log(title = "资产分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetClassifyService.deleteMmsAssetClassifyByIds(ids));
    }
}
