package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetInfoChange;
import com.ruoyi.system.service.IMmsAssetInfoChangeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产信息变更Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/change")
public class MmsAssetInfoChangeController extends BaseController
{
    @Autowired
    private IMmsAssetInfoChangeService mmsAssetInfoChangeService;

    /**
     * 查询资产信息变更列表
     */
    @PreAuthorize("@ss.hasPermi('system:change:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetInfoChange mmsAssetInfoChange)
    {
        startPage();
        List<MmsAssetInfoChange> list = mmsAssetInfoChangeService.selectMmsAssetInfoChangeList(mmsAssetInfoChange);
        return getDataTable(list);
    }

    /**
     * 导出资产信息变更列表
     */
    @PreAuthorize("@ss.hasPermi('system:change:export')")
    @Log(title = "资产信息变更", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetInfoChange mmsAssetInfoChange)
    {
        List<MmsAssetInfoChange> list = mmsAssetInfoChangeService.selectMmsAssetInfoChangeList(mmsAssetInfoChange);
        ExcelUtil<MmsAssetInfoChange> util = new ExcelUtil<MmsAssetInfoChange>(MmsAssetInfoChange.class);
        util.exportExcel(response, list, "资产信息变更数据");
    }

    /**
     * 获取资产信息变更详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:change:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetInfoChangeService.selectMmsAssetInfoChangeById(id));
    }

    /**
     * 新增资产信息变更
     */
    @PreAuthorize("@ss.hasPermi('system:change:add')")
    @Log(title = "资产信息变更", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetInfoChange mmsAssetInfoChange)
    {
        return toAjax(mmsAssetInfoChangeService.insertMmsAssetInfoChange(mmsAssetInfoChange));
    }

    /**
     * 修改资产信息变更
     */
    @PreAuthorize("@ss.hasPermi('system:change:edit')")
    @Log(title = "资产信息变更", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetInfoChange mmsAssetInfoChange)
    {
        return toAjax(mmsAssetInfoChangeService.updateMmsAssetInfoChange(mmsAssetInfoChange));
    }

    /**
     * 删除资产信息变更
     */
    @PreAuthorize("@ss.hasPermi('system:change:remove')")
    @Log(title = "资产信息变更", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetInfoChangeService.deleteMmsAssetInfoChangeByIds(ids));
    }
}
