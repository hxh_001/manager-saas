package com.ruoyi.web.controller.asset;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MmsAssetInventory;
import com.ruoyi.system.service.IMmsAssetInventoryService;
import com.ruoyi.system.service.impl.MmsAssetInventoryServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 资产盘点Controller
 *
 * @author ruoyi
 * @date 2024-06-27
 */
@RestController
@RequestMapping("/system/inventory")
public class MmsAssetInventoryController extends BaseController {
    @Autowired
    private IMmsAssetInventoryService mmsAssetInventoryService;

    /**
     * 查询资产盘点列表
     */
    @PreAuthorize("@ss.hasPermi('system:inventory:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetInventory mmsAssetInventory) {
        startPage();
        List<MmsAssetInventory> list = mmsAssetInventoryService.selectMmsAssetInventoryList(mmsAssetInventory);
        return getDataTable(list);
    }

    /**
     * 导出资产盘点列表
     */
    @PreAuthorize("@ss.hasPermi('system:inventory:export')")
    @Log(title = "资产盘点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetInventory mmsAssetInventory) {
        List<MmsAssetInventory> list = mmsAssetInventoryService.selectMmsAssetInventoryList(mmsAssetInventory);
        ExcelUtil<MmsAssetInventory> util = new ExcelUtil<MmsAssetInventory>(MmsAssetInventory.class);
        util.exportExcel(response, list, "资产盘点数据");
    }

    /**
     * 获取资产盘点详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:inventory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(mmsAssetInventoryService.selectMmsAssetInventoryById(id));
    }

    /**
     * 新增资产盘点
     */
    @PreAuthorize("@ss.hasPermi('system:inventory:add')")
    @Log(title = "资产盘点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetInventory mmsAssetInventory) {
        return toAjax(mmsAssetInventoryService.insertMmsAssetInventory(mmsAssetInventory));
    }

    /**
     * 修改资产盘点
     */
    @PreAuthorize("@ss.hasPermi('system:inventory:edit')")
    @Log(title = "资产盘点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetInventory mmsAssetInventory) {
        return toAjax(mmsAssetInventoryService.updateMmsAssetInventory(mmsAssetInventory));
    }

    /**
     * 删除资产盘点
     */
    @PreAuthorize("@ss.hasPermi('system:inventory:remove')")
    @Log(title = "资产盘点", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(mmsAssetInventoryService.deleteMmsAssetInventoryByIds(ids));
    }
}
