package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.system.domain.MmsArea;
import com.ruoyi.system.service.IMmsAreaService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 区域Controller
 * 
 * @author ruoyi
 * @date 2024-08-26
 */
@Api("区域")
@RestController
@RequestMapping("/system/area")
public class MmsAreaController extends BaseController
{
    @Autowired
    private IMmsAreaService mmsAreaService;

    /**
     * 查询区域列表
     */
     @ApiOperation("查询区域列表")
    @PreAuthorize("@ss.hasPermi('system:area:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsArea mmsArea)
    {
        startPage();
        List<MmsArea> list = mmsAreaService.selectMmsAreaList(mmsArea);
        return getDataTable(list);
    }

    /**
     * 导出区域列表
     */
     @ApiOperation("导出区域列表")
    @PreAuthorize("@ss.hasPermi('system:area:export')")
    @Log(title = "区域", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsArea mmsArea)
    {
        List<MmsArea> list = mmsAreaService.selectMmsAreaList(mmsArea);
        ExcelUtil<MmsArea> util = new ExcelUtil<MmsArea>(MmsArea.class);
        util.exportExcel(response, list, "区域数据");
    }

    /**
     * 获取区域详细信息
     */
     @ApiOperation("获取区域列表")
    @PreAuthorize("@ss.hasPermi('system:area:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAreaService.selectMmsAreaById(id));
    }

    /**
     * 新增区域
     */
     @ApiOperation("新增区域列表")
    @PreAuthorize("@ss.hasPermi('system:area:add')")
    @Log(title = "区域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsArea mmsArea)
    {
        return toAjax(mmsAreaService.insertMmsArea(mmsArea));
    }

    /**
     * 修改区域
     */
     @ApiOperation("修改区域列表")
    @PreAuthorize("@ss.hasPermi('system:area:edit')")
    @Log(title = "区域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsArea mmsArea)
    {
        return toAjax(mmsAreaService.updateMmsArea(mmsArea));
    }

    /**
     * 删除区域
     */
      @ApiOperation("删除区域列表")
    @PreAuthorize("@ss.hasPermi('system:area:remove')")
    @Log(title = "区域", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAreaService.deleteMmsAreaByIds(ids));
    }
}
