package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetTransfer;
import com.ruoyi.system.service.IMmsAssetTransferService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产调拨Controller
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/transfer")
public class MmsAssetTransferController extends BaseController
{
    @Autowired
    private IMmsAssetTransferService mmsAssetTransferService;

    /**
     * 查询资产调拨列表
     */
    @PreAuthorize("@ss.hasPermi('system:transfer:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetTransfer mmsAssetTransfer)
    {
        startPage();
        List<MmsAssetTransfer> list = mmsAssetTransferService.selectMmsAssetTransferList(mmsAssetTransfer);
        return getDataTable(list);
    }

    /**
     * 导出资产调拨列表
     */
    @PreAuthorize("@ss.hasPermi('system:transfer:export')")
    @Log(title = "资产调拨", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetTransfer mmsAssetTransfer)
    {
        List<MmsAssetTransfer> list = mmsAssetTransferService.selectMmsAssetTransferList(mmsAssetTransfer);
        ExcelUtil<MmsAssetTransfer> util = new ExcelUtil<MmsAssetTransfer>(MmsAssetTransfer.class);
        util.exportExcel(response, list, "资产调拨数据");
    }

    /**
     * 获取资产调拨详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:transfer:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetTransferService.selectMmsAssetTransferById(id));
    }

    /**
     * 新增资产调拨
     */
    @PreAuthorize("@ss.hasPermi('system:transfer:add')")
    @Log(title = "资产调拨", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetTransfer mmsAssetTransfer)
    {
        return toAjax(mmsAssetTransferService.insertMmsAssetTransfer(mmsAssetTransfer));
    }

    /**
     * 修改资产调拨
     */
    @PreAuthorize("@ss.hasPermi('system:transfer:edit')")
    @Log(title = "资产调拨", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetTransfer mmsAssetTransfer)
    {
        return toAjax(mmsAssetTransferService.updateMmsAssetTransfer(mmsAssetTransfer));
    }

    /**
     * 删除资产调拨
     */
    @PreAuthorize("@ss.hasPermi('system:transfer:remove')")
    @Log(title = "资产调拨", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetTransferService.deleteMmsAssetTransferByIds(ids));
    }
}
