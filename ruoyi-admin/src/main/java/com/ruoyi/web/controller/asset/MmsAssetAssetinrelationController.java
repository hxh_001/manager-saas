package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsAssetAssetinrelation;
import com.ruoyi.system.service.IMmsAssetAssetinrelationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产入库单-关联资产idController
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@RestController
@RequestMapping("/system/assetinrelation")
public class MmsAssetAssetinrelationController extends BaseController
{
    @Autowired
    private IMmsAssetAssetinrelationService mmsAssetAssetinrelationService;

    /**
     * 查询资产入库单-关联资产id列表
     */
    @PreAuthorize("@ss.hasPermi('system:assetinrelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        startPage();
        List<MmsAssetAssetinrelation> list = mmsAssetAssetinrelationService.selectMmsAssetAssetinrelationList(mmsAssetAssetinrelation);
        return getDataTable(list);
    }

    /**
     * 导出资产入库单-关联资产id列表
     */
    @PreAuthorize("@ss.hasPermi('system:assetinrelation:export')")
    @Log(title = "资产入库单-关联资产id", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        List<MmsAssetAssetinrelation> list = mmsAssetAssetinrelationService.selectMmsAssetAssetinrelationList(mmsAssetAssetinrelation);
        ExcelUtil<MmsAssetAssetinrelation> util = new ExcelUtil<MmsAssetAssetinrelation>(MmsAssetAssetinrelation.class);
        util.exportExcel(response, list, "资产入库单-关联资产id数据");
    }

    /**
     * 获取资产入库单-关联资产id详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:assetinrelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetAssetinrelationService.selectMmsAssetAssetinrelationById(id));
    }

    /**
     * 新增资产入库单-关联资产id
     */
    @PreAuthorize("@ss.hasPermi('system:assetinrelation:add')")
    @Log(title = "资产入库单-关联资产id", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        return toAjax(mmsAssetAssetinrelationService.insertMmsAssetAssetinrelation(mmsAssetAssetinrelation));
    }

    /**
     * 修改资产入库单-关联资产id
     */
    @PreAuthorize("@ss.hasPermi('system:assetinrelation:edit')")
    @Log(title = "资产入库单-关联资产id", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetAssetinrelation mmsAssetAssetinrelation)
    {
        return toAjax(mmsAssetAssetinrelationService.updateMmsAssetAssetinrelation(mmsAssetAssetinrelation));
    }

    /**
     * 删除资产入库单-关联资产id
     */
    @PreAuthorize("@ss.hasPermi('system:assetinrelation:remove')")
    @Log(title = "资产入库单-关联资产id", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetAssetinrelationService.deleteMmsAssetAssetinrelationByIds(ids));
    }
}
