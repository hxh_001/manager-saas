package com.ruoyi.web.controller.goods;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsGoodsout;
import com.ruoyi.system.service.IMmsGoodsoutService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品出库单（关联入库单）Controller
 * 
 * @author ruoyi
 * @date 2024-06-29
 */
@RestController
@RequestMapping("/system/goodsout")
public class MmsGoodsoutController extends BaseController
{
    @Autowired
    private IMmsGoodsoutService mmsGoodsoutService;

    /**
     * 查询物品出库单（关联入库单）列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsout:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsGoodsout mmsGoodsout)
    {
        startPage();
        List<MmsGoodsout> list = mmsGoodsoutService.selectMmsGoodsoutList(mmsGoodsout);
        return getDataTable(list);
    }

    /**
     * 导出物品出库单（关联入库单）列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsout:export')")
    @Log(title = "物品出库单（关联入库单）", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsGoodsout mmsGoodsout)
    {
        List<MmsGoodsout> list = mmsGoodsoutService.selectMmsGoodsoutList(mmsGoodsout);
        ExcelUtil<MmsGoodsout> util = new ExcelUtil<MmsGoodsout>(MmsGoodsout.class);
        util.exportExcel(response, list, "物品出库单（关联入库单）数据");
    }

    /**
     * 获取物品出库单（关联入库单）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:goodsout:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsGoodsoutService.selectMmsGoodsoutById(id));
    }

    /**
     * 新增物品出库单（关联入库单）
     */
    @PreAuthorize("@ss.hasPermi('system:goodsout:add')")
    @Log(title = "物品出库单（关联入库单）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsGoodsout mmsGoodsout)
    {
        return toAjax(mmsGoodsoutService.insertMmsGoodsout(mmsGoodsout));
    }

    /**
     * 修改物品出库单（关联入库单）
     */
    @PreAuthorize("@ss.hasPermi('system:goodsout:edit')")
    @Log(title = "物品出库单（关联入库单）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsGoodsout mmsGoodsout)
    {
        return toAjax(mmsGoodsoutService.updateMmsGoodsout(mmsGoodsout));
    }

    /**
     * 删除物品出库单（关联入库单）
     */
    @PreAuthorize("@ss.hasPermi('system:goodsout:remove')")
    @Log(title = "物品出库单（关联入库单）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsGoodsoutService.deleteMmsGoodsoutByIds(ids));
    }
}
