package com.ruoyi.web.controller.asset;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.system.domain.MmsAssetInventoryrelation;
import com.ruoyi.system.service.IMmsAssetInventoryrelationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资产盘库单-关联资产idController
 * 
 * @author ruoyi
 * @date 2024-07-20
 */
@Api("资产盘库单-关联资产id")
@RestController
@RequestMapping("/system/inventoryrelation")
public class MmsAssetInventoryrelationController extends BaseController
{
    @Autowired
    private IMmsAssetInventoryrelationService mmsAssetInventoryrelationService;

    /**
     * 查询资产盘库单-关联资产id列表
     */
     @ApiOperation("查询资产盘库单-关联资产id列表")
    @PreAuthorize("@ss.hasPermi('system:inventoryrelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsAssetInventoryrelation mmsAssetInventoryrelation)
    {
        startPage();
        List<MmsAssetInventoryrelation> list = mmsAssetInventoryrelationService.selectMmsAssetInventoryrelationList(mmsAssetInventoryrelation);
        return getDataTable(list);
    }

    /**
     * 导出资产盘库单-关联资产id列表
     */
     @ApiOperation("导出资产盘库单-关联资产id列表")
    @PreAuthorize("@ss.hasPermi('system:inventoryrelation:export')")
    @Log(title = "资产盘库单-关联资产id", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsAssetInventoryrelation mmsAssetInventoryrelation)
    {
        List<MmsAssetInventoryrelation> list = mmsAssetInventoryrelationService.selectMmsAssetInventoryrelationList(mmsAssetInventoryrelation);
        ExcelUtil<MmsAssetInventoryrelation> util = new ExcelUtil<MmsAssetInventoryrelation>(MmsAssetInventoryrelation.class);
        util.exportExcel(response, list, "资产盘库单-关联资产id数据");
    }

    /**
     * 获取资产盘库单-关联资产id详细信息
     */
     @ApiOperation("获取资产盘库单-关联资产id列表")
    @PreAuthorize("@ss.hasPermi('system:inventoryrelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsAssetInventoryrelationService.selectMmsAssetInventoryrelationById(id));
    }

    /**
     * 新增资产盘库单-关联资产id
     */
     @ApiOperation("新增资产盘库单-关联资产id列表")
    @PreAuthorize("@ss.hasPermi('system:inventoryrelation:add')")
    @Log(title = "资产盘库单-关联资产id", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsAssetInventoryrelation mmsAssetInventoryrelation)
    {
        return toAjax(mmsAssetInventoryrelationService.insertMmsAssetInventoryrelation(mmsAssetInventoryrelation));
    }

    /**
     * 修改资产盘库单-关联资产id
     */
     @ApiOperation("修改资产盘库单-关联资产id列表")
    @PreAuthorize("@ss.hasPermi('system:inventoryrelation:edit')")
    @Log(title = "资产盘库单-关联资产id", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsAssetInventoryrelation mmsAssetInventoryrelation)
    {
        return toAjax(mmsAssetInventoryrelationService.updateMmsAssetInventoryrelation(mmsAssetInventoryrelation));
    }

    /**
     * 删除资产盘库单-关联资产id
     */
      @ApiOperation("删除资产盘库单-关联资产id列表")
    @PreAuthorize("@ss.hasPermi('system:inventoryrelation:remove')")
    @Log(title = "资产盘库单-关联资产id", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsAssetInventoryrelationService.deleteMmsAssetInventoryrelationByIds(ids));
    }
}
