package com.ruoyi.web.controller.goods;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MmsGoodsclassify;
import com.ruoyi.system.service.IMmsGoodsclassifyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品分类Controller
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@RestController
@RequestMapping("/system/goodsclassify")
public class MmsGoodsclassifyController extends BaseController
{
    @Autowired
    private IMmsGoodsclassifyService mmsGoodsclassifyService;

    /**
     * 查询物品分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsclassify:list')")
    @GetMapping("/list")
    public TableDataInfo list(MmsGoodsclassify mmsGoodsclassify)
    {
        startPage();
        List<MmsGoodsclassify> list = mmsGoodsclassifyService.selectMmsGoodsclassifyList(mmsGoodsclassify);
        return getDataTable(list);
    }

    /**
     * 导出物品分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:goodsclassify:export')")
    @Log(title = "物品分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MmsGoodsclassify mmsGoodsclassify)
    {
        List<MmsGoodsclassify> list = mmsGoodsclassifyService.selectMmsGoodsclassifyList(mmsGoodsclassify);
        ExcelUtil<MmsGoodsclassify> util = new ExcelUtil<MmsGoodsclassify>(MmsGoodsclassify.class);
        util.exportExcel(response, list, "物品分类数据");
    }

    /**
     * 获取物品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:goodsclassify:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mmsGoodsclassifyService.selectMmsGoodsclassifyById(id));
    }

    /**
     * 新增物品分类
     */
    @PreAuthorize("@ss.hasPermi('system:goodsclassify:add')")
    @Log(title = "物品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MmsGoodsclassify mmsGoodsclassify)
    {
        return toAjax(mmsGoodsclassifyService.insertMmsGoodsclassify(mmsGoodsclassify));
    }

    /**
     * 修改物品分类
     */
    @PreAuthorize("@ss.hasPermi('system:goodsclassify:edit')")
    @Log(title = "物品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MmsGoodsclassify mmsGoodsclassify)
    {
        return toAjax(mmsGoodsclassifyService.updateMmsGoodsclassify(mmsGoodsclassify));
    }

    /**
     * 删除物品分类
     */
    @PreAuthorize("@ss.hasPermi('system:goodsclassify:remove')")
    @Log(title = "物品分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mmsGoodsclassifyService.deleteMmsGoodsclassifyByIds(ids));
    }
}
